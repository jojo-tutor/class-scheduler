/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modules;

import java.awt.Color;

/**
 *
 * @author Jojo
 */

public class ColorClassHandler {
    private final Color AQUA                = new Color(0x00FFFF);  //0     foreground: black
    private final Color BISQUE              = new Color(0xFFE4C4);  //1     foreground: black
    private final Color BLUE                = new Color(0x0000FF);  //2     foreground: white
    private final Color BROWN               = new Color(0xA52A2A);  //3     foreground: white
    private final Color CHARTREUSE          = new Color(0x7FFF00);  //4     foreground: black
    private final Color CORAL               = new Color(0xFF7F50);  //5     foreground: black
    private final Color DARKGOLDENROD       = new Color(0xB8860B);  //6     foreground: white
    private final Color DARKMAGENTA         = new Color(0x8B008B);  //7     foreground: white
    private final Color DARKORANGE          = new Color(0xFF8C00);  //8     foreground: black
    private final Color DEEPPINK            = new Color(0xFF1493);  //9     foreground: white
    private final Color DIMGRAY             = new Color(0x696969);  //10    foreground: white
    private final Color GOLD                = new Color(0xFFD700);  //11    foreground: black
    private final Color GREEN               = new Color(0x008000);  //12    foreground: white
    private final Color FUSHSIA             = new Color(0xFF00FF);  //13    foreground: black
    private final Color KHAKI               = new Color(0xF0E68C);  //14    foreground: black
    private final Color LIGHTSKYBLUE        = new Color(0x87CEFA);  //15    foreground: black
    private final Color ORANGE              = new Color(0xFFA500);  //16    foreground: black
    private final Color YELLOW              = new Color(0xFFFF00);  //17    foreground: black
    private final Color VIOLET              = new Color(0xEE82EE);  //18    foreground: black
    private final Color GREENYELLOW         = new Color(0xADFF2F);  //19    foreground: black
    private final Color TAN                 = new Color(0xD2B48C);  //20    foreground: black
    private final Color AQUAMARINE          = new Color(0x7FFFD4);  //21    foreground: black
    private final Color BLUEVIOLET          = new Color(0x8A2BE2);  //22    foreground: white
    private final Color BURLYWOOD           = new Color(0xDEB887);  //23    foreground: black
    private final Color DARKOLIVEGREEN      = new Color(0x556B2F);  //24    foreground: white
    private final Color CADETBLUE           = new Color(0x5F9EA0);  //25    foreground: white
    private final Color CHOCOLATE           = new Color(0xD2691E);  //26    foreground: white
    private final Color CORNFLOWERBLUE      = new Color(0x6495ED);  //27    foreground: black
    private final Color CRIMSON             = new Color(0xDC143C);  //28    foreground: white
    private final Color CYAN                = new Color(0x00FFFF);  //29    foreground: black
    private final Color MEDIUMPURPLE        = new Color(0x9370DB);  //30    foreground: white
    private final Color DARKGREEN           = new Color(0x006400);  //31    foreground: white
    private final Color DARKRED             = new Color(0x8B0000);  //32    foreground: white
    private final Color DARKSALMON          = new Color(0xE9967A);  //33    foreground: black
    private final Color DARKSEAGREEN        = new Color(0x8FBC8F);  //34    foreground: black
    private final Color DARKSLATEBLUE       = new Color(0x483D8B);  //35    foreground: white
    private final Color DARKSLATEGRAY       = new Color(0x2F4F4F);  //36    foreground: white
    private final Color INDIANRED           = new Color(0xCD5C5C);  //37    foreground: white
    private final Color MEDIUMORCHID        = new Color(0xBA55D3);  //38    foreground: white
    private final Color LIGHTBLUE           = new Color(0xADD8E6);  //39    foreground: black
    private final Color LIGHTGREEN          = new Color(0x90EE90);  //40    foreground: black
    private final Color LIGHTPINK           = new Color(0xFFB6C1);  //41    foreground: black
    private final Color LIGHTSTEELBLUE      = new Color(0xB0C4DE);  //42    foreground: black
    private final Color MAGENTA             = new Color(0xFF00FF);  //43    foreground: black
    private final Color OLIVE               = new Color(0x808000);  //44    foreground: white
    private final Color ORANGERED           = new Color(0xFF4500);  //45    foreground: black
    private final Color PALEVIOLETRED       = new Color(0xDB7093);  //46    foreground: black
    private final Color PLUM                = new Color(0xDDA0DD);  //47    foreground: black
    private final Color WHEAT               = new Color(0xF5DEB3);  //48    foreground: black
    private final Color TOMATO              = new Color(0xFF6347);  //49    foreground: black
    
    
    
    private final Color COLORS[] = {AQUA,BISQUE,BLUE,BROWN,CHARTREUSE,CORAL,DARKGOLDENROD,
                                    DARKMAGENTA,DARKORANGE,DEEPPINK,DIMGRAY,GOLD,GREEN,
                                    FUSHSIA,KHAKI,LIGHTSKYBLUE,ORANGE,YELLOW,VIOLET,
                                    GREENYELLOW,TAN,AQUAMARINE,BLUEVIOLET,BURLYWOOD,DARKOLIVEGREEN,CADETBLUE,
                                    CHOCOLATE,CORNFLOWERBLUE,CRIMSON,CYAN,MEDIUMPURPLE,DARKGREEN,
                                    DARKRED,DARKSALMON,DARKSEAGREEN,DARKSLATEBLUE,DARKSLATEGRAY,INDIANRED,
                                    MEDIUMORCHID,LIGHTBLUE,LIGHTGREEN,LIGHTPINK,LIGHTSTEELBLUE,MAGENTA,
                                    OLIVE,ORANGERED,PALEVIOLETRED,PLUM,WHEAT,TOMATO};
    
    private final int BLACK_INDEX[] = {0,1,4,5,8,11,13,14,15,16,17,18,
                                        19,20,21,23,27,29,33,34,39,
                                        40,41,42,43,45,46,47,48,49};

    public ColorClassHandler() {
    }
    
    public Color getColor(int index){
        return COLORS[index];
    }
    
    public Color getForeground(int index){
        boolean found=false;
        for(int a=0;a<BLACK_INDEX.length;a++){
            if(index==BLACK_INDEX[a]){
                found=true;
                break;
            }
        }
        if(found){
            return Color.BLACK;
        }else{
            return Color.WHITE;
        }
    }
    
    public int getNextIndex(int currentIndex){
        int maxIndex = COLORS.length;
        
        if(currentIndex+1<maxIndex){
            return currentIndex+1;
        }else{
            return 0;
        }
    }
}
