/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modules;

import AppPackage.*;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Jojo
 */
public class ClassSchedulerRenderer1 extends DefaultTableCellRenderer {

    public static int col_reserved_1[];
    public static int row_reserved_1[];
    public static boolean isUsed_1 = false;
    public static int schedID_1 = -1;

    public static int col_reserved_2[];
    public static int row_reserved_2[];
    public static boolean isUsed_2 = false;
    public static int schedID_2 = -1;

    public static int col_reserved_3[];
    public static int row_reserved_3[];
    public static boolean isUsed_3 = false;
    public static int schedID_3 = -1;

    public static int col_reserved_4[];
    public static int row_reserved_4[];
    public static boolean isUsed_4 = false;
    public static int schedID_4 = -1;

    public static int col_reserved_5[];
    public static int row_reserved_5[];
    public static boolean isUsed_5 = false;
    public static int schedID_5 = -1;

    public static int col_reserved_6[];
    public static int row_reserved_6[];
    public static boolean isUsed_6 = false;
    public static int schedID_6 = -1;

    public static int col_reserved_7[];
    public static int row_reserved_7[];
    public static boolean isUsed_7 = false;
    public static int schedID_7 = -1;

    public static int col_reserved_8[];
    public static int row_reserved_8[];
    public static boolean isUsed_8 = false;
    public static int schedID_8 = -1;

    public static int col_reserved_9[];
    public static int row_reserved_9[];
    public static boolean isUsed_9 = false;
    public static int schedID_9 = -1;

    public static int col_reserved_10[];
    public static int row_reserved_10[];
    public static boolean isUsed_10 = false;
    public static int schedID_10 = -1;

    public static int col_reserved_11[];
    public static int row_reserved_11[];
    public static boolean isUsed_11 = false;
    public static int schedID_11 = -1;

    public static int col_reserved_12[];
    public static int row_reserved_12[];
    public static boolean isUsed_12 = false;
    public static int schedID_12 = -1;

    public static int col_reserved_13[];
    public static int row_reserved_13[];
    public static boolean isUsed_13 = false;
    public static int schedID_13 = -1;

    public static int col_reserved_14[];
    public static int row_reserved_14[];
    public static boolean isUsed_14 = false;
    public static int schedID_14 = -1;

    public static int col_reserved_15[];
    public static int row_reserved_15[];
    public static boolean isUsed_15 = false;
    public static int schedID_15 = -1;
    
    public static int colorBy = 0; //color by: 0=Subject 1=Room 2=faculty

    public void clearVariables() {
        col_reserved_1 = new int[0];
        row_reserved_1 = new int[0];
        isUsed_1 = false;
        schedID_1 = -1;

        col_reserved_2 = new int[0];
        row_reserved_2 = new int[0];
        isUsed_2 = false;
        schedID_2 = -1;

        col_reserved_3 = new int[0];
        row_reserved_3 = new int[0];
        isUsed_3 = false;
        schedID_3 = -1;

        col_reserved_4 = new int[0];
        row_reserved_4 = new int[0];
        isUsed_4 = false;
        schedID_4 = -1;

        col_reserved_5 = new int[0];
        row_reserved_5 = new int[0];
        isUsed_5 = false;
        schedID_5 = -1;

        col_reserved_6 = new int[0];
        row_reserved_6 = new int[0];
        isUsed_6 = false;
        schedID_6 = -1;

        col_reserved_7 = new int[0];
        row_reserved_7 = new int[0];
        isUsed_7 = false;
        schedID_7 = -1;

        col_reserved_8 = new int[0];
        row_reserved_8 = new int[0];
        isUsed_8 = false;
        schedID_8 = -1;

        col_reserved_9 = new int[0];
        row_reserved_9 = new int[0];
        isUsed_9 = false;
        schedID_9 = -1;

        col_reserved_10 = new int[0];
        row_reserved_10 = new int[0];
        isUsed_10 = false;
        schedID_10 = -1;

        col_reserved_11 = new int[0];
        row_reserved_11 = new int[0];
        isUsed_11 = false;
        schedID_11 = -1;

        col_reserved_12 = new int[0];
        row_reserved_12 = new int[0];
        isUsed_12 = false;
        schedID_12 = -1;

        col_reserved_13 = new int[0];
        row_reserved_13 = new int[0];
        isUsed_13 = false;
        schedID_13 = -1;

        col_reserved_14 = new int[0];
        row_reserved_14 = new int[0];
        isUsed_14 = false;
        schedID_14 = -1;

        col_reserved_15 = new int[0];
        row_reserved_15 = new int[0];
        isUsed_15 = false;
        schedID_15 = -1;

    }

    @Override
    public java.awt.Component getTableCellRendererComponent(javax.swing.JTable table, java.lang.Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        //java.awt.Component cellComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        JLabel cellComponent = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        //l.setBackground(Color.GREEN);
        cellComponent.setBackground(java.awt.Color.WHITE);
        cellComponent.setForeground(Color.BLACK);
        cellComponent.setToolTipText(null);

        if (isSelected) {
            cellComponent.setBackground(new java.awt.Color(51, 153, 255));
        }

        if (isUsed_1 && schedID_1!=-1) {
            for (int colIndex = 0; colIndex < col_reserved_1.length; colIndex++) {
                for (int rowIndex = 0; rowIndex < row_reserved_1.length; rowIndex++) {
                    if (column == col_reserved_1[colIndex] && row == row_reserved_1[rowIndex]) {
                        
                        dbConnection dbcon1 = new dbConnection();
                        String strColor="";
                        switch (colorBy) {
                            case 0:
                                //below -> for Color by Subject, LAB and LEC are same color
                                strColor = dbcon1.findOneQuery("sjc.color_index", "tbl_schedule sc,tbl_subject sub,tbl_subject_detail sjd,tbl_subject_color sjc", "sc.id='" + schedID_1 + "' and sc.subject_id=sjd.id and sjd.subject_id=sjc.subject_id");
                                break;
                                
                            case 1:
                                //below -> for Color by Room
                                strColor = dbcon1.findOneQuery("rmc.color_index", "tbl_schedule sc,tbl_room_color rmc", "sc.id='" + schedID_1 + "' and sc.room_id=rmc.room_id");
                                break;
                                
                            case 2:
                                //below -> for Color by Faculty
                                strColor = dbcon1.findOneQuery("fc.color_index", "tbl_schedule sc,tbl_faculty_color fc", "sc.id='" + schedID_1 + "' and sc.faculty_id=fc.faculty_id");
                                //below -> for Color by Class
                                //String strColor = dbcon1.findOneQuery("clc.color_index", "tbl_schedule sc,tbl_classlist_color clc", "sc.id='" + schedID_1 + "' and sc.classlist_id=clc.classlist_id and sc.section=clc.section");
                                break;
                                
                            case 3:
                                //below -> for Color by Class
                                strColor = dbcon1.findOneQuery("clc.color_index", "tbl_schedule sc,tbl_classlist_color clc", "sc.id='" + schedID_1 + "' and sc.classlist_id=clc.classlist_id and sc.section=clc.section");
                                break;
                                
                            default:
                                break;
                        }
     
                        ColorClassHandler cch = new ColorClassHandler();
                        int colorIndex = Integer.parseInt(strColor);
                        if(colorIndex!=-1){
                            cellComponent.setBackground(cch.getColor(colorIndex));
                            cellComponent.setForeground(cch.getForeground(colorIndex));
                        }
                
                        cellComponent.setToolTipText("<html><b>" + dbcon1.findOneQuery("CONCAT(b.address,' ',a.firstname,' ',a.middlename,' ',a.lastname,' ',c.extension)","tbl_faculty a, tbl_name_address b, tbl_name_extension c, tbl_schedule sc","a.address_id=b.id and a.extension_id=c.id and sc.faculty_id=a.id and sc.id='" + schedID_1 + "'") + "</b><br/><i>" + dbcon1.findOneQuery("sj.subject_description", "tbl_subject sj,tbl_subject_detail sjd,tbl_schedule sc", "sc.subject_id=sjd.id and sjd.subject_id=sj.id and sc.id='" + schedID_1 + "'") + "</i></html>");
                        
                        if (rowIndex % 2 == 0) {
                            if ((rowIndex + 1) != row_reserved_1.length) {
                                cellComponent.setText(dbcon1.findOneQuery("CONCAT(sj.subject_code,' (',sjd.subject_type,')')", "tbl_subject sj,tbl_subject_detail sjd,tbl_schedule sc", "sc.subject_id=sjd.id and sjd.subject_id=sj.id and sc.id='" + schedID_1 + "'"));
                            }

                        } else {
                            cellComponent.setText("Room: "+dbcon1.findOneQuery("r.room", "tbl_room r, tbl_schedule sc", "sc.room_id=r.id and sc.id='" + schedID_1 + "'"));
                        }
                    }
                }
            }
        }

        if (isUsed_2 && schedID_2!=-1) {
            for (int colIndex = 0; colIndex < col_reserved_2.length; colIndex++) {
                for (int rowIndex = 0; rowIndex < row_reserved_2.length; rowIndex++) {
                    if (column == col_reserved_2[colIndex] && row == row_reserved_2[rowIndex]) {
                        
                        dbConnection dbcon1 = new dbConnection();
                        String strColor="";
                        switch (colorBy) {
                            case 0:
                                //below -> for Color by Subject, LAB and LEC are same color
                                strColor = dbcon1.findOneQuery("sjc.color_index", "tbl_schedule sc,tbl_subject sub,tbl_subject_detail sjd,tbl_subject_color sjc", "sc.id='" + schedID_2 + "' and sc.subject_id=sjd.id and sjd.subject_id=sjc.subject_id");
                                break;
                                
                            case 1:
                                //below -> for Color by Room
                                strColor = dbcon1.findOneQuery("rmc.color_index", "tbl_schedule sc,tbl_room_color rmc", "sc.id='" + schedID_2 + "' and sc.room_id=rmc.room_id");
                                break;
                                
                            case 2:
                                //below -> for Color by Faculty
                                strColor = dbcon1.findOneQuery("fc.color_index", "tbl_schedule sc,tbl_faculty_color fc", "sc.id='" + schedID_2 + "' and sc.faculty_id=fc.faculty_id");
                                //below -> for Color by Class
                                //String strColor = dbcon1.findOneQuery("clc.color_index", "tbl_schedule sc,tbl_classlist_color clc", "sc.id='" + schedID_2 + "' and sc.classlist_id=clc.classlist_id and sc.section=clc.section");
                                break;
                               
                            case 3:
                                //below -> for Color by Class
                                strColor = dbcon1.findOneQuery("clc.color_index", "tbl_schedule sc,tbl_classlist_color clc", "sc.id='" + schedID_2 + "' and sc.classlist_id=clc.classlist_id and sc.section=clc.section");
                                break;
                                
                            default:
                                break;
                        }
                        
                        ColorClassHandler cch = new ColorClassHandler();
                        int colorIndex = Integer.parseInt(strColor);
                        if(colorIndex!=-1){
                            cellComponent.setBackground(cch.getColor(colorIndex));
                            cellComponent.setForeground(cch.getForeground(colorIndex));
                        }
                        
                        cellComponent.setToolTipText("<html><b>" + dbcon1.findOneQuery("CONCAT(b.address,' ',a.firstname,' ',a.middlename,' ',a.lastname,' ',c.extension)","tbl_faculty a, tbl_name_address b, tbl_name_extension c, tbl_schedule sc","a.address_id=b.id and a.extension_id=c.id and sc.faculty_id=a.id and sc.id='" + schedID_2 + "'") + "</b><br/><i>" + dbcon1.findOneQuery("sj.subject_description", "tbl_subject sj,tbl_subject_detail sjd,tbl_schedule sc", "sc.subject_id=sjd.id and sjd.subject_id=sj.id and sc.id='" + schedID_2 + "'") + "</i></html>");
                        
                        if (rowIndex % 2 == 0) {
                            if ((rowIndex + 1) != row_reserved_2.length) {
                                //dbConnection dbcon = new dbConnection();
                                cellComponent.setText(dbcon1.findOneQuery("CONCAT(sj.subject_code,' (',sjd.subject_type,')')", "tbl_subject sj,tbl_subject_detail sjd,tbl_schedule sc", "sc.subject_id=sjd.id and sjd.subject_id=sj.id and sc.id='" + schedID_2 + "'"));
                            }
                        } else {
                            //dbConnection dbcon = new dbConnection();
                            cellComponent.setText("Room: " + dbcon1.findOneQuery("r.room", "tbl_room r, tbl_schedule sc", "sc.room_id=r.id and sc.id='" + schedID_2 + "'"));
                        }
                    }
                }
            }
        }

        if (isUsed_3 && schedID_3!=-1) {
            for (int colIndex = 0; colIndex < col_reserved_3.length; colIndex++) {
                for (int rowIndex = 0; rowIndex < row_reserved_3.length; rowIndex++) {
                    if (column == col_reserved_3[colIndex] && row == row_reserved_3[rowIndex]) {
                        
                        dbConnection dbcon1 = new dbConnection();
                        String strColor="";
                        switch (colorBy) {
                            case 0:
                                //below -> for Color by Subject, LAB and LEC are same color
                                strColor = dbcon1.findOneQuery("sjc.color_index", "tbl_schedule sc,tbl_subject sub,tbl_subject_detail sjd,tbl_subject_color sjc", "sc.id='" + schedID_3 + "' and sc.subject_id=sjd.id and sjd.subject_id=sjc.subject_id");
                                break;
                                
                            case 1:
                                //below -> for Color by Room
                                strColor = dbcon1.findOneQuery("rmc.color_index", "tbl_schedule sc,tbl_room_color rmc", "sc.id='" + schedID_3 + "' and sc.room_id=rmc.room_id");
                                break;
                                
                            case 2:
                                //below -> for Color by Faculty
                                strColor = dbcon1.findOneQuery("fc.color_index", "tbl_schedule sc,tbl_faculty_color fc", "sc.id='" + schedID_3 + "' and sc.faculty_id=fc.faculty_id");
                                //below -> for Color by Class
                                //String strColor = dbcon1.findOneQuery("clc.color_index", "tbl_schedule sc,tbl_classlist_color clc", "sc.id='" + schedID_3 + "' and sc.classlist_id=clc.classlist_id and sc.section=clc.section");
                                break;
                              
                            case 3:
                                //below -> for Color by Class
                                strColor = dbcon1.findOneQuery("clc.color_index", "tbl_schedule sc,tbl_classlist_color clc", "sc.id='" + schedID_3 + "' and sc.classlist_id=clc.classlist_id and sc.section=clc.section");
                                break;
                                
                            default:
                                break;
                        }
                        
                        ColorClassHandler cch = new ColorClassHandler();
                        int colorIndex = Integer.parseInt(strColor);
                        if(colorIndex!=-1){
                            cellComponent.setBackground(cch.getColor(colorIndex));
                            cellComponent.setForeground(cch.getForeground(colorIndex));
                        }
                        
                        cellComponent.setToolTipText("<html><b>" + dbcon1.findOneQuery("CONCAT(b.address,' ',a.firstname,' ',a.middlename,' ',a.lastname,' ',c.extension)","tbl_faculty a, tbl_name_address b, tbl_name_extension c, tbl_schedule sc","a.address_id=b.id and a.extension_id=c.id and sc.faculty_id=a.id and sc.id='" + schedID_3 + "'") + "</b><br/><i>" + dbcon1.findOneQuery("sj.subject_description", "tbl_subject sj,tbl_subject_detail sjd,tbl_schedule sc", "sc.subject_id=sjd.id and sjd.subject_id=sj.id and sc.id='" + schedID_3 + "'") + "</i></html>");
                        
                        if (rowIndex % 2 == 0) {
                            if ((rowIndex + 1) != row_reserved_3.length) {
                                //dbConnection dbcon = new dbConnection();
                                cellComponent.setText(dbcon1.findOneQuery("CONCAT(sj.subject_code,' (',sjd.subject_type,')')", "tbl_subject sj,tbl_subject_detail sjd,tbl_schedule sc", "sc.subject_id=sjd.id and sjd.subject_id=sj.id and sc.id='" + schedID_3 + "'"));
                            }
                        } else {
                            //dbConnection dbcon = new dbConnection();
                            cellComponent.setText("Room: " + dbcon1.findOneQuery("r.room", "tbl_room r, tbl_schedule sc", "sc.room_id=r.id and sc.id='" + schedID_3 + "'"));
                        }

                    }
                }
            }
        }

        if (isUsed_4 && schedID_4!=-1) {
            for (int colIndex = 0; colIndex < col_reserved_4.length; colIndex++) {
                for (int rowIndex = 0; rowIndex < row_reserved_4.length; rowIndex++) {
                    if (column == col_reserved_4[colIndex] && row == row_reserved_4[rowIndex]) {
                        
                        dbConnection dbcon1 = new dbConnection();
                        String strColor="";
                        switch (colorBy) {
                            case 0:
                                //below -> for Color by Subject, LAB and LEC are same color
                                strColor = dbcon1.findOneQuery("sjc.color_index", "tbl_schedule sc,tbl_subject sub,tbl_subject_detail sjd,tbl_subject_color sjc", "sc.id='" + schedID_4 + "' and sc.subject_id=sjd.id and sjd.subject_id=sjc.subject_id");
                                break;
                                
                            case 1:
                                //below -> for Color by Room
                                strColor = dbcon1.findOneQuery("rmc.color_index", "tbl_schedule sc,tbl_room_color rmc", "sc.id='" + schedID_4 + "' and sc.room_id=rmc.room_id");
                                break;
                                
                            case 2:
                                //below -> for Color by Faculty
                                strColor = dbcon1.findOneQuery("fc.color_index", "tbl_schedule sc,tbl_faculty_color fc", "sc.id='" + schedID_4 + "' and sc.faculty_id=fc.faculty_id");
                                //below -> for Color by Class
                                //String strColor = dbcon1.findOneQuery("clc.color_index", "tbl_schedule sc,tbl_classlist_color clc", "sc.id='" + schedID_4 + "' and sc.classlist_id=clc.classlist_id and sc.section=clc.section");
                                break;
                                
                            case 3:
                                //below -> for Color by Class
                                strColor = dbcon1.findOneQuery("clc.color_index", "tbl_schedule sc,tbl_classlist_color clc", "sc.id='" + schedID_4 + "' and sc.classlist_id=clc.classlist_id and sc.section=clc.section");
                                break;
                                
                            default:
                                break;
                        }
                        
                        ColorClassHandler cch = new ColorClassHandler();
                        int colorIndex = Integer.parseInt(strColor);
                        if(colorIndex!=-1){
                            cellComponent.setBackground(cch.getColor(colorIndex));
                            cellComponent.setForeground(cch.getForeground(colorIndex));
                        }
                        
                        cellComponent.setToolTipText("<html><b>" + dbcon1.findOneQuery("CONCAT(b.address,' ',a.firstname,' ',a.middlename,' ',a.lastname,' ',c.extension)","tbl_faculty a, tbl_name_address b, tbl_name_extension c, tbl_schedule sc","a.address_id=b.id and a.extension_id=c.id and sc.faculty_id=a.id and sc.id='" + schedID_4 + "'") + "</b><br/><i>" + dbcon1.findOneQuery("sj.subject_description", "tbl_subject sj,tbl_subject_detail sjd,tbl_schedule sc", "sc.subject_id=sjd.id and sjd.subject_id=sj.id and sc.id='" + schedID_4 + "'") + "</i></html>");
                        
                        if (rowIndex % 2 == 0) {
                            if ((rowIndex + 1) != row_reserved_4.length) {
                                //dbConnection dbcon = new dbConnection();
                                cellComponent.setText(dbcon1.findOneQuery("CONCAT(sj.subject_code,' (',sjd.subject_type,')')", "tbl_subject sj,tbl_subject_detail sjd,tbl_schedule sc", "sc.subject_id=sjd.id and sjd.subject_id=sj.id and sc.id='" + schedID_4 + "'"));
                            }
                        } else {
                            //dbConnection dbcon = new dbConnection();
                            cellComponent.setText("Room: " + dbcon1.findOneQuery("r.room", "tbl_room r, tbl_schedule sc", "sc.room_id=r.id and sc.id='" + schedID_4 + "'"));
                        }

                    }
                }
            }
        }

        if (isUsed_5 && schedID_5!=-1) {
            for (int colIndex = 0; colIndex < col_reserved_5.length; colIndex++) {
                for (int rowIndex = 0; rowIndex < row_reserved_5.length; rowIndex++) {
                    if (column == col_reserved_5[colIndex] && row == row_reserved_5[rowIndex]) {
                        
                        dbConnection dbcon1 = new dbConnection();
                        String strColor="";
                        switch (colorBy) {
                            case 0:
                                //below -> for Color by Subject, LAB and LEC are same color
                                strColor = dbcon1.findOneQuery("sjc.color_index", "tbl_schedule sc,tbl_subject sub,tbl_subject_detail sjd,tbl_subject_color sjc", "sc.id='" + schedID_5 + "' and sc.subject_id=sjd.id and sjd.subject_id=sjc.subject_id");
                                break;
                                
                            case 1:
                                //below -> for Color by Room
                                strColor = dbcon1.findOneQuery("rmc.color_index", "tbl_schedule sc,tbl_room_color rmc", "sc.id='" + schedID_5 + "' and sc.room_id=rmc.room_id");
                                break;
                                
                            case 2:
                                //below -> for Color by Faculty
                                strColor = dbcon1.findOneQuery("fc.color_index", "tbl_schedule sc,tbl_faculty_color fc", "sc.id='" + schedID_5 + "' and sc.faculty_id=fc.faculty_id");
                                //below -> for Color by Class
                                //String strColor = dbcon1.findOneQuery("clc.color_index", "tbl_schedule sc,tbl_classlist_color clc", "sc.id='" + schedID_5 + "' and sc.classlist_id=clc.classlist_id and sc.section=clc.section");
                                break;
                                
                            case 3:
                                //below -> for Color by Class
                                strColor = dbcon1.findOneQuery("clc.color_index", "tbl_schedule sc,tbl_classlist_color clc", "sc.id='" + schedID_5 + "' and sc.classlist_id=clc.classlist_id and sc.section=clc.section");
                                break;
                                
                            default:
                                break;
                        }
                        
                        ColorClassHandler cch = new ColorClassHandler();
                        int colorIndex = Integer.parseInt(strColor);
                        if(colorIndex!=-1){
                            cellComponent.setBackground(cch.getColor(colorIndex));
                            cellComponent.setForeground(cch.getForeground(colorIndex));
                        }
                        
                        cellComponent.setToolTipText("<html><b>" + dbcon1.findOneQuery("CONCAT(b.address,' ',a.firstname,' ',a.middlename,' ',a.lastname,' ',c.extension)","tbl_faculty a, tbl_name_address b, tbl_name_extension c, tbl_schedule sc","a.address_id=b.id and a.extension_id=c.id and sc.faculty_id=a.id and sc.id='" + schedID_5 + "'") + "</b><br/><i>" + dbcon1.findOneQuery("sj.subject_description", "tbl_subject sj,tbl_subject_detail sjd,tbl_schedule sc", "sc.subject_id=sjd.id and sjd.subject_id=sj.id and sc.id='" + schedID_5 + "'") + "</i></html>");
                        
                        if (rowIndex % 2 == 0) {
                            if ((rowIndex + 1) != row_reserved_5.length) {
                                //dbConnection dbcon = new dbConnection();
                                cellComponent.setText(dbcon1.findOneQuery("CONCAT(sj.subject_code,' (',sjd.subject_type,')')", "tbl_subject sj,tbl_subject_detail sjd,tbl_schedule sc", "sc.subject_id=sjd.id and sjd.subject_id=sj.id and sc.id='" + schedID_5 + "'"));
                            }
                        } else {
                            //dbConnection dbcon = new dbConnection();
                            cellComponent.setText("Room: " + dbcon1.findOneQuery("r.room", "tbl_room r, tbl_schedule sc", "sc.room_id=r.id and sc.id='" + schedID_5 + "'"));
                        }

                    }
                }
            }
        }

        if (isUsed_6 && schedID_6!=-1) {
            for (int colIndex = 0; colIndex < col_reserved_6.length; colIndex++) {
                for (int rowIndex = 0; rowIndex < row_reserved_6.length; rowIndex++) {
                    if (column == col_reserved_6[colIndex] && row == row_reserved_6[rowIndex]) {
                        
                        dbConnection dbcon1 = new dbConnection();
                        String strColor="";
                        switch (colorBy) {
                            case 0:
                                //below -> for Color by Subject, LAB and LEC are same color
                                strColor = dbcon1.findOneQuery("sjc.color_index", "tbl_schedule sc,tbl_subject sub,tbl_subject_detail sjd,tbl_subject_color sjc", "sc.id='" + schedID_6 + "' and sc.subject_id=sjd.id and sjd.subject_id=sjc.subject_id");
                                break;
                                
                            case 1:
                                //below -> for Color by Room
                                strColor = dbcon1.findOneQuery("rmc.color_index", "tbl_schedule sc,tbl_room_color rmc", "sc.id='" + schedID_6 + "' and sc.room_id=rmc.room_id");
                                break;
                                
                            case 2:
                                //below -> for Color by Faculty
                                strColor = dbcon1.findOneQuery("fc.color_index", "tbl_schedule sc,tbl_faculty_color fc", "sc.id='" + schedID_6 + "' and sc.faculty_id=fc.faculty_id");
                                //below -> for Color by Class
                                //String strColor = dbcon1.findOneQuery("clc.color_index", "tbl_schedule sc,tbl_classlist_color clc", "sc.id='" + schedID_6 + "' and sc.classlist_id=clc.classlist_id and sc.section=clc.section");
                                break;
                                
                            case 3:
                                //below -> for Color by Class
                                strColor = dbcon1.findOneQuery("clc.color_index", "tbl_schedule sc,tbl_classlist_color clc", "sc.id='" + schedID_6 + "' and sc.classlist_id=clc.classlist_id and sc.section=clc.section");
                                break;
                                
                            default:
                                break;
                        }
                        
                        ColorClassHandler cch = new ColorClassHandler();
                        int colorIndex = Integer.parseInt(strColor);
                        if(colorIndex!=-1){
                            cellComponent.setBackground(cch.getColor(colorIndex));
                            cellComponent.setForeground(cch.getForeground(colorIndex));
                        }
                        
                        cellComponent.setToolTipText("<html><b>" + dbcon1.findOneQuery("CONCAT(b.address,' ',a.firstname,' ',a.middlename,' ',a.lastname,' ',c.extension)","tbl_faculty a, tbl_name_address b, tbl_name_extension c, tbl_schedule sc","a.address_id=b.id and a.extension_id=c.id and sc.faculty_id=a.id and sc.id='" + schedID_6 + "'") + "</b><br/><i>" + dbcon1.findOneQuery("sj.subject_description", "tbl_subject sj,tbl_subject_detail sjd,tbl_schedule sc", "sc.subject_id=sjd.id and sjd.subject_id=sj.id and sc.id='" + schedID_6 + "'") + "</i></html>");
                        
                        if (rowIndex % 2 == 0) {
                            if ((rowIndex + 1) != row_reserved_6.length) {
                                //dbConnection dbcon = new dbConnection();
                                cellComponent.setText(dbcon1.findOneQuery("CONCAT(sj.subject_code,' (',sjd.subject_type,')')", "tbl_subject sj,tbl_subject_detail sjd,tbl_schedule sc", "sc.subject_id=sjd.id and sjd.subject_id=sj.id and sc.id='" + schedID_6 + "'"));
                            }
                        } else {
                            //dbConnection dbcon = new dbConnection();
                            cellComponent.setText("Room: " + dbcon1.findOneQuery("r.room", "tbl_room r, tbl_schedule sc", "sc.room_id=r.id and sc.id='" + schedID_6 + "'"));
                        }

                    }
                }
            }
        }

        if (isUsed_7 && schedID_7!=-1) {
            for (int colIndex = 0; colIndex < col_reserved_7.length; colIndex++) {
                for (int rowIndex = 0; rowIndex < row_reserved_7.length; rowIndex++) {
                    if (column == col_reserved_7[colIndex] && row == row_reserved_7[rowIndex]) {
                        
                        dbConnection dbcon1 = new dbConnection();
                        String strColor="";
                        switch (colorBy) {
                            case 0:
                                //below -> for Color by Subject, LAB and LEC are same color
                                strColor = dbcon1.findOneQuery("sjc.color_index", "tbl_schedule sc,tbl_subject sub,tbl_subject_detail sjd,tbl_subject_color sjc", "sc.id='" + schedID_7 + "' and sc.subject_id=sjd.id and sjd.subject_id=sjc.subject_id");
                                break;
                                
                            case 1:
                                //below -> for Color by Room
                                strColor = dbcon1.findOneQuery("rmc.color_index", "tbl_schedule sc,tbl_room_color rmc", "sc.id='" + schedID_7 + "' and sc.room_id=rmc.room_id");
                                break;
                                
                            case 2:
                                //below -> for Color by Faculty
                                strColor = dbcon1.findOneQuery("fc.color_index", "tbl_schedule sc,tbl_faculty_color fc", "sc.id='" + schedID_7 + "' and sc.faculty_id=fc.faculty_id");
                                //below -> for Color by Class
                                //String strColor = dbcon1.findOneQuery("clc.color_index", "tbl_schedule sc,tbl_classlist_color clc", "sc.id='" + schedID_7 + "' and sc.classlist_id=clc.classlist_id and sc.section=clc.section");
                                break;
                                
                            case 3:
                                //below -> for Color by Class
                                strColor = dbcon1.findOneQuery("clc.color_index", "tbl_schedule sc,tbl_classlist_color clc", "sc.id='" + schedID_7 + "' and sc.classlist_id=clc.classlist_id and sc.section=clc.section");
                                break;
                                
                            default:
                                break;
                        }
                        
                        ColorClassHandler cch = new ColorClassHandler();
                        int colorIndex = Integer.parseInt(strColor);
                        if(colorIndex!=-1){
                            cellComponent.setBackground(cch.getColor(colorIndex));
                            cellComponent.setForeground(cch.getForeground(colorIndex));
                        }
                        
                        cellComponent.setToolTipText("<html><b>" + dbcon1.findOneQuery("CONCAT(b.address,' ',a.firstname,' ',a.middlename,' ',a.lastname,' ',c.extension)","tbl_faculty a, tbl_name_address b, tbl_name_extension c, tbl_schedule sc","a.address_id=b.id and a.extension_id=c.id and sc.faculty_id=a.id and sc.id='" + schedID_7 + "'") + "</b><br/><i>" + dbcon1.findOneQuery("sj.subject_description", "tbl_subject sj,tbl_subject_detail sjd,tbl_schedule sc", "sc.subject_id=sjd.id and sjd.subject_id=sj.id and sc.id='" + schedID_7 + "'") + "</i></html>");
                        
                        if (rowIndex % 2 == 0) {
                            if ((rowIndex + 1) != row_reserved_7.length) {
                                //dbConnection dbcon = new dbConnection();
                                cellComponent.setText(dbcon1.findOneQuery("CONCAT(sj.subject_code,' (',sjd.subject_type,')')", "tbl_subject sj,tbl_subject_detail sjd,tbl_schedule sc", "sc.subject_id=sjd.id and sjd.subject_id=sj.id and sc.id='" + schedID_7 + "'"));
                            }
                        } else {
                            //dbConnection dbcon = new dbConnection();
                            cellComponent.setText("Room: " + dbcon1.findOneQuery("r.room", "tbl_room r, tbl_schedule sc", "sc.room_id=r.id and sc.id='" + schedID_7 + "'"));
                        }

                    }
                }
            }
        }

        if (isUsed_8 && schedID_8!=-1) {
            for (int colIndex = 0; colIndex < col_reserved_8.length; colIndex++) {
                for (int rowIndex = 0; rowIndex < row_reserved_8.length; rowIndex++) {
                    if (column == col_reserved_8[colIndex] && row == row_reserved_8[rowIndex]) {
                        
                        dbConnection dbcon1 = new dbConnection();
                        String strColor="";
                        switch (colorBy) {
                            case 0:
                                //below -> for Color by Subject, LAB and LEC are same color
                                strColor = dbcon1.findOneQuery("sjc.color_index", "tbl_schedule sc,tbl_subject sub,tbl_subject_detail sjd,tbl_subject_color sjc", "sc.id='" + schedID_8 + "' and sc.subject_id=sjd.id and sjd.subject_id=sjc.subject_id");
                                break;
                                
                            case 1:
                                //below -> for Color by Room
                                strColor = dbcon1.findOneQuery("rmc.color_index", "tbl_schedule sc,tbl_room_color rmc", "sc.id='" + schedID_8 + "' and sc.room_id=rmc.room_id");
                                break;
                                
                            case 2:
                                //below -> for Color by Faculty
                                strColor = dbcon1.findOneQuery("fc.color_index", "tbl_schedule sc,tbl_faculty_color fc", "sc.id='" + schedID_8 + "' and sc.faculty_id=fc.faculty_id");
                                //below -> for Color by Class
                                //String strColor = dbcon1.findOneQuery("clc.color_index", "tbl_schedule sc,tbl_classlist_color clc", "sc.id='" + schedID_8 + "' and sc.classlist_id=clc.classlist_id and sc.section=clc.section");
                                break;
                                
                            case 3:
                                //below -> for Color by Class
                                strColor = dbcon1.findOneQuery("clc.color_index", "tbl_schedule sc,tbl_classlist_color clc", "sc.id='" + schedID_8 + "' and sc.classlist_id=clc.classlist_id and sc.section=clc.section");
                                break;
                                
                            default:
                                break;
                        }
                        
                        ColorClassHandler cch = new ColorClassHandler();
                        int colorIndex = Integer.parseInt(strColor);
                        if(colorIndex!=-1){
                            cellComponent.setBackground(cch.getColor(colorIndex));
                            cellComponent.setForeground(cch.getForeground(colorIndex));
                        }
                        
                        cellComponent.setToolTipText("<html><b>" + dbcon1.findOneQuery("CONCAT(b.address,' ',a.firstname,' ',a.middlename,' ',a.lastname,' ',c.extension)","tbl_faculty a, tbl_name_address b, tbl_name_extension c, tbl_schedule sc","a.address_id=b.id and a.extension_id=c.id and sc.faculty_id=a.id and sc.id='" + schedID_8 + "'") + "</b><br/><i>" + dbcon1.findOneQuery("sj.subject_description", "tbl_subject sj,tbl_subject_detail sjd,tbl_schedule sc", "sc.subject_id=sjd.id and sjd.subject_id=sj.id and sc.id='" + schedID_8 + "'") + "</i></html>");
                        
                        if (rowIndex % 2 == 0) {
                            if ((rowIndex + 1) != row_reserved_8.length) {
                                //dbConnection dbcon = new dbConnection();
                                cellComponent.setText(dbcon1.findOneQuery("CONCAT(sj.subject_code,' (',sjd.subject_type,')')", "tbl_subject sj,tbl_subject_detail sjd,tbl_schedule sc", "sc.subject_id=sjd.id and sjd.subject_id=sj.id and sc.id='" + schedID_8 + "'"));
                            }
                        } else {
                            //dbConnection dbcon = new dbConnection();
                            cellComponent.setText("Room: " + dbcon1.findOneQuery("r.room", "tbl_room r, tbl_schedule sc", "sc.room_id=r.id and sc.id='" + schedID_8 + "'"));
                        }

                    }
                }
            }
        }

        if (isUsed_9 && schedID_9!=-1) {
            for (int colIndex = 0; colIndex < col_reserved_9.length; colIndex++) {
                for (int rowIndex = 0; rowIndex < row_reserved_9.length; rowIndex++) {
                    if (column == col_reserved_9[colIndex] && row == row_reserved_9[rowIndex]) {
                        
                        dbConnection dbcon1 = new dbConnection();
                        String strColor="";
                        switch (colorBy) {
                            case 0:
                                //below -> for Color by Subject, LAB and LEC are same color
                                strColor = dbcon1.findOneQuery("sjc.color_index", "tbl_schedule sc,tbl_subject sub,tbl_subject_detail sjd,tbl_subject_color sjc", "sc.id='" + schedID_9 + "' and sc.subject_id=sjd.id and sjd.subject_id=sjc.subject_id");
                                break;
                                
                            case 1:
                                //below -> for Color by Room
                                strColor = dbcon1.findOneQuery("rmc.color_index", "tbl_schedule sc,tbl_room_color rmc", "sc.id='" + schedID_9 + "' and sc.room_id=rmc.room_id");
                                break;
                                
                            case 2:
                                //below -> for Color by Faculty
                                strColor = dbcon1.findOneQuery("fc.color_index", "tbl_schedule sc,tbl_faculty_color fc", "sc.id='" + schedID_9 + "' and sc.faculty_id=fc.faculty_id");
                                //below -> for Color by Class
                                //String strColor = dbcon1.findOneQuery("clc.color_index", "tbl_schedule sc,tbl_classlist_color clc", "sc.id='" + schedID_9 + "' and sc.classlist_id=clc.classlist_id and sc.section=clc.section");
                                break;
                               
                            case 3:
                                //below -> for Color by Class
                                strColor = dbcon1.findOneQuery("clc.color_index", "tbl_schedule sc,tbl_classlist_color clc", "sc.id='" + schedID_9 + "' and sc.classlist_id=clc.classlist_id and sc.section=clc.section");
                                break;
                                
                            default:
                                break;
                        }
                        
                        ColorClassHandler cch = new ColorClassHandler();
                        int colorIndex = Integer.parseInt(strColor);
                        if(colorIndex!=-1){
                            cellComponent.setBackground(cch.getColor(colorIndex));
                            cellComponent.setForeground(cch.getForeground(colorIndex));
                        }
                        
                        cellComponent.setToolTipText("<html><b>" + dbcon1.findOneQuery("CONCAT(b.address,' ',a.firstname,' ',a.middlename,' ',a.lastname,' ',c.extension)","tbl_faculty a, tbl_name_address b, tbl_name_extension c, tbl_schedule sc","a.address_id=b.id and a.extension_id=c.id and sc.faculty_id=a.id and sc.id='" + schedID_9 + "'") + "</b><br/><i>" + dbcon1.findOneQuery("sj.subject_description", "tbl_subject sj,tbl_subject_detail sjd,tbl_schedule sc", "sc.subject_id=sjd.id and sjd.subject_id=sj.id and sc.id='" + schedID_9 + "'") + "</i></html>");
                        
                        if (rowIndex % 2 == 0) {
                            if ((rowIndex + 1) != row_reserved_9.length) {
                                //dbConnection dbcon = new dbConnection();
                                cellComponent.setText(dbcon1.findOneQuery("CONCAT(sj.subject_code,' (',sjd.subject_type,')')", "tbl_subject sj,tbl_subject_detail sjd,tbl_schedule sc", "sc.subject_id=sjd.id and sjd.subject_id=sj.id and sc.id='" + schedID_9 + "'"));
                            }
                        } else {
                            //dbConnection dbcon = new dbConnection();
                            cellComponent.setText("Room: " + dbcon1.findOneQuery("r.room", "tbl_room r, tbl_schedule sc", "sc.room_id=r.id and sc.id='" + schedID_9 + "'"));
                        }

                    }
                }
            }
        }

        if (isUsed_10 && schedID_10!=-1) {
            for (int colIndex = 0; colIndex < col_reserved_10.length; colIndex++) {
                for (int rowIndex = 0; rowIndex < row_reserved_10.length; rowIndex++) {
                    if (column == col_reserved_10[colIndex] && row == row_reserved_10[rowIndex]) {
                        
                        dbConnection dbcon1 = new dbConnection();
                        String strColor="";
                        switch (colorBy) {
                            case 0:
                                //below -> for Color by Subject, LAB and LEC are same color
                                strColor = dbcon1.findOneQuery("sjc.color_index", "tbl_schedule sc,tbl_subject sub,tbl_subject_detail sjd,tbl_subject_color sjc", "sc.id='" + schedID_10 + "' and sc.subject_id=sjd.id and sjd.subject_id=sjc.subject_id");
                                break;
                                
                            case 1:
                                //below -> for Color by Room
                                strColor = dbcon1.findOneQuery("rmc.color_index", "tbl_schedule sc,tbl_room_color rmc", "sc.id='" + schedID_10 + "' and sc.room_id=rmc.room_id");
                                break;
                                
                            case 2:
                                //below -> for Color by Faculty
                                strColor = dbcon1.findOneQuery("fc.color_index", "tbl_schedule sc,tbl_faculty_color fc", "sc.id='" + schedID_10 + "' and sc.faculty_id=fc.faculty_id");
                                //below -> for Color by Class
                                //String strColor = dbcon1.findOneQuery("clc.color_index", "tbl_schedule sc,tbl_classlist_color clc", "sc.id='" + schedID_10 + "' and sc.classlist_id=clc.classlist_id and sc.section=clc.section");
                                break;
                                
                            case 3:
                                //below -> for Color by Class
                                strColor = dbcon1.findOneQuery("clc.color_index", "tbl_schedule sc,tbl_classlist_color clc", "sc.id='" + schedID_10 + "' and sc.classlist_id=clc.classlist_id and sc.section=clc.section");
                                break;
                                
                            default:
                                break;
                        }
                        
                        ColorClassHandler cch = new ColorClassHandler();
                        int colorIndex = Integer.parseInt(strColor);
                        if(colorIndex!=-1){
                            cellComponent.setBackground(cch.getColor(colorIndex));
                            cellComponent.setForeground(cch.getForeground(colorIndex));
                        }
                        
                        cellComponent.setToolTipText("<html><b>" + dbcon1.findOneQuery("CONCAT(b.address,' ',a.firstname,' ',a.middlename,' ',a.lastname,' ',c.extension)","tbl_faculty a, tbl_name_address b, tbl_name_extension c, tbl_schedule sc","a.address_id=b.id and a.extension_id=c.id and sc.faculty_id=a.id and sc.id='" + schedID_10 + "'") + "</b><br/><i>" + dbcon1.findOneQuery("sj.subject_description", "tbl_subject sj,tbl_subject_detail sjd,tbl_schedule sc", "sc.subject_id=sjd.id and sjd.subject_id=sj.id and sc.id='" + schedID_10 + "'") + "</i></html>");
                        
                        if (rowIndex % 2 == 0) {
                            if ((rowIndex + 1) != row_reserved_10.length) {
                                //dbConnection dbcon = new dbConnection();
                                cellComponent.setText(dbcon1.findOneQuery("CONCAT(sj.subject_code,' (',sjd.subject_type,')')", "tbl_subject sj,tbl_subject_detail sjd,tbl_schedule sc", "sc.subject_id=sjd.id and sjd.subject_id=sj.id and sc.id='" + schedID_10 + "'"));
                            }
                        } else {
                            //dbConnection dbcon = new dbConnection();
                            cellComponent.setText("Room: " + dbcon1.findOneQuery("r.room", "tbl_room r, tbl_schedule sc", "sc.room_id=r.id and sc.id='" + schedID_10 + "'"));
                        }

                    }
                }
            }
        }

        if (isUsed_11 && schedID_11!=-1) {
            for (int colIndex = 0; colIndex < col_reserved_11.length; colIndex++) {
                for (int rowIndex = 0; rowIndex < row_reserved_11.length; rowIndex++) {
                    if (column == col_reserved_11[colIndex] && row == row_reserved_11[rowIndex]) {
                        
                        dbConnection dbcon1 = new dbConnection();
                        String strColor="";
                        switch (colorBy) {
                            case 0:
                                //below -> for Color by Subject, LAB and LEC are same color
                                strColor = dbcon1.findOneQuery("sjc.color_index", "tbl_schedule sc,tbl_subject sub,tbl_subject_detail sjd,tbl_subject_color sjc", "sc.id='" + schedID_11 + "' and sc.subject_id=sjd.id and sjd.subject_id=sjc.subject_id");
                                break;
                                
                            case 1:
                                //below -> for Color by Room
                                strColor = dbcon1.findOneQuery("rmc.color_index", "tbl_schedule sc,tbl_room_color rmc", "sc.id='" + schedID_11 + "' and sc.room_id=rmc.room_id");
                                break;
                                
                            case 2:
                                //below -> for Color by Faculty
                                strColor = dbcon1.findOneQuery("fc.color_index", "tbl_schedule sc,tbl_faculty_color fc", "sc.id='" + schedID_11 + "' and sc.faculty_id=fc.faculty_id");
                                //below -> for Color by Class
                                //String strColor = dbcon1.findOneQuery("clc.color_index", "tbl_schedule sc,tbl_classlist_color clc", "sc.id='" + schedID_11 + "' and sc.classlist_id=clc.classlist_id and sc.section=clc.section");
                                break;
                                
                            case 3:
                                //below -> for Color by Class
                                strColor = dbcon1.findOneQuery("clc.color_index", "tbl_schedule sc,tbl_classlist_color clc", "sc.id='" + schedID_11 + "' and sc.classlist_id=clc.classlist_id and sc.section=clc.section");
                                break;
                                
                            default:
                                break;
                        }
                        
                        ColorClassHandler cch = new ColorClassHandler();
                        int colorIndex = Integer.parseInt(strColor);
                        if(colorIndex!=-1){
                            cellComponent.setBackground(cch.getColor(colorIndex));
                            cellComponent.setForeground(cch.getForeground(colorIndex));
                        }
                        
                        cellComponent.setToolTipText("<html><b>" + dbcon1.findOneQuery("CONCAT(b.address,' ',a.firstname,' ',a.middlename,' ',a.lastname,' ',c.extension)","tbl_faculty a, tbl_name_address b, tbl_name_extension c, tbl_schedule sc","a.address_id=b.id and a.extension_id=c.id and sc.faculty_id=a.id and sc.id='" + schedID_11 + "'") + "</b><br/><i>" + dbcon1.findOneQuery("sj.subject_description", "tbl_subject sj,tbl_subject_detail sjd,tbl_schedule sc", "sc.subject_id=sjd.id and sjd.subject_id=sj.id and sc.id='" + schedID_11 + "'") + "</i></html>");
                        
                        if (rowIndex % 2 == 0) {
                            if ((rowIndex + 1) != row_reserved_11.length) {
                                //dbConnection dbcon = new dbConnection();
                                cellComponent.setText(dbcon1.findOneQuery("CONCAT(sj.subject_code,' (',sjd.subject_type,')')", "tbl_subject sj,tbl_subject_detail sjd,tbl_schedule sc", "sc.subject_id=sjd.id and sjd.subject_id=sj.id and sc.id='" + schedID_11 + "'"));
                            }
                        } else {
                            //dbConnection dbcon = new dbConnection();
                            cellComponent.setText("Room: " + dbcon1.findOneQuery("r.room", "tbl_room r, tbl_schedule sc", "sc.room_id=r.id and sc.id='" + schedID_11 + "'"));
                        }

                    }
                }
            }
        }

        if (isUsed_12 && schedID_12!=-1) {
            for (int colIndex = 0; colIndex < col_reserved_12.length; colIndex++) {
                for (int rowIndex = 0; rowIndex < row_reserved_12.length; rowIndex++) {
                    if (column == col_reserved_12[colIndex] && row == row_reserved_12[rowIndex]) {
                        
                        dbConnection dbcon1 = new dbConnection();
                        String strColor="";
                        switch (colorBy) {
                            case 0:
                                //below -> for Color by Subject, LAB and LEC are same color
                                strColor = dbcon1.findOneQuery("sjc.color_index", "tbl_schedule sc,tbl_subject sub,tbl_subject_detail sjd,tbl_subject_color sjc", "sc.id='" + schedID_12 + "' and sc.subject_id=sjd.id and sjd.subject_id=sjc.subject_id");
                                break;
                                
                            case 1:
                                //below -> for Color by Room
                                strColor = dbcon1.findOneQuery("rmc.color_index", "tbl_schedule sc,tbl_room_color rmc", "sc.id='" + schedID_12 + "' and sc.room_id=rmc.room_id");
                                break;
                                
                            case 2:
                                //below -> for Color by Faculty
                                strColor = dbcon1.findOneQuery("fc.color_index", "tbl_schedule sc,tbl_faculty_color fc", "sc.id='" + schedID_12 + "' and sc.faculty_id=fc.faculty_id");
                                //below -> for Color by Class
                                //String strColor = dbcon1.findOneQuery("clc.color_index", "tbl_schedule sc,tbl_classlist_color clc", "sc.id='" + schedID_12 + "' and sc.classlist_id=clc.classlist_id and sc.section=clc.section");
                                break;
                                
                            case 3:
                                //below -> for Color by Class
                                strColor = dbcon1.findOneQuery("clc.color_index", "tbl_schedule sc,tbl_classlist_color clc", "sc.id='" + schedID_12 + "' and sc.classlist_id=clc.classlist_id and sc.section=clc.section");
                                break;
                                
                            default:
                                break;
                        }
                        
                        ColorClassHandler cch = new ColorClassHandler();
                        int colorIndex = Integer.parseInt(strColor);
                        if(colorIndex!=-1){
                            cellComponent.setBackground(cch.getColor(colorIndex));
                            cellComponent.setForeground(cch.getForeground(colorIndex));
                        }
                        
                        cellComponent.setToolTipText("<html><b>" + dbcon1.findOneQuery("CONCAT(b.address,' ',a.firstname,' ',a.middlename,' ',a.lastname,' ',c.extension)","tbl_faculty a, tbl_name_address b, tbl_name_extension c, tbl_schedule sc","a.address_id=b.id and a.extension_id=c.id and sc.faculty_id=a.id and sc.id='" + schedID_12 + "'") + "</b><br/><i>" + dbcon1.findOneQuery("sj.subject_description", "tbl_subject sj,tbl_subject_detail sjd,tbl_schedule sc", "sc.subject_id=sjd.id and sjd.subject_id=sj.id and sc.id='" + schedID_12 + "'") + "</i></html>");
                        
                        if (rowIndex % 2 == 0) {
                            if ((rowIndex + 1) != row_reserved_12.length) {
                                //dbConnection dbcon = new dbConnection();
                                cellComponent.setText(dbcon1.findOneQuery("CONCAT(sj.subject_code,' (',sjd.subject_type,')')", "tbl_subject sj,tbl_subject_detail sjd,tbl_schedule sc", "sc.subject_id=sjd.id and sjd.subject_id=sj.id and sc.id='" + schedID_12 + "'"));
                            }
                        } else {
                            //dbConnection dbcon = new dbConnection();
                            cellComponent.setText("Room: " + dbcon1.findOneQuery("r.room", "tbl_room r, tbl_schedule sc", "sc.room_id=r.id and sc.id='" + schedID_12 + "'"));
                        }

                    }
                }
            }
        }

        if (isUsed_13 && schedID_13!=-1) {
            for (int colIndex = 0; colIndex < col_reserved_13.length; colIndex++) {
                for (int rowIndex = 0; rowIndex < row_reserved_13.length; rowIndex++) {
                    if (column == col_reserved_13[colIndex] && row == row_reserved_13[rowIndex]) {
                        
                        dbConnection dbcon1 = new dbConnection();
                        String strColor="";
                        switch (colorBy) {
                            case 0:
                                //below -> for Color by Subject, LAB and LEC are same color
                                strColor = dbcon1.findOneQuery("sjc.color_index", "tbl_schedule sc,tbl_subject sub,tbl_subject_detail sjd,tbl_subject_color sjc", "sc.id='" + schedID_13 + "' and sc.subject_id=sjd.id and sjd.subject_id=sjc.subject_id");
                                break;
                                
                            case 1:
                                //below -> for Color by Room
                                strColor = dbcon1.findOneQuery("rmc.color_index", "tbl_schedule sc,tbl_room_color rmc", "sc.id='" + schedID_13 + "' and sc.room_id=rmc.room_id");
                                break;
                                
                            case 2:
                                //below -> for Color by Faculty
                                strColor = dbcon1.findOneQuery("fc.color_index", "tbl_schedule sc,tbl_faculty_color fc", "sc.id='" + schedID_13 + "' and sc.faculty_id=fc.faculty_id");
                                //below -> for Color by Class
                                //String strColor = dbcon1.findOneQuery("clc.color_index", "tbl_schedule sc,tbl_classlist_color clc", "sc.id='" + schedID_13 + "' and sc.classlist_id=clc.classlist_id and sc.section=clc.section");
                                break;
                                
                            case 3:
                                //below -> for Color by Class
                                strColor = dbcon1.findOneQuery("clc.color_index", "tbl_schedule sc,tbl_classlist_color clc", "sc.id='" + schedID_13 + "' and sc.classlist_id=clc.classlist_id and sc.section=clc.section");
                                break;
                                
                            default:
                                break;
                        }
                        
                        ColorClassHandler cch = new ColorClassHandler();
                        int colorIndex = Integer.parseInt(strColor);
                        if(colorIndex!=-1){
                            cellComponent.setBackground(cch.getColor(colorIndex));
                            cellComponent.setForeground(cch.getForeground(colorIndex));
                        }
                        
                        cellComponent.setToolTipText("<html><b>" + dbcon1.findOneQuery("CONCAT(b.address,' ',a.firstname,' ',a.middlename,' ',a.lastname,' ',c.extension)","tbl_faculty a, tbl_name_address b, tbl_name_extension c, tbl_schedule sc","a.address_id=b.id and a.extension_id=c.id and sc.faculty_id=a.id and sc.id='" + schedID_13 + "'") + "</b><br/><i>" + dbcon1.findOneQuery("sj.subject_description", "tbl_subject sj,tbl_subject_detail sjd,tbl_schedule sc", "sc.subject_id=sjd.id and sjd.subject_id=sj.id and sc.id='" + schedID_13 + "'") + "</i></html>");
                        
                        if (rowIndex % 2 == 0) {
                            if ((rowIndex + 1) != row_reserved_13.length) {
                                //dbConnection dbcon = new dbConnection();
                                cellComponent.setText(dbcon1.findOneQuery("CONCAT(sj.subject_code,' (',sjd.subject_type,')')", "tbl_subject sj,tbl_subject_detail sjd,tbl_schedule sc", "sc.subject_id=sjd.id and sjd.subject_id=sj.id and sc.id='" + schedID_13 + "'"));
                            }
                        } else {
                            //dbConnection dbcon = new dbConnection();
                            cellComponent.setText("Room: " + dbcon1.findOneQuery("r.room", "tbl_room r, tbl_schedule sc", "sc.room_id=r.id and sc.id='" + schedID_13 + "'"));
                        }

                    }
                }
            }
        }

        if (isUsed_14 && schedID_14!=-1) {
            for (int colIndex = 0; colIndex < col_reserved_14.length; colIndex++) {
                for (int rowIndex = 0; rowIndex < row_reserved_14.length; rowIndex++) {
                    if (column == col_reserved_14[colIndex] && row == row_reserved_14[rowIndex]) {
                        
                        dbConnection dbcon1 = new dbConnection();
                        String strColor="";
                        switch (colorBy) {
                            case 0:
                                //below -> for Color by Subject, LAB and LEC are same color
                                strColor = dbcon1.findOneQuery("sjc.color_index", "tbl_schedule sc,tbl_subject sub,tbl_subject_detail sjd,tbl_subject_color sjc", "sc.id='" + schedID_14 + "' and sc.subject_id=sjd.id and sjd.subject_id=sjc.subject_id");
                                break;
                                
                            case 1:
                                //below -> for Color by Room
                                strColor = dbcon1.findOneQuery("rmc.color_index", "tbl_schedule sc,tbl_room_color rmc", "sc.id='" + schedID_14 + "' and sc.room_id=rmc.room_id");
                                break;
                                
                            case 2:
                                //below -> for Color by Faculty
                                strColor = dbcon1.findOneQuery("fc.color_index", "tbl_schedule sc,tbl_faculty_color fc", "sc.id='" + schedID_14 + "' and sc.faculty_id=fc.faculty_id");
                                //below -> for Color by Class
                                //String strColor = dbcon1.findOneQuery("clc.color_index", "tbl_schedule sc,tbl_classlist_color clc", "sc.id='" + schedID_14 + "' and sc.classlist_id=clc.classlist_id and sc.section=clc.section");
                                break;
                                
                            case 3:
                                //below -> for Color by Class
                                strColor = dbcon1.findOneQuery("clc.color_index", "tbl_schedule sc,tbl_classlist_color clc", "sc.id='" + schedID_14 + "' and sc.classlist_id=clc.classlist_id and sc.section=clc.section");
                                break;
                                
                            default:
                                break;
                        }
                        
                        ColorClassHandler cch = new ColorClassHandler();
                        int colorIndex = Integer.parseInt(strColor);
                        if(colorIndex!=-1){
                            cellComponent.setBackground(cch.getColor(colorIndex));
                            cellComponent.setForeground(cch.getForeground(colorIndex));
                        }
                        
                        cellComponent.setToolTipText("<html><b>" + dbcon1.findOneQuery("CONCAT(b.address,' ',a.firstname,' ',a.middlename,' ',a.lastname,' ',c.extension)","tbl_faculty a, tbl_name_address b, tbl_name_extension c, tbl_schedule sc","a.address_id=b.id and a.extension_id=c.id and sc.faculty_id=a.id and sc.id='" + schedID_14 + "'") + "</b><br/><i>" + dbcon1.findOneQuery("sj.subject_description", "tbl_subject sj,tbl_subject_detail sjd,tbl_schedule sc", "sc.subject_id=sjd.id and sjd.subject_id=sj.id and sc.id='" + schedID_14 + "'") + "</i></html>");
                        
                        if (rowIndex % 2 == 0) {
                            if ((rowIndex + 1) != row_reserved_14.length) {
                                //dbConnection dbcon = new dbConnection();
                                cellComponent.setText(dbcon1.findOneQuery("CONCAT(sj.subject_code,' (',sjd.subject_type,')')", "tbl_subject sj,tbl_subject_detail sjd,tbl_schedule sc", "sc.subject_id=sjd.id and sjd.subject_id=sj.id and sc.id='" + schedID_14 + "'"));
                            }
                        } else {
                            //dbConnection dbcon = new dbConnection();
                            cellComponent.setText("Room: " + dbcon1.findOneQuery("r.room", "tbl_room r, tbl_schedule sc", "sc.room_id=r.id and sc.id='" + schedID_14 + "'"));
                        }

                    }
                }
            }
        }

        if (isUsed_15 && schedID_15!=-1) {
            for (int colIndex = 0; colIndex < col_reserved_15.length; colIndex++) {
                for (int rowIndex = 0; rowIndex < row_reserved_15.length; rowIndex++) {
                    if (column == col_reserved_15[colIndex] && row == row_reserved_15[rowIndex]) {
                        
                        dbConnection dbcon1 = new dbConnection();
                        String strColor="";
                        switch (colorBy) {
                            case 0:
                                //below -> for Color by Subject, LAB and LEC are same color
                                strColor = dbcon1.findOneQuery("sjc.color_index", "tbl_schedule sc,tbl_subject sub,tbl_subject_detail sjd,tbl_subject_color sjc", "sc.id='" + schedID_15 + "' and sc.subject_id=sjd.id and sjd.subject_id=sjc.subject_id");
                                break;
                                
                            case 1:
                                //below -> for Color by Room
                                strColor = dbcon1.findOneQuery("rmc.color_index", "tbl_schedule sc,tbl_room_color rmc", "sc.id='" + schedID_15 + "' and sc.room_id=rmc.room_id");
                                break;
                                
                            case 2:
                                //below -> for Color by Faculty
                                strColor = dbcon1.findOneQuery("fc.color_index", "tbl_schedule sc,tbl_faculty_color fc", "sc.id='" + schedID_15 + "' and sc.faculty_id=fc.faculty_id");
                                //below -> for Color by Class
                                //String strColor = dbcon1.findOneQuery("clc.color_index", "tbl_schedule sc,tbl_classlist_color clc", "sc.id='" + schedID_15 + "' and sc.classlist_id=clc.classlist_id and sc.section=clc.section");
                                break;
                                
                            case 3:
                                //below -> for Color by Class
                                strColor = dbcon1.findOneQuery("clc.color_index", "tbl_schedule sc,tbl_classlist_color clc", "sc.id='" + schedID_15 + "' and sc.classlist_id=clc.classlist_id and sc.section=clc.section");
                                break;
                                
                            default:
                                break;
                        }
                        
                        ColorClassHandler cch = new ColorClassHandler();
                        int colorIndex = Integer.parseInt(strColor);
                        if(colorIndex!=-1){
                            cellComponent.setBackground(cch.getColor(colorIndex));
                            cellComponent.setForeground(cch.getForeground(colorIndex));
                        }
                        
                        cellComponent.setToolTipText("<html><b>" + dbcon1.findOneQuery("CONCAT(b.address,' ',a.firstname,' ',a.middlename,' ',a.lastname,' ',c.extension)","tbl_faculty a, tbl_name_address b, tbl_name_extension c, tbl_schedule sc","a.address_id=b.id and a.extension_id=c.id and sc.faculty_id=a.id and sc.id='" + schedID_15 + "'") + "</b><br/><i>" + dbcon1.findOneQuery("sj.subject_description", "tbl_subject sj,tbl_subject_detail sjd,tbl_schedule sc", "sc.subject_id=sjd.id and sjd.subject_id=sj.id and sc.id='" + schedID_15 + "'") + "</i></html>");
                        
                        if (rowIndex % 2 == 0) {
                            if ((rowIndex + 1) != row_reserved_15.length) {
                                //dbConnection dbcon = new dbConnection();
                                cellComponent.setText(dbcon1.findOneQuery("CONCAT(sj.subject_code,' (',sjd.subject_type,')')", "tbl_subject sj,tbl_subject_detail sjd,tbl_schedule sc", "sc.subject_id=sjd.id and sjd.subject_id=sj.id and sc.id='" + schedID_15 + "'"));
                            }
                        } else {
                            //dbConnection dbcon = new dbConnection();
                            cellComponent.setText("Room: " + dbcon1.findOneQuery("r.room", "tbl_room r, tbl_schedule sc", "sc.room_id=r.id and sc.id='" + schedID_15 + "'"));
                        }

                    }
                }
            }
        }

        cellComponent.setHorizontalAlignment(CENTER);
        return cellComponent;
    }
}
