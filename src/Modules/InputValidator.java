/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modules;

import java.awt.Component;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

/**
 *
 * @author cl5admin
 */
public class InputValidator {   
    public boolean validateIsEmptyComponent(Component C){
        if (C instanceof JTextField || C instanceof JTextArea){
            if(((JTextComponent) C).getText().trim().length()==0){
                ((JTextComponent) C).setBackground(new java.awt.Color(255, 204, 204));
                ((JTextComponent) C).setToolTipText("This field is required.");
                return false;
            }else{
                ((JTextComponent) C).setBackground(new java.awt.Color(255,255,255));
                ((JTextComponent) C).setToolTipText(((JTextComponent) C).getText());
                return true;
            }
        }else{
            return true;
        }
    }
    
    /*public boolean validateIsEmptyAll(JPanel jp){
         for (Component C : jp.getComponents()){    
            if (C instanceof JTextField || C instanceof JTextArea){
                if(((JTextComponent) C).getText().trim().length()==0){
                    ((JTextComponent) C).requestFocus();
                    return false;
                }
            }
        }
        return true;
    }*/
    /*public boolean validateIsEmptyAll(JPanel jp){
        //Component C[] = jp.getComponents();
        //System.out.println( ((JTextArea) jp.getComponent(0)).getText() );
         for (int i=0;i<jp.getComponentCount();i++){    
            Component C = jp.getComponent(i);
            if (C instanceof JTextField){
                if(((JTextField) C).getText().trim().length()==0){
                    //((JTextField) C).requestFocus();
                    System.out.println("TextField");
                    //return false;
                }
            }else if(C instanceof JTextArea){
                if(((JTextArea) C).getText().trim().length()==0){
                    //((JTextArea) C).requestFocus();
                    System.out.println("TextArea");
                    //return false;
                }
            }
        }
        return false;
    }*/
   
}
