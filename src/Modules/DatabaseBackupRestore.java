/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modules;

import AppPackage.dbConnection;
import java.awt.HeadlessException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

/**
 *
 * @author Jojo
 */
public class DatabaseBackupRestore {
    public static JProgressBar pb;
    public static void BackupdbToSql(String dir, String filename, String execFile) {
        try {

            /*NOTE: Getting path to the Jar file being executed*/
            /*NOTE: YourImplementingClass-> replace with the class executing the code*/
            //CodeSource codeSource = DatabaseBackupRestore.class.getProtectionDomain().getCodeSource();
            //File jarFile = new File(codeSource.getLocation().toURI().getPath());
            //String jarDir = jarFile.getParentFile().getPath();
            
            //----- increment progress ------------------
            incrementPBar(pb.getValue(),20);
            //-------------------------------------------
            /*NOTE: Creating Database Constraints*/
            dbConnection dbcon = new dbConnection();
            String dbName = dbcon.getDB();
            String dbUser = dbcon.getUID();
            String dbPass = dbcon.getPWD();

            /*NOTE: Creating Path Constraints for folder saving*/
            /*NOTE: Here the backup folder is created for saving inside it*/
            //String folderPath = jarDir + "\\backup";
            
            /*NOTE: Creating Folder if it does not exist*/
            //File f1 = new File(dir);
            //f1.mkdir();

            /*NOTE: Creating Path Constraints for backup saving*/
            /*NOTE: Here the backup is saved in a folder called backup with the name backup.sql*/
            //String fileName = new GetterSetterClass().getDateTime();
            String savePath = "\"" + dir + "\\" + filename + "\"";
            
            //----- increment progress ------------------
            incrementPBar(pb.getValue(),30);
            //-------------------------------------------
            /*NOTE: Used to create a cmd command*/
            String port = dbcon.getPORT();
            String ip = dbcon.getSHORTIP();
            String executeCmd = execFile + " --host=" + ip + " --port=" + port + " -u" + dbUser + " -p" + dbPass + " --database " + dbName + " -r " + savePath;
            
            //----- increment progress ------------------
            incrementPBar(pb.getValue(),40);
            //-------------------------------------------
            /*NOTE: Executing the command here*/
            Process runtimeProcess = Runtime.getRuntime().exec(executeCmd);
            int processComplete = runtimeProcess.waitFor();
            
            //----- increment progress ------------------
            incrementPBar(pb.getValue(),80);
            //-------------------------------------------
            /*NOTE: processComplete=0 if correctly executed, will contain other values if not*/
            if (processComplete == 0) {
                //----- increment progress ------------------
                incrementPBar(pb.getValue(),100);
                //-------------------------------------------
                JOptionPane.showMessageDialog(null,"Backup Complete");
            } else {
                //----- increment progress ------------------
                incrementPBar(pb.getValue(),90);
                //-------------------------------------------
                JOptionPane.showMessageDialog(null,"Backup Failure");
            }

        } catch (IOException | InterruptedException ex) {
            JOptionPane.showMessageDialog(null, "Error at Backup: " + ex.getMessage());
        }
    }
    
    public static void RestoreDbFromSql(String fullFilename, String execFile) {
        try {
            /*NOTE: String s is the mysql file name including the .sql in its name*/
            /*NOTE: Getting path to the Jar file being executed*/
            /*NOTE: YourImplementingClass-> replace with the class executing the code*/
            //CodeSource codeSource = DatabaseBackupRestore.class.getProtectionDomain().getCodeSource();
            //File jarFile = new File(codeSource.getLocation().toURI().getPath());
            //String jarDir = jarFile.getParentFile().getPath();

            /*NOTE: Creating Database Constraints*/
            dbConnection dbcon = new dbConnection();
            String dbName = dbcon.getDB();
            String dbUser = dbcon.getUID();
            String dbPass = dbcon.getPWD();
            //----- increment progress ------------------
            DatabaseBackupRestore.incrementPBar(pb.getValue(),35);
            //-------------------------------------------
            /*NOTE: Creating Path Constraints for restoring*/
            String restorePath = fullFilename;

            /*NOTE: Used to create a cmd command*/
            /*NOTE: Do not create a single large string, this will cause buffer locking, use string array*/
            String port = dbcon.getPORT();
            String ip = dbcon.getSHORTIP();
            //----- increment progress ------------------
            DatabaseBackupRestore.incrementPBar(pb.getValue(),45);
            //-------------------------------------------
            String[] executeCmd = new String[]{execFile, "--host", ip, "--port", port,dbName, "-u" + dbUser, "-p" + dbPass, "-e", " source " + restorePath};
            //----- increment progress ------------------
            DatabaseBackupRestore.incrementPBar(pb.getValue(),50);
            //-------------------------------------------
            /*NOTE: processComplete=0 if correctly executed, will contain other values if not*/
            Process runtimeProcess = Runtime.getRuntime().exec(executeCmd);
            //----- increment progress ------------------
            DatabaseBackupRestore.incrementPBar(pb.getValue(),70);
            //-------------------------------------------
            int processComplete = runtimeProcess.waitFor();
            //----- increment progress ------------------
            DatabaseBackupRestore.incrementPBar(pb.getValue(),80);
            //-------------------------------------------
            /*NOTE: processComplete=0 if correctly executed, will contain other values if not*/
            if (processComplete == 0) {
                //----- increment progress ------------------
                DatabaseBackupRestore.incrementPBar(pb.getValue(),100);
                //-------------------------------------------
                JOptionPane.showMessageDialog(null, "Successfully restored from SQL.");
            } else {
                //----- increment progress ------------------
                DatabaseBackupRestore.incrementPBar(pb.getValue(),90);
                //-------------------------------------------
                JOptionPane.showMessageDialog(null, "System has encountered an error.");
            }

        } catch (IOException | InterruptedException | HeadlessException ex) {
            JOptionPane.showMessageDialog(null, "Error at RestoreDbFromSql" + ex.getMessage());
        }

    }
    
    public static void incrementPBar(int initialValue, int lastValue){
        for(int i=initialValue;i<=lastValue;i++){
            pb.setValue(i);
            try {
            Thread.sleep(10);
            } catch (InterruptedException ex) {
                Logger.getLogger(DatabaseBackupRestore.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
