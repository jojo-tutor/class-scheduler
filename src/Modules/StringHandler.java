/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modules;

/**
 *
 * @author Jojo
 */
public class StringHandler {
    public String toTitleCase(String givenString) {
        if(givenString.length()!=0){
            givenString = givenString.replaceAll("\\s+", " "); //this gets all the double spaces, or triple spaces
            try{
                String[] arr = givenString.split(" ");
                StringBuilder sb = new StringBuilder();
                for (String arr1 : arr) {
                    sb.append(Character.toUpperCase(arr1.charAt(0))).append(arr1.substring(1)).append(" ");
                }   
                return sb.toString().trim();

            }catch(StringIndexOutOfBoundsException e){
                return givenString;
            }
        }else{
            return "";
        }
    }
}
