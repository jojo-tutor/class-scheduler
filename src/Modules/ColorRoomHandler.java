/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modules;

import java.awt.Color;

/**
 *
 * @author Jojo
 */

public class ColorRoomHandler {
    private final Color AQUA                = new Color(0x00FFFF);  //0
    private final Color BISQUE              = new Color(0xFFE4C4);  //1
    private final Color BLUE                = new Color(0x0000FF);  //2
    private final Color BROWN               = new Color(0xA52A2A);  //3
    private final Color CHARTREUSE          = new Color(0x7FFF00);  //4
    private final Color CORAL               = new Color(0xFF7F50);  //5
    private final Color DARKGOLDENROD       = new Color(0xB8860B);  //6
    private final Color DARKMAGENTA         = new Color(0x8B008B);  //7
    private final Color DARKORANGE          = new Color(0xFF8C00);  //8
    private final Color DEEPPINK            = new Color(0xFF1493);  //9
    private final Color DIMGRAY             = new Color(0x696969);  //10
    private final Color GOLD                = new Color(0xFFD700);  //11
    private final Color GREEN               = new Color(0x008000);  //12
    private final Color FUSHSIA             = new Color(0xFF00FF);  //13
    private final Color KHAKI               = new Color(0xF0E68C);  //14
    private final Color LIGHTSKYBLUE        = new Color(0x87CEFA);  //15
    private final Color ORANGE              = new Color(0xFFA500);  //16
    private final Color YELLOW              = new Color(0xFFFF00);  //17
    private final Color VIOLET              = new Color(0xEE82EE);  //18
    private final Color GREENYELLOW         = new Color(0xADFF2F);  //19
    private final Color TAN                 = new Color(0xD2B48C);  //20
    
    
    private final Color COLORS[] = {AQUA,BISQUE,BLUE,BROWN,CHARTREUSE,CORAL,DARKGOLDENROD,
                                    DARKMAGENTA,DARKORANGE,DEEPPINK,DIMGRAY,GOLD,GREEN,
                                    FUSHSIA,KHAKI,LIGHTSKYBLUE,ORANGE,YELLOW,VIOLET,
                                    GREENYELLOW,TAN};

    public ColorRoomHandler() {
    }
    
    public Color getColor(int index){
        return COLORS[index];
    }
    
    public int getNextIndex(int currentIndex){
        int maxIndex = COLORS.length;
        
        if(currentIndex+1<maxIndex){
            return currentIndex+1;
        }else{
            return 0;
        }
    }
    
    
}
