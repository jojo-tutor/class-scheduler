/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package AppPackage;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 *
 * @author Jojo
 */
public class InquiryClassSchedule extends javax.swing.JInternalFrame {

    /**
     * Creates new form ManageClassListGUI
     */
    public InquiryClassSchedule() {
        initComponents();
        
        GetterSetterClass gsc = new GetterSetterClass();
        ayStart.setText(gsc.getAcademicYearStart()+"");
        ayEnd.setText(gsc.getAcademicYearEnd()+"");
        
        fillCombos();
        
        tableListener();
        
        addListener(classlist);
        addListener(section);
        addListener(semester);
        
        //---------FOR LOGS---------------------
        new dbConnection().recordLog("[Open] Class Schedule Inquiry");
        //--------------------------------------
        
        refreshTable();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        classlist = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        section = new javax.swing.JComboBox();
        jLabel6 = new javax.swing.JLabel();
        semester = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        btnRefresh = new javax.swing.JButton();
        btnPrint = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        ayStart = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        ayEnd = new javax.swing.JLabel();
        btnMinus = new javax.swing.JButton();
        btnPlus = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        labelNofRows = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        ScheduleList = new javax.swing.JTable();

        setClosable(true);
        setTitle("Inquiry Class Schedule");
        setPreferredSize(new java.awt.Dimension(930, 500));
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosing(evt);
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        jPanel1.setBackground(new java.awt.Color(102, 153, 255));

        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Course/Year:");
        jLabel2.setPreferredSize(new java.awt.Dimension(80, 30));

        classlist.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "BSCS 1" }));
        classlist.setPreferredSize(new java.awt.Dimension(127, 30));
        classlist.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                classlistMouseClicked(evt);
            }
        });
        classlist.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                classlistActionPerformed(evt);
            }
        });

        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Section:");
        jLabel4.setPreferredSize(new java.awt.Dimension(45, 30));

        section.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "A", "B", "C", "D", "E", "F", "G" }));
        section.setPreferredSize(new java.awt.Dimension(127, 30));
        section.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sectionMouseClicked(evt);
            }
        });
        section.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sectionActionPerformed(evt);
            }
        });

        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Semester:");
        jLabel6.setPreferredSize(new java.awt.Dimension(49, 30));

        semester.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3" }));
        semester.setPreferredSize(new java.awt.Dimension(100, 30));

        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel7.setText("A.Y:");
        jLabel7.setPreferredSize(new java.awt.Dimension(49, 30));

        btnRefresh.setText("Refresh");
        btnRefresh.setPreferredSize(new java.awt.Dimension(80, 30));
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        btnPrint.setText("Print");
        btnPrint.setPreferredSize(new java.awt.Dimension(145, 30));
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        ayStart.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ayStart.setText("2015");
        ayStart.setPreferredSize(new java.awt.Dimension(30, 30));

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("-");
        jLabel5.setPreferredSize(new java.awt.Dimension(40, 30));

        ayEnd.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ayEnd.setText("2016");
        ayEnd.setPreferredSize(new java.awt.Dimension(30, 30));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ayStart, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(ayEnd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ayStart, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ayEnd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        btnMinus.setText("<");
        btnMinus.setPreferredSize(new java.awt.Dimension(80, 30));
        btnMinus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMinusActionPerformed(evt);
            }
        });

        btnPlus.setText(">");
        btnPlus.setPreferredSize(new java.awt.Dimension(80, 30));
        btnPlus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPlusActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(classlist, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(section, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(semester, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnMinus, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(btnPlus, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(classlist, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(section, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(semester, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnMinus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnPlus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel3.setText("Results found:");

        labelNofRows.setText("0 row(s)");

        ScheduleList.setAutoCreateRowSorter(true);
        ScheduleList.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                { new Integer(1), "MATH 3", "College Algebra 1", "MTW", "07:30 AM - 08:30 AM", "3", "3", "15", "Emmylou C. Maputol"}
            },
            new String [] {
                "ID", "SUBJECT CODE", "SUBJECT DESCRIPTION", "DAYS", "TIME", "UNITS", "HOURS", "ROOM NO", "FACULTY"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        ScheduleList.setToolTipText("");
        ScheduleList.setRowHeight(25);
        ScheduleList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ScheduleListMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(ScheduleList);
        if (ScheduleList.getColumnModel().getColumnCount() > 0) {
            ScheduleList.getColumnModel().getColumn(0).setMinWidth(0);
            ScheduleList.getColumnModel().getColumn(0).setPreferredWidth(0);
            ScheduleList.getColumnModel().getColumn(0).setMaxWidth(0);
            ScheduleList.getColumnModel().getColumn(1).setMinWidth(120);
            ScheduleList.getColumnModel().getColumn(1).setPreferredWidth(120);
            ScheduleList.getColumnModel().getColumn(1).setMaxWidth(120);
            ScheduleList.getColumnModel().getColumn(3).setMinWidth(60);
            ScheduleList.getColumnModel().getColumn(3).setPreferredWidth(60);
            ScheduleList.getColumnModel().getColumn(3).setMaxWidth(60);
            ScheduleList.getColumnModel().getColumn(4).setMinWidth(140);
            ScheduleList.getColumnModel().getColumn(4).setPreferredWidth(140);
            ScheduleList.getColumnModel().getColumn(4).setMaxWidth(140);
            ScheduleList.getColumnModel().getColumn(5).setMinWidth(60);
            ScheduleList.getColumnModel().getColumn(5).setPreferredWidth(60);
            ScheduleList.getColumnModel().getColumn(5).setMaxWidth(60);
            ScheduleList.getColumnModel().getColumn(6).setMinWidth(60);
            ScheduleList.getColumnModel().getColumn(6).setPreferredWidth(60);
            ScheduleList.getColumnModel().getColumn(6).setMaxWidth(60);
            ScheduleList.getColumnModel().getColumn(7).setMinWidth(80);
            ScheduleList.getColumnModel().getColumn(7).setPreferredWidth(80);
            ScheduleList.getColumnModel().getColumn(7).setMaxWidth(80);
        }

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelNofRows, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(labelNofRows))
                .addGap(20, 20, 20))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void addAY(){
        ayStart.setText((Integer.parseInt(ayStart.getText())+1)+"");
        ayEnd.setText((Integer.parseInt(ayEnd.getText())+1)+"");
    }
    private void minusAY(){
        ayStart.setText((Integer.parseInt(ayStart.getText())-1)+"");
        ayEnd.setText((Integer.parseInt(ayEnd.getText())-1)+"");
    }
    
    private void fillCombos(){
        dbConnection dbcon = new dbConnection();
        dbcon.fillListDualCombo(classlist, "Select id,classlist from tbl_classlist order by yearlevel,classlist");
        
    }
    
    private void addListener(JComboBox combo){
        combo.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                refreshTable();
            }
        });
    }
    
    private void classlistMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_classlistMouseClicked

    }//GEN-LAST:event_classlistMouseClicked

    private void classlistActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_classlistActionPerformed

    }//GEN-LAST:event_classlistActionPerformed

    private void sectionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sectionMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_sectionMouseClicked

    private void sectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sectionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_sectionActionPerformed

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        refreshTable();
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
        //PrintUIWindow pu = new PrintUIWindow(schedPanel);
        //pu.goPrint();
    }//GEN-LAST:event_btnPrintActionPerformed

    private void ScheduleListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ScheduleListMouseClicked
        /*UpdateLink.setEnabled(true);
        RemoveLink.setEnabled(true);

        if(evt.getClickCount()==2){
            showUpdateDialog();
        }*/
    }//GEN-LAST:event_ScheduleListMouseClicked

    private void btnMinusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMinusActionPerformed
        minusAY();
        refreshTable();
    }//GEN-LAST:event_btnMinusActionPerformed

    private void btnPlusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPlusActionPerformed
        addAY();
        refreshTable();
    }//GEN-LAST:event_btnPlusActionPerformed

    private void formInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosing
        //---------FOR LOGS---------------------
        new dbConnection().recordLog("[Close] Class Schedule Inquiry");
        //--------------------------------------
    }//GEN-LAST:event_formInternalFrameClosing

    public void tableListener(){
       TableModel tm = ScheduleList.getModel();
       tm.addTableModelListener(new TableModelListener() {

            @Override
            public void tableChanged(TableModelEvent e) {
                labelNofRows.setText(ScheduleList.getRowCount() + " row(s)");
            }
        });
             
    }

    private void refreshTable(){
        //---------FOR LOGS---------------------
        new dbConnection().recordLog("[Inquire] Class Schedule: "+classlist.getSelectedItem().toString()+"-"+section.getSelectedItem().toString()+", Semester: "+semester.getSelectedItem().toString()+", A.Y.: "+ayStart.getText()+"-"+ayEnd.getText());
        //--------------------------------------
        ComboItem Classlist = (ComboItem) classlist.getSelectedItem();
        String query = "SELECT "
                + "sched.id, sj.subject_code, "
                + "CONCAT(sj.subject_description,' (',sjd.subject_type,')') as description,"
                + "sched.days,"
                + "CONCAT(sched.start_time,' - ',sched.end_time) as time,"
                + "IFNULL((Select numofunits From tbl_subject where id=sjd.subject_id and subject_type!='LAB'),'') "
                + ", sched.nOfHours_perWeek, rm.room,"
                + "CONCAT(fac.firstname,' ',fac.middlename,' ',fac.lastname) as facultyname "
                + "FROM "
                + "tbl_schedule sched, tbl_subject_detail sjd, tbl_subject sj, tbl_room rm, tbl_faculty fac "
                + "WHERE "
                + "sched.subject_id=sjd.id and sjd.subject_id=sj.id and "
                + "sched.room_id = rm.id and "
                + "sched.faculty_id = fac.id and "
                + "sched.classlist_id='" + Classlist.getID() + "' and "
                + "sched.section='" + section.getSelectedItem() + "' and "
                + "sched.semester='" + semester.getSelectedItem() + "' and "
                + "sched.ay_start='" + ayStart.getText() + "' and "
                + "sched.ay_end='" + ayEnd.getText() + "' and "
                + "sjd.numofhours!='0' "
                + "ORDER BY sj.subject_code, sj.subject_description";

        dbConnection dbcon = new dbConnection();
        dbcon.fillTable(ScheduleList, query);
    }
     
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable ScheduleList;
    private javax.swing.JLabel ayEnd;
    private javax.swing.JLabel ayStart;
    private javax.swing.JButton btnMinus;
    private javax.swing.JButton btnPlus;
    private javax.swing.JButton btnPrint;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JComboBox classlist;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelNofRows;
    private javax.swing.JComboBox section;
    private javax.swing.JComboBox<String> semester;
    // End of variables declaration//GEN-END:variables
}
