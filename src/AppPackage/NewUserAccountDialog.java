/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package AppPackage;

import Modules.InputValidator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;

/**
 *
 * @author Jojo
 */
public class NewUserAccountDialog extends javax.swing.JDialog {

    /**
     * Creates new form NewSubjectDialog
     */
    protected static JTable table;
    public NewUserAccountDialog(java.awt.Frame parent, boolean modal, JTable table) {
        super(parent, modal);
        initComponents();
        
        this.table = table;
        groupButton();
        
        dbConnection dbcon = new dbConnection();
        dbcon.fillListDualCombo(comboUser,"Select id, CONCAT(firstname,' ',middlename,' ',lastname) as name FROM tbl_faculty ORDER BY firstname");
        //dbcon.fillTable(table, "Select a.id, a.username, a.password, a.usertype, CONCAT(b.lastname,', ',b.firstname) AS name, a.instructor_id  FROM tbl_account a, tbl_faculty b WHERE a.instructor_id=b.id GROUP BY a.id");
        resetFields();
        
        //---------FOR LOGS---------------------
        new dbConnection().recordLog("[Open] New User Account Dialog");
        //--------------------------------------
    }
    
    private void groupButton( ) {
        ButtonGroup radioGroup = new ButtonGroup( );
        radioGroup.add(radioChairperson);
        radioGroup.add(radioAdministrator);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        textFieldUsername = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        radioAdministrator = new javax.swing.JRadioButton();
        radioChairperson = new javax.swing.JRadioButton();
        passwordFieldPassword = new javax.swing.JPasswordField();
        jLabel6 = new javax.swing.JLabel();
        comboUser = new javax.swing.JComboBox();
        checkShow = new javax.swing.JCheckBox();
        btnCancel = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setLocationByPlatform(true);
        setResizable(false);
        setType(java.awt.Window.Type.UTILITY);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Segoe UI Light", 0, 36)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 102, 255));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel7.setText(" New User");
        jLabel7.setOpaque(true);

        jLabel3.setFont(new java.awt.Font("Segoe UI Semilight", 0, 12)); // NOI18N
        jLabel3.setText("User Type:");

        textFieldUsername.setFont(new java.awt.Font("Segoe UI Semilight", 0, 14)); // NOI18N
        textFieldUsername.setText("Sir Jojo");
        textFieldUsername.setPreferredSize(new java.awt.Dimension(50, 30));
        textFieldUsername.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                textFieldUsernameFocusLost(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Segoe UI Semilight", 0, 12)); // NOI18N
        jLabel2.setText("Password:");

        jLabel1.setFont(new java.awt.Font("Segoe UI Semilight", 0, 12)); // NOI18N
        jLabel1.setText("Username:");

        radioAdministrator.setBackground(new java.awt.Color(255, 255, 255));
        radioAdministrator.setFont(new java.awt.Font("Segoe UI Semilight", 0, 14)); // NOI18N
        radioAdministrator.setText("Administrator");
        radioAdministrator.setPreferredSize(new java.awt.Dimension(90, 30));

        radioChairperson.setBackground(new java.awt.Color(255, 255, 255));
        radioChairperson.setFont(new java.awt.Font("Segoe UI Semilight", 0, 14)); // NOI18N
        radioChairperson.setSelected(true);
        radioChairperson.setText("Chairperson");
        radioChairperson.setName(""); // NOI18N
        radioChairperson.setNextFocusableComponent(radioAdministrator);
        radioChairperson.setPreferredSize(new java.awt.Dimension(90, 30));

        passwordFieldPassword.setFont(new java.awt.Font("Segoe UI Semilight", 0, 14)); // NOI18N
        passwordFieldPassword.setText("jPasswordField1");
        passwordFieldPassword.setPreferredSize(new java.awt.Dimension(110, 30));

        jLabel6.setFont(new java.awt.Font("Segoe UI Semilight", 0, 12)); // NOI18N
        jLabel6.setText("Faculty User:");

        comboUser.setFont(new java.awt.Font("Segoe UI Semilight", 0, 14)); // NOI18N
        comboUser.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tutor, Josaphat" }));
        comboUser.setPreferredSize(new java.awt.Dimension(102, 30));

        checkShow.setBackground(new java.awt.Color(255, 255, 255));
        checkShow.setFont(new java.awt.Font("Segoe UI Semilight", 0, 14)); // NOI18N
        checkShow.setText("Show");
        checkShow.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkShowActionPerformed(evt);
            }
        });

        btnCancel.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        btnCancel.setText("Cancel");
        btnCancel.setPreferredSize(new java.awt.Dimension(100, 35));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        btnSave.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        btnSave.setText("Save");
        btnSave.setPreferredSize(new java.awt.Dimension(100, 35));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(comboUser, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(passwordFieldPassword, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(checkShow))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(radioChairperson, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(radioAdministrator, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(textFieldUsername, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1)
                            .addComponent(jLabel3)
                            .addComponent(jLabel6)
                            .addComponent(jLabel2))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(textFieldUsername, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(passwordFieldPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(checkShow, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radioChairperson, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(radioAdministrator, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(comboUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jLabel8.setFont(new java.awt.Font("Segoe UI Light", 0, 5)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 102, 255));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel8.setText("_____________________________________________________________________________________________________________________________________________________________________________");
        jLabel8.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel8.setOpaque(true);
        jLabel8.setVerticalTextPosition(javax.swing.SwingConstants.TOP);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jLabel8)
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(20, 20, 20))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        //---------FOR LOGS---------------------
        new dbConnection().recordLog("[Close] New User Account Dialog");
        //--------------------------------------
        this.dispose();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        InputValidator iv = new InputValidator();
        boolean aFlag = iv.validateIsEmptyComponent(textFieldUsername);
        if(aFlag){
            if(comboUser.getSelectedIndex()!=-1){
                String strPassword = new String(passwordFieldPassword.getPassword());
                String encryptedPassword = encrypt(strPassword);
                String userType;

                if(radioChairperson.isSelected()){
                    userType = "Chairperson";
                }else{
                    userType = "Administrator";
                }

                ComboItem comboItem = (ComboItem) comboUser.getSelectedItem();

                dbConnection dbcon = new dbConnection();
                if(dbcon.insertDataDuplicateException("tbl_account","instructor_id,username,password,usertype","'" + comboItem.getID() + "','" + textFieldUsername.getText() + "','" + encryptedPassword + "','" + userType + "'")){
                    dbcon.fillTable(table, "Select aa.id, aa.username, aa.password, aa.usertype, CONCAT(b.address,' ',a.firstname,' ',a.middlename,' ',a.lastname,' ',c.extension) as name, aa.instructor_id  FROM tbl_account aa, tbl_faculty a, tbl_name_address b,tbl_name_extension c WHERE (a.address_id=b.id) and (a.extension_id=c.id) and a.id=aa.instructor_id ORDER BY aa.username");
                    resetFields();
                    //---------FOR LOGS---------------------
                    new dbConnection().recordLog("[Save] New User Account: "+textFieldUsername.getText()+" UserType: "+userType+" Faculty: "+comboItem.getName());
                    //--------------------------------------
                    this.dispose();
                }else{
                    //---------FOR LOGS---------------------
                    new dbConnection().recordLog("[Save] New User Account - Error Duplicate Values");
                    //--------------------------------------
                    JOptionPane.showMessageDialog(null, "Username already exists, please change the field.");
                    textFieldUsername.requestFocus();
                    textFieldUsername.selectAll();
                }
            }else{
                JOptionPane.showMessageDialog(null, "Plese select user.");
            }
        }
        
    }//GEN-LAST:event_btnSaveActionPerformed

    private String encrypt(String text){
        String encryptionKey = "J0jOChoYaNzGwAp0";
        String plainText = text;
        String cipherText="";
        AdvancedEncryptionStandard advancedEncryptionStandard = new AdvancedEncryptionStandard(encryptionKey);
        
        try {
            cipherText = advancedEncryptionStandard.encrypt(plainText);
        } catch (Exception ex) {
            Logger.getLogger(MainMDI.class.getName()).log(Level.SEVERE, null, ex);
        }

        return cipherText;
    }
    
    private String decrypt(String text){
        String encryptionKey = "J0jOChoYaNzGwAp0";
        String cipherText = text;
        String decryptedCipherText="";
        AdvancedEncryptionStandard advancedEncryptionStandard = new AdvancedEncryptionStandard(encryptionKey);
        
        try {
            decryptedCipherText = advancedEncryptionStandard.decrypt(cipherText);
        } catch (Exception ex) {
            Logger.getLogger(MainMDI.class.getName()).log(Level.SEVERE, null, ex);
        }

        return decryptedCipherText;
    }
    
    private void checkShowActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkShowActionPerformed
        if(checkShow.isSelected()){
            passwordFieldPassword.setEchoChar((char)0);
        }else{
            passwordFieldPassword.setEchoChar('•');
        }
    }//GEN-LAST:event_checkShowActionPerformed

    private void textFieldUsernameFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_textFieldUsernameFocusLost
        new InputValidator().validateIsEmptyComponent(evt.getComponent());
    }//GEN-LAST:event_textFieldUsernameFocusLost

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        //---------FOR LOGS---------------------
        new dbConnection().recordLog("[Close] New User Account Dialog");
        //--------------------------------------
    }//GEN-LAST:event_formWindowClosing
    private void resetFields(){
        textFieldUsername.setText(null);
        passwordFieldPassword.setText(null);
        passwordFieldPassword.setEchoChar('•');
        radioChairperson.setSelected(true);
        comboUser.setSelectedIndex(-1);
        checkShow.setSelected(false);
        textFieldUsername.requestFocus();
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(NewUserAccountDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(NewUserAccountDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(NewUserAccountDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NewUserAccountDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                NewUserAccountDialog dialog = new NewUserAccountDialog(new javax.swing.JFrame(), true, new JTable());
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });

                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnSave;
    private javax.swing.JCheckBox checkShow;
    private javax.swing.JComboBox comboUser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPasswordField passwordFieldPassword;
    private javax.swing.JRadioButton radioAdministrator;
    private javax.swing.JRadioButton radioChairperson;
    private javax.swing.JTextField textFieldUsername;
    // End of variables declaration//GEN-END:variables
}
