/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package AppPackage;

import Modules.InputValidator;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import javax.swing.JTable;

/**
 *
 * @author Jojo
 */
public class UpdateDepartmentDialog extends javax.swing.JDialog {

    /**
     * Creates new form NewRoomDialog
     */
    public UpdateDepartmentDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }
    
    protected int id;
    protected JTable table;
    protected int rowIndex;
    private String officeID;
    public UpdateDepartmentDialog(java.awt.Frame parent, boolean modal,JTable table,int rowIndex,int id, String departmentcode, String department, String collegeCode, int facultyID) {
        super(parent, modal);
        initComponents();
        
        this.id = id;
        this.rowIndex = rowIndex;
        this.table = table;
        
        dbConnection dbcon = new dbConnection();
        officeID = dbcon.findOneQuery("id", "tbl_office", "office_code='" + departmentcode + "'");
        dbcon.fillListDualCombo(comboCollege,"Select id, college_code FROM tbl_college ORDER BY college_code");
        dbcon.fillListDualCombo(comboChairperson,"Select id, CONCAT(firstname,' ',middlename,' ',lastname) as name FROM tbl_faculty ORDER BY firstname");
        
        textFieldDepartmentCode.setText(departmentcode);
        textFieldDepartmentDescription.setText(department);
        
        //select the item on combo box
        int currentIndex2=-1;
        for(int x=0;x<comboChairperson.getItemCount();x++){
            comboChairperson.setSelectedIndex(x);
            ComboItem comboItem = (ComboItem) comboChairperson.getSelectedItem();
            if(comboItem.getID()==facultyID){
                currentIndex2=x;
                break;
            }
        }
        comboChairperson.setSelectedIndex(currentIndex2);
        
        //select the item on combo box
        int currentIndex=-1;
        for(int x=0;x<comboCollege.getItemCount();x++){
            if(((comboCollege.getItemAt(x)).toString()).equalsIgnoreCase(collegeCode)){
                currentIndex=x;
                break;
            }
        }
        comboCollege.setSelectedIndex(currentIndex);
        
        //---------FOR LOGS---------------------
        new dbConnection().recordLog("[Open] Update Department Dialog");
        //--------------------------------------

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        textFieldDepartmentCode = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        comboCollege = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        comboChairperson = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        textFieldDepartmentDescription = new javax.swing.JTextArea();
        btnCancel = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setLocationByPlatform(true);
        setResizable(false);
        setType(java.awt.Window.Type.UTILITY);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Segoe UI Semilight", 0, 12)); // NOI18N
        jLabel3.setText("Department of:");

        textFieldDepartmentCode.setFont(new java.awt.Font("Segoe UI Semilight", 0, 14)); // NOI18N
        textFieldDepartmentCode.setText("ICT");
        textFieldDepartmentCode.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                textFieldDepartmentCodeFocusLost(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Segoe UI Semilight", 0, 12)); // NOI18N
        jLabel1.setText("Department Code:");

        jLabel2.setFont(new java.awt.Font("Segoe UI Semilight", 0, 12)); // NOI18N
        jLabel2.setText("College:");

        comboCollege.setFont(new java.awt.Font("Segoe UI Semilight", 0, 14)); // NOI18N
        comboCollege.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "CTAS", "CTE", "CFMS", "CADS" }));

        jLabel4.setFont(new java.awt.Font("Segoe UI Semilight", 0, 12)); // NOI18N
        jLabel4.setText("Chairperson:");

        comboChairperson.setFont(new java.awt.Font("Segoe UI Semilight", 0, 14)); // NOI18N
        comboChairperson.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Prof. Jojo E. Tutor", "Prof. Kennery V. Romero", "Prof. Reynan G. Amoguis" }));

        textFieldDepartmentDescription.setColumns(20);
        textFieldDepartmentDescription.setFont(new java.awt.Font("Segoe UI Semilight", 0, 14)); // NOI18N
        textFieldDepartmentDescription.setLineWrap(true);
        textFieldDepartmentDescription.setRows(3);
        textFieldDepartmentDescription.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                textFieldDepartmentDescriptionFocusLost(evt);
            }
        });
        jScrollPane1.setViewportView(textFieldDepartmentDescription);

        btnCancel.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        btnSave.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        btnSave.setText("Save Changes");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(comboCollege, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(comboChairperson, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2)
                            .addComponent(jLabel4))
                        .addGap(0, 245, Short.MAX_VALUE))
                    .addComponent(textFieldDepartmentCode))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(textFieldDepartmentCode, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(comboChairperson, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(comboCollege, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jLabel7.setFont(new java.awt.Font("Segoe UI Light", 0, 36)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 102, 255));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel7.setText("Update Deparment");
        jLabel7.setOpaque(true);

        jLabel8.setFont(new java.awt.Font("Segoe UI Light", 0, 5)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 102, 255));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel8.setText("_________________________________________________________________________________________________________________________________________________________________________");
        jLabel8.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel8.setOpaque(true);
        jLabel8.setVerticalTextPosition(javax.swing.SwingConstants.TOP);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jLabel8)
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(20, 20, 20))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        //---------FOR LOGS---------------------
        new dbConnection().recordLog("[Close] Update Department Dialog");
        //--------------------------------------
        this.dispose();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        InputValidator iv = new InputValidator();
        boolean aFlag = iv.validateIsEmptyComponent(textFieldDepartmentCode) &&
                        iv.validateIsEmptyComponent(textFieldDepartmentDescription);
        if(aFlag){
            ComboItem comboItem = (ComboItem) comboCollege.getSelectedItem();
            ComboItem comboItem2 = (ComboItem) comboChairperson.getSelectedItem();

            GetterSetterClass gsc = new GetterSetterClass();
            dbConnection dbcon = new dbConnection();
            String desigID_chairperson = dbcon.findOneQuery("id", "tbl_designation", "designation='Chairperson'");
            boolean isUpdated = dbcon.updateDataDuplicateExceptionSilently("tbl_department", "department_code='" + textFieldDepartmentCode.getText() + "',department_description='" + textFieldDepartmentDescription.getText() + "',college_id='" + comboItem.getID() + "',chairperson='" + comboItem2.getID() + "'", "id='" + id + "'");
            if(isUpdated){
                boolean isInserted = dbcon.insertDataDuplicateExceptionSilently("tbl_faculty_designation","faculty_id,designation_id,office_id,semester,ay_start,ay_end", "'" + comboItem.getID() + "','" + desigID_chairperson + "','" + officeID + "','" + gsc.getSemester() + "','" + gsc.getAcademicYearStart() + "','" + gsc.getAcademicYearEnd() + "'");
                if(isInserted){
                    JOptionPane.showMessageDialog(null, "Data has been updated.");
                }else{
                    dbcon.updateData("tbl_faculty_designation", "faculty_id='" + comboItem2.getID() + "',office_id='" + officeID + "'", "designation_id='" + desigID_chairperson + "' and office_id='" + officeID + "' and semester='" + gsc.getSemester() + "' and ay_start='" + gsc.getAcademicYearStart() + "' and ay_end='" + gsc.getAcademicYearEnd() + "'");
                }
            }
            dbcon.fillTable(table, "Select dept.id, dept.department_code, dept.department_description, CONCAT(b.address,' ',a.firstname,' ',a.middlename,' ',a.lastname,' ',c.extension) AS name, col.college_code, dept.chairperson FROM tbl_department dept, tbl_college col,tbl_faculty a, tbl_name_address b, tbl_name_extension c WHERE (a.address_id=b.id) and (a.extension_id=c.id) and a.id=dept.chairperson and dept.college_id=col.id ORDER by dept.department_code");
            //---------FOR LOGS---------------------
            new dbConnection().recordLog("[Save] Update Department ID: "+id+" "+textFieldDepartmentCode.getText()+"-"+textFieldDepartmentDescription.getText()+" Chairperson: "+comboItem2.getName()+" College:"+comboItem.getName());
            //--------------------------------------
            table.setRowSelectionInterval(rowIndex, rowIndex);
            this.dispose();
        }
        
    }//GEN-LAST:event_btnSaveActionPerformed

    private void textFieldDepartmentCodeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_textFieldDepartmentCodeFocusLost
        new InputValidator().validateIsEmptyComponent(evt.getComponent());
    }//GEN-LAST:event_textFieldDepartmentCodeFocusLost

    private void textFieldDepartmentDescriptionFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_textFieldDepartmentDescriptionFocusLost
        new InputValidator().validateIsEmptyComponent(evt.getComponent());
    }//GEN-LAST:event_textFieldDepartmentDescriptionFocusLost

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        //---------FOR LOGS---------------------
        new dbConnection().recordLog("[Close] Update Department Dialog");
        //--------------------------------------
    }//GEN-LAST:event_formWindowClosing

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(UpdateDepartmentDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(UpdateDepartmentDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(UpdateDepartmentDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UpdateDepartmentDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                UpdateDepartmentDialog dialog = new UpdateDepartmentDialog(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox comboChairperson;
    private javax.swing.JComboBox comboCollege;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField textFieldDepartmentCode;
    private javax.swing.JTextArea textFieldDepartmentDescription;
    // End of variables declaration//GEN-END:variables
}
