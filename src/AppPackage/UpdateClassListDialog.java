/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package AppPackage;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Jojo
 */
public class UpdateClassListDialog extends javax.swing.JDialog {
    
    
    //unused constructor
    public UpdateClassListDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }
    
    protected int id;
    protected JTable table;
    protected int rowIndex;
    protected int defaultSectionCount;
    public UpdateClassListDialog(java.awt.Frame parent, boolean modal,JTable table,int rowIndex,int id, String courseCode, String yearLevel, String semester) {
        super(parent, modal);
        initComponents();
        
        this.table = table;
        this.rowIndex = rowIndex;
        this.id = id;

        dbConnection dbcon = new dbConnection();
        dbcon.fillListDualCombo(comboCourse,"Select id,course_code FROM tbl_course ORDER BY course_code");
        selectItemOnCombo(comboCourse,courseCode);
        
        dbcon.fillListDualCombo(comboYearLevel,"Select id,yearlevel FROM tbl_yearlevel ORDER BY yearlevel");
        selectItemOnCombo(comboYearLevel,yearLevel);
        
        fillRight();
        fillLeft();
        deleteCommonRow();
        
        setNoOfSections();
        
        GetterSetterClass gsc = new GetterSetterClass();
        txtSem.setText(gsc.getSemester()+"");
        txtAY.setText(gsc.getAcademicYearStart()+"-"+gsc.getAcademicYearEnd());
        
        addListener(comboCourse);

        rightTable.getSelectionModel().addListSelectionListener(new RowListenerRight());
        leftTable.getSelectionModel().addListSelectionListener(new RowListenerLeft());
        
        //---------FOR LOGS---------------------
        new dbConnection().recordLog("[Open] Update Checklist Dialog");
        //--------------------------------------
        
    }
    
    private void setNoOfSections(){
        dbConnection dbcon = new dbConnection();
        GetterSetterClass gsc = new GetterSetterClass();
        String sectionCount = dbcon.findOneQuery("COUNT(DISTINCT section)", "tbl_classlist_section", "classlist_id='" + id + "' and semester='" + gsc.getSemester() + "' and ay_start='" + gsc.getAcademicYearStart() + "' and ay_end='" + gsc.getAcademicYearEnd() + "'");
        sections.setValue(Integer.parseInt(sectionCount));
        defaultSectionCount=Integer.parseInt(sectionCount);
    }
    
    private class RowListenerRight implements ListSelectionListener {
        public void valueChanged(ListSelectionEvent event) {
            if (event.getValueIsAdjusting()) {
                int sRows[];
                sRows=rightTable.getSelectedRows();
                for(int a=0;a<sRows.length;a++){
                    rightTable.setValueAt(true, sRows[a], 0);
                }
                /*
                if(tableSched.getSelectedRowCount()==1){
                    timeTableLeft.setRowSelectionInterval(tableSched.getSelectedRow(), tableSched.getSelectedRow());
                    timeTableRight.setRowSelectionInterval(tableSched.getSelectedRow(), tableSched.getSelectedRow());
                }else if(tableSched.getSelectedRowCount()>1){
                    timeTableLeft.setRowSelectionInterval(tableSched.getSelectedRow(), tableSched.getSelectedRow()+tableSched.getSelectedRowCount()-1);
                    timeTableRight.setRowSelectionInterval(tableSched.getSelectedRow(), tableSched.getSelectedRow()+tableSched.getSelectedRowCount()-1);
                }*/
                return;
            }
        }
    }
    
    private class RowListenerLeft implements ListSelectionListener {
        public void valueChanged(ListSelectionEvent event) {
            if (event.getValueIsAdjusting()) {
                int sRows[];
                sRows=leftTable.getSelectedRows();
                for(int a=0;a<sRows.length;a++){
                    leftTable.setValueAt(true, sRows[a], 0);
                }
                /*
                if(tableSched.getSelectedRowCount()==1){
                    timeTableLeft.setRowSelectionInterval(tableSched.getSelectedRow(), tableSched.getSelectedRow());
                    timeTableRight.setRowSelectionInterval(tableSched.getSelectedRow(), tableSched.getSelectedRow());
                }else if(tableSched.getSelectedRowCount()>1){
                    timeTableLeft.setRowSelectionInterval(tableSched.getSelectedRow(), tableSched.getSelectedRow()+tableSched.getSelectedRowCount()-1);
                    timeTableRight.setRowSelectionInterval(tableSched.getSelectedRow(), tableSched.getSelectedRow()+tableSched.getSelectedRowCount()-1);
                }*/
                return;
            }
        }
    }
    
    private void addListener(JComboBox combo){
        combo.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                fillRight();
            }
        });
    }
    private void addListener2(JComboBox combo){
        combo.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                fillRight();
                fillLeft();
                deleteCommonRow();
                setNoOfSections();
            }
        });
    }
    
    private void fillRight(){
        dbConnection dbcon = new dbConnection();
        if(searchBoxRight.getText().length()==0){
            dbcon.fillSpecialTable(rightTable, "Select a.id,a.id, a.subject_code, a.subject_description from tbl_subject a order by a.subject_code");
        }else{
            dbcon.fillSpecialTable(rightTable, "Select a.id,a.id, a.subject_code, a.subject_description from tbl_subject a where a.subject_code like '%" + searchBoxRight.getText() + "%' or a.subject_description like '%" + searchBoxRight.getText() + "%' order by a.subject_code");
        }
        
        //dbcon.fillSpecialTable(rightTable, "Select a.id,a.id, a.subject_code, a.subject_description from tbl_subject a, tbl_course b where (a.course_id=b.id) and b.course_code='" + comboCourse.getSelectedItem().toString() + "' order by a.subject_code");
        
        countItems();
    }
    
    private void deleteCommonRow(){
       
       int leftRowCount = leftTable.getRowCount();
       int rightRowCount = rightTable.getRowCount();
       int leftCurrentID;
       int rightCurrentID;
       
       for(int ctrLeft=0;ctrLeft<leftRowCount;ctrLeft++){
           leftCurrentID = Integer.parseInt(leftTable.getValueAt(ctrLeft, 1).toString());
           
           for(int ctrRight=0;ctrRight<rightRowCount;ctrRight++){
               rightCurrentID = Integer.parseInt(rightTable.getValueAt(ctrRight, 1).toString());
               
               if(leftCurrentID==rightCurrentID){
                   ((DefaultTableModel) rightTable.getModel()).removeRow(ctrRight);
                   ctrRight=rightRowCount;
                   rightRowCount--;
               }
           }
       }
    }
    
    private void fillLeft(){
        dbConnection dbcon = new dbConnection();
        //dbcon.fillSpecialTable(leftTable, "Select sj.id,sj.id, sj.subject_code, sj.subject_description from tbl_subject sj, tbl_classlist_details cd where cd.subject_id=sj.id and cd.classlist_id='" + id + "' order by sj.subject_code");
        dbcon.fillSpecialTable(leftTable, "Select sj.id,sj.id, sj.subject_code, sj.subject_description from tbl_subject sj, tbl_classlist_details cd where cd.subject_id=sj.id and cd.classlist_id='" + id + "' order by sj.subject_code");
        countItems();
    }
    
    private void selectItemOnCombo(JComboBox combo, String mustBeSelected){
        int currentIndex=-1;
        for(int x=0;x<combo.getItemCount();x++){
            if(((combo.getItemAt(x)).toString()).equalsIgnoreCase(mustBeSelected)){
                currentIndex=x;
            }
        }
        combo.setSelectedIndex(currentIndex);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        comboCourse = new javax.swing.JComboBox();
        txtAY = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        comboYearLevel = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        sections = new javax.swing.JSpinner();
        jLabel6 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtSem = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        btnToRight = new javax.swing.JButton();
        btnToLeft = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        nSubClasslist = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        leftTable = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        nSubAll = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        searchBoxRight = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        rightTable = new javax.swing.JTable();
        jButton9 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Update Checklist");
        setLocationByPlatform(true);
        setResizable(false);
        setSize(new java.awt.Dimension(0, 0));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        btnSave.setFont(new java.awt.Font("Segoe UI Semibold", 0, 14)); // NOI18N
        btnSave.setText("Save Changes");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnCancel.setFont(new java.awt.Font("Segoe UI Semibold", 0, 14)); // NOI18N
        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(102, 153, 255));
        jPanel3.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 1, true));

        comboCourse.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        comboCourse.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "BSCS", "BSOSM", "BHRST", "BEED" }));
        comboCourse.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                comboCourseMouseClicked(evt);
            }
        });
        comboCourse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboCourseActionPerformed(evt);
            }
        });

        txtAY.setEditable(false);
        txtAY.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        txtAY.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtAY.setText("2016-2017");

        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Course:");

        comboYearLevel.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        comboYearLevel.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "I", "II", "III", "IV" }));

        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Year Level:");

        sections.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        sections.setModel(new javax.swing.SpinnerNumberModel(1, 1, null, 1));

        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("No. of Sections:");

        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Sem/AY:");

        txtSem.setEditable(false);
        txtSem.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        txtSem.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtSem.setText("1");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(comboCourse, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(comboYearLevel, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sections, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtSem, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtAY, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE, false)
                        .addComponent(txtSem, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel9)
                        .addComponent(txtAY, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2)
                        .addComponent(comboCourse, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel3)
                        .addComponent(comboYearLevel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel6)
                        .addComponent(sections, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(11, 11, 11))
        );

        jPanel2.setBackground(new java.awt.Color(102, 153, 255));
        jPanel2.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 153), 2, true));
        jPanel2.setPreferredSize(new java.awt.Dimension(1157, 14));

        btnToRight.setText(">");
        btnToRight.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnToRightActionPerformed(evt);
            }
        });

        btnToLeft.setText("<");
        btnToLeft.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnToLeftActionPerformed(evt);
            }
        });

        nSubClasslist.setEditable(false);
        nSubClasslist.setBackground(new java.awt.Color(255, 255, 255));
        nSubClasslist.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        nSubClasslist.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        nSubClasslist.setText("3");

        jLabel7.setText("SUBJECTS ON CHECKLIST:");

        leftTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, "1", "OOP1", "Programming 1"},
                {null, "2", "CS25", "Software Engineering"},
                {null, "3", "CSELEC4", "Artificial Intelligence"}
            },
            new String [] {
                "", "ID", "CODE", "DESCRIPTION"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Boolean.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                true, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        leftTable.setToolTipText("");
        leftTable.setRowHeight(25);
        leftTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                leftTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(leftTable);
        if (leftTable.getColumnModel().getColumnCount() > 0) {
            leftTable.getColumnModel().getColumn(0).setMinWidth(30);
            leftTable.getColumnModel().getColumn(0).setPreferredWidth(30);
            leftTable.getColumnModel().getColumn(0).setMaxWidth(30);
            leftTable.getColumnModel().getColumn(1).setMinWidth(0);
            leftTable.getColumnModel().getColumn(1).setPreferredWidth(0);
            leftTable.getColumnModel().getColumn(1).setMaxWidth(0);
            leftTable.getColumnModel().getColumn(2).setMinWidth(100);
            leftTable.getColumnModel().getColumn(2).setPreferredWidth(100);
            leftTable.getColumnModel().getColumn(2).setMaxWidth(100);
        }

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(nSubClasslist, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(11, 11, 11))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(nSubClasslist, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        nSubAll.setEditable(false);
        nSubAll.setBackground(new java.awt.Color(255, 255, 255));
        nSubAll.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        nSubAll.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        nSubAll.setText("6");

        jLabel5.setText("Search:");

        jLabel8.setText("AVAILABLE SUBJECTS:");

        searchBoxRight.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchBoxRightActionPerformed(evt);
            }
        });

        rightTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, "SC1", "Physics"},
                {null, null, "ENG1", "English I"},
                {null, null, "BIO", "Bilogy"},
                {null, null, "FIL2", "Filipino II"},
                {null, null, "CHEM1", "Chemistry"},
                {null, null, "GEO1", "Geometry"}
            },
            new String [] {
                "", "ID", "CODE", "DESCRIPTION"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Boolean.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                true, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        rightTable.setToolTipText("");
        rightTable.setRowHeight(25);
        rightTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rightTableMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(rightTable);
        if (rightTable.getColumnModel().getColumnCount() > 0) {
            rightTable.getColumnModel().getColumn(0).setMinWidth(30);
            rightTable.getColumnModel().getColumn(0).setPreferredWidth(30);
            rightTable.getColumnModel().getColumn(0).setMaxWidth(30);
            rightTable.getColumnModel().getColumn(1).setMinWidth(0);
            rightTable.getColumnModel().getColumn(1).setPreferredWidth(0);
            rightTable.getColumnModel().getColumn(1).setMaxWidth(0);
            rightTable.getColumnModel().getColumn(2).setMinWidth(100);
            rightTable.getColumnModel().getColumn(2).setPreferredWidth(100);
            rightTable.getColumnModel().getColumn(2).setMaxWidth(100);
        }

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(searchBoxRight))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(nSubAll, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE))
                .addGap(11, 11, 11))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(nSubAll, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(searchBoxRight, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 326, Short.MAX_VALUE)
                .addContainerGap())
        );

        jButton9.setText("<<");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        jButton10.setText(">>");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnToRight, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnToLeft, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(20, 20, 20)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(92, 92, 92)
                        .addComponent(jButton9, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnToLeft, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnToRight, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 194, Short.MAX_VALUE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 450, Short.MAX_VALUE)
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        //---------FOR LOGS---------------------
        new dbConnection().recordLog("[Close] Update Checklist Dialog");
        //--------------------------------------
        this.dispose();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void addSections(){
        //for the tbl_classlist_section
        String sectionArray[] = {"A","B","C","D","E","F","G","H","I","J","K"};
        dbConnection dbcon = new dbConnection();
        GetterSetterClass gsc = new GetterSetterClass();
        int maxSection = Integer.parseInt(sections.getValue()+"");
        for(int a=0;a<maxSection;a++){
            dbcon.insertDataSilently("tbl_classlist_section", "classlist_id,section,semester,ay_start,ay_end", "'" + id + "','" + sectionArray[a] + "','" + gsc.getSemester() + "','" + gsc.getAcademicYearStart() + "','" + gsc.getAcademicYearEnd() + "'");
        }
    }
    
    private void deleteSections(){
        dbConnection dbcon = new dbConnection();
        GetterSetterClass gsc = new GetterSetterClass();
        dbcon.removeDataSilently("tbl_classlist_section", "classlist_id='" + id + "' and semester='" + gsc.getSemester() + "' and ay_start='" + gsc.getAcademicYearStart() + "' and ay_end='" + gsc.getAcademicYearEnd() + "'");
    }
    
    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        if(leftTable.getRowCount()!=0){
            if(defaultSectionCount==Integer.parseInt(sections.getValue()+"")){
                //do nothing, no changes
            }else if(defaultSectionCount<Integer.parseInt(sections.getValue()+"")){
                deleteSections();
                addSections();
            }else if(defaultSectionCount>Integer.parseInt(sections.getValue()+"")){
                deleteSections();
                addSections();
            }
            addNotFound();
            removeNotFound();
            JOptionPane.showMessageDialog(rootPane, "Data has been updated.");

            dbConnection dbcon = new dbConnection();
            GetterSetterClass gsc = new GetterSetterClass();
            //dbcon4.fillTable(table, "Select cl.id, cl.classlist, COUNT(cld.id) ,COUNT(cl.id) ,de.department_code from tbl_classlist cl, tbl_course co, tbl_department de, tbl_classlist_details cld where cl.course_id=co.id  and co.department_id=de.id and cld.classlist_id=cl.id group by cl.id order by cl.classlist");
            dbcon.fillTable(table, "Select cl.id, cl.classlist, COUNT(cld.id) ,SUM(ss.total_numofhours) ,de.department_code from tbl_classlist cl, tbl_course co, tbl_department de, tbl_classlist_details cld, tbl_subject ss where cl.course_id=co.id  and co.department_id=de.id and cld.classlist_id=cl.id and cld.subject_id=ss.id and cl.semester='" + gsc.getSemester() + "' group by cl.id order by cl.classlist");
            ComboItem course = (ComboItem) comboCourse.getSelectedItem();
            ComboItem yearLevel = (ComboItem) comboYearLevel.getSelectedItem();
            //---------FOR LOGS---------------------
            new dbConnection().recordLog("[Save] Update Checklist ID: "+id+" "+course.getName()+" "+yearLevel.getName()+" Sections: "+Integer.parseInt(sections.getValue()+""));
            //--------------------------------------
            table.setRowSelectionInterval(rowIndex, rowIndex);
            this.dispose();
        }else{
            JOptionPane.showMessageDialog(rootPane, "Please add atleast one (1) subject on classlist.");
            //---------FOR LOGS---------------------
            new dbConnection().recordLog("[Save] Update Checklist - Error No subject selected");
            //--------------------------------------
        }
    }//GEN-LAST:event_btnSaveActionPerformed

    private void addNotFound(){
        int leftCurrentID;
        dbConnection dbcon = new dbConnection();
       
        for(int a=0;a<leftTable.getRowCount();a++){
            leftCurrentID = Integer.parseInt(leftTable.getValueAt(a, 1).toString());
            
            if(Integer.parseInt(dbcon.findOneQuery("COUNT(subject_id)", "tbl_classlist_details", "classlist_id='" + id + "' and subject_id='" + leftCurrentID + "'"))>0){
                //do nothing if subject is already in the class list
            }else{
                dbcon.insertDataSilently("tbl_classlist_details", "classlist_id,subject_id", "'" + id + "','" + leftCurrentID + "'");
            }
        }
    }
    
    private void removeNotFound(){
        int leftCurrentID;
        int IDs[];
        
        dbConnection dbcon = new dbConnection();
        IDs = dbcon.findArrayQuery("subject_id", "tbl_classlist_details", "classlist_id='" + id + "'");
        
        for(int a=0;a<IDs.length;a++){
            boolean isFound=false;
            for(int b=0;b<leftTable.getRowCount();b++){
                leftCurrentID = Integer.parseInt(leftTable.getValueAt(b, 1).toString());
                if(IDs[a]==leftCurrentID){
                    isFound=true;
                    b=leftTable.getRowCount();
                }
            }
            if(isFound==false){
                dbcon.removeDataSilently("tbl_classlist_details", "classlist_id='" + id + "' and subject_id='" + IDs[a] + "'");
            }

        }
    }
        
    private void comboCourseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_comboCourseMouseClicked

    }//GEN-LAST:event_comboCourseMouseClicked

    private void comboCourseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboCourseActionPerformed

    }//GEN-LAST:event_comboCourseActionPerformed

    private void btnToRightActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnToRightActionPerformed
        TableRowMover trm = new TableRowMover();
        trm.moveData(leftTable, rightTable);
        countItems();
    }//GEN-LAST:event_btnToRightActionPerformed

    private void btnToLeftActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnToLeftActionPerformed
        TableRowMover trm = new TableRowMover();
        trm.moveData(rightTable, leftTable);
        countItems();
    }//GEN-LAST:event_btnToLeftActionPerformed

    private void leftTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_leftTableMouseClicked
        /*int x = SubjectList.getSelectedRow();
        if(!(x==-1)){
            System.out.println(x);
        }*/
    }//GEN-LAST:event_leftTableMouseClicked

    private void rightTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rightTableMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_rightTableMouseClicked

    private void countItems(){
        nSubAll.setText(rightTable.getRowCount()+"");
        nSubClasslist.setText(leftTable.getRowCount()+"");
    }
    
    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
         TableRowMover trm = new TableRowMover();
         trm.moveData_AllSelected(rightTable, leftTable);
         countItems();
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        TableRowMover trm = new TableRowMover();
        trm.moveData_AllSelected(leftTable, rightTable);
        countItems();
    }//GEN-LAST:event_jButton10ActionPerformed

    private void searchBoxRightActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchBoxRightActionPerformed
        fillRight();
        deleteCommonRow();
    }//GEN-LAST:event_searchBoxRightActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        //---------FOR LOGS---------------------
        new dbConnection().recordLog("[Close] Update Checklist Dialog");
        //--------------------------------------
    }//GEN-LAST:event_formWindowClosing

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(UpdateClassListDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(UpdateClassListDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(UpdateClassListDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UpdateClassListDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                UpdateClassListDialog dialog = new UpdateClassListDialog(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnToLeft;
    private javax.swing.JButton btnToRight;
    private javax.swing.JComboBox comboCourse;
    private javax.swing.JComboBox comboYearLevel;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable leftTable;
    private javax.swing.JTextField nSubAll;
    private javax.swing.JTextField nSubClasslist;
    private javax.swing.JTable rightTable;
    private javax.swing.JTextField searchBoxRight;
    private javax.swing.JSpinner sections;
    private javax.swing.JTextField txtAY;
    private javax.swing.JTextField txtSem;
    // End of variables declaration//GEN-END:variables
}
