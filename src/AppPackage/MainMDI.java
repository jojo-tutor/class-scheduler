/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package AppPackage;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.Timer;

/**
 *
 * @author Jojo
 */

public class MainMDI extends javax.swing.JFrame {
    
    ManageUserAccountGUI muag;
    ManageSubjectGUI msg;
    ManageClassListGUI mcg;
    ManageRoomGUI mrg;
    ManageCourseGUI mccg;
    ManageYearLevelGUI mnyl;
    ManageDepartmentGUI mdg;
    ManageCollegeGUI mcog;
    ManageFacultyGUI mfg;
    ManageDesignationGUI mdeg;
    
    InquiryClassSchedule ics;
    InquiryRoomSchedule irs;
    InquiryFacultySchedule ifs;
    InquiryUnscheduleSubjects ius;
    
    ReportFacultyLoad rfl;
    ReportClassSchedule rcs;
    ReportRoomSchedule rrs;
    
    GenerateSchedule gs;
    
    public static int instanceOfRoomScheduler=0;
    public static int instanceOfFacultyScheduler=0;
    public static int instanceOfClassScheduler=0;
    
    public MainMDI() {
        initComponents();
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);

        disableFeatures();
        
        welcomeUser();
        fetchAvatar();
        
        getDateTime();
        updateDateTime();
        checkConnectionToServer(this);
        
        setAY_Sem();
        
    }
    
    private void disableFeatures(){
        String userType = new dbConnection().findOneQuery("usertype", "tbl_account", "id='" + new GetterSetterClass().getUID() + "'");
        if(userType.equalsIgnoreCase("Administrator")){
            settingsMenu.setVisible(true);
        }else{
            settingsMenu.setVisible(false);
        }
    }
    
    private void fetchAvatar(){
        int facID = new GetterSetterClass().getUserID();
        dbConnection dbcon = new dbConnection();
        String facultyID = new dbConnection().fetchAvatar(facID);
        if(!facultyID.equals("-1")){
            String facultyName = new dbConnection().findOneQuery("CONCAT(fac.firstname,'-',fac.lastname)", "tbl_faculty fac", "id='" + facID + "'");
            String avatarFilename = new FileReaderWriter().getMyAppDIR()+facultyName+".jpg";
      
            try{
                picAvatar.setIcon(resizeImageIcon(avatarFilename,50,50));
                picAvatar.setToolTipText(facultyName);
            }catch(Exception e){
                //System.err.println(e); if no picture set
                picAvatar.setToolTipText(facultyName);    
            }
        }
    }
    
    private ImageIcon resizeImageIcon(String path,int newWidth, int newHeight){
        
        try{
            ImageIcon avatarIcon = new ImageIcon(new ImageIcon(path).getImage().getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH));
            return avatarIcon;
        }catch(Exception e){
            System.err.println(e);
            return null;
        }
    }
    
    private void welcomeUser(){
        GetterSetterClass gsc = new GetterSetterClass();
        int userID = gsc.getUserID();
        dbConnection dbcon = new dbConnection();
        //String facultyName = dbcon.findOneQuery("CONCAT(b.address,' ',a.firstname,' ',a.middlename,' ',a.lastname,' ',c.extension) as name", "tbl_faculty a, tbl_name_address b, tbl_name_extension c", "(a.address_id=b.id) and (a.extension_id=c.id) and a.id='" + userID + "'");
        String facultyName = dbcon.findOneQuery("CONCAT(b.address,' ',a.lastname) as name", "tbl_faculty a, tbl_name_address b", "(a.address_id=b.id) and a.id='" + userID + "'");
        txtWelcomeUser.setText("Welcome, "+facultyName+" !");
        
    }
    private void getDateTime(){
        //get the time and date
        GetterSetterClass gsc = new GetterSetterClass();
        textFieldDay.setText(gsc.getDayNow());
        textFieldDate.setText(gsc.getDateNow());
        textFieldTime.setText(gsc.getTimeNow());
    }
    private void updateDateTime(){
        ActionListener taskPerformer = new ActionListener() {
            @Override
                public void actionPerformed(ActionEvent evt) {
                    //System.out.println(currentTime());
                    //time.setText(currentTime());
                    getDateTime();
                }
            };
        Timer t = new Timer(1000, taskPerformer);
        t.start();
    }
    //always check the server connection every 5 sec
    //show status where `variable_name` = 'Threads_connected' 
    private void checkConnectionToServer(final JFrame jf){
        ActionListener taskPerformer = new ActionListener() {
            @Override
                public void actionPerformed(ActionEvent evt) {   
                    setPrivate_AY_Sem();
                    dbConnection dbcon = new dbConnection();
                    if(!dbcon.checkConnection()){
                        txtStatus.setText("Disconnected");
                        txtStatus.setForeground(Color.red);
                        ((Timer)evt.getSource()).stop();
                        JOptionPane.showMessageDialog(rootPane, "Opsss.. You are disconnected from the server. Please try to check the connection.");
                        jf.setVisible(false);
                        DBSettings dbs = new DBSettings();
                        dbs.setVisible(true);
                    }else{
                        txtThreads.setText(dbcon.threadsConnected());
                    }
                }
            };
        Timer t = new Timer(5000, taskPerformer);
        t.start();
    }
    
    public void setPrivate_AY_Sem(){
        String forStatus="";
        GetterSetterClass gsc = new GetterSetterClass();
        int sem = gsc.getSemester();
        int ay_start = gsc.getAcademicYearStart();
        int ay_end = gsc.getAcademicYearEnd();
        switch (sem) {
            case 1:
                forStatus = "1st";
                break;
                
            case 2:
                forStatus = "2nd";
                break;
                
            case 3:
                forStatus = "3rd";
                break;
                
            default:
                break;
        }
        
        textAY.setText(ay_start+"-"+ay_end);
        textSem.setText(forStatus);
    }
    
    public void setAY_Sem(){
        int sem=-1;
        int ay_start=-1;
        int ay_end=-1;
        
        String forStatus="";
        
        dbConnection dbcon = new dbConnection();
        sem = Integer.parseInt(dbcon.findOneQuery("semester", "tbl_ay_sem", "status='1'"));
        
        switch (sem) {
            case 1:
                forStatus = "1st";
                ay_start = Integer.parseInt(dbcon.findOneQuery("year_start", "tbl_ay_sem", "status='1'"));
                ay_end = ay_start+1;
                break;
            case 2:
                forStatus = "2nd";
                ay_end = Integer.parseInt(dbcon.findOneQuery("year_end", "tbl_ay_sem", "status='1'"));
                ay_start = ay_end-1;
                break;
            case 3:
                forStatus = "3rd";
                ay_end = Integer.parseInt(dbcon.findOneQuery("year_end", "tbl_ay_sem", "status='1'"));
                ay_start = ay_end-1;
                break;
            default:
                break;
        }
        
        GetterSetterClass gsc = new GetterSetterClass();
        gsc.setAcademicYearStart(ay_start);
        gsc.setAcademicYearEnd(ay_end);
        gsc.setSemester(sem);
        
        textAY.setText(ay_start+"-"+ay_end);
        textSem.setText(forStatus);
        
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        quicklunchPanel = new javax.swing.JPanel();
        btnSubject = new javax.swing.JButton();
        btnChecklist = new javax.swing.JButton();
        btnSchedule = new javax.swing.JButton();
        btnFaculty = new javax.swing.JButton();
        btnRoom = new javax.swing.JButton();
        btnCourse = new javax.swing.JButton();
        btnDepartment = new javax.swing.JButton();
        btnCollege = new javax.swing.JButton();
        txtLogout = new javax.swing.JLabel();
        picAvatar = new javax.swing.JLabel();
        txtWelcomeUser = new javax.swing.JLabel();
        statusPanel = new javax.swing.JPanel();
        labelDateTime = new javax.swing.JLabel();
        textFieldDate = new javax.swing.JTextField();
        labelDateTime1 = new javax.swing.JLabel();
        textFieldTime = new javax.swing.JTextField();
        textFieldDay = new javax.swing.JTextField();
        labelDateTime2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        textAY = new javax.swing.JTextField();
        textSem = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtStatus = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtThreads = new javax.swing.JTextField();
        mainPanel = new javax.swing.JPanel();
        jDesktopPane1 = new javax.swing.JDesktopPane();
        menuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        jMenuItem29 = new javax.swing.JMenuItem();
        jMenuItem24 = new javax.swing.JMenuItem();
        jMenuItem28 = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        jMenuItem18 = new javax.swing.JMenuItem();
        jSeparator8 = new javax.swing.JPopupMenu.Separator();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        viewMenu = new javax.swing.JMenu();
        quicklunchPanelCheckbox = new javax.swing.JCheckBoxMenuItem();
        statusPanelCheckbox = new javax.swing.JCheckBoxMenuItem();
        jSeparator7 = new javax.swing.JPopupMenu.Separator();
        userlogMenuItem = new javax.swing.JMenuItem();
        inquiryMenu = new javax.swing.JMenu();
        jMenuItem13 = new javax.swing.JMenuItem();
        jMenuItem14 = new javax.swing.JMenuItem();
        jMenuItem17 = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        jMenuItem15 = new javax.swing.JMenuItem();
        reportsMenu = new javax.swing.JMenu();
        jMenuItem26 = new javax.swing.JMenuItem();
        jMenuItem21 = new javax.swing.JMenuItem();
        jMenuItem25 = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem16 = new javax.swing.JMenuItem();
        jMenuItem19 = new javax.swing.JMenuItem();
        jMenuItem23 = new javax.swing.JMenuItem();
        managementMenu = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem30 = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        jMenuItem10 = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        jMenuItem12 = new javax.swing.JMenuItem();
        jMenuItem27 = new javax.swing.JMenuItem();
        settingsMenu = new javax.swing.JMenu();
        jMenuItem8 = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem20 = new javax.swing.JMenuItem();
        jMenuItem22 = new javax.swing.JMenuItem();
        helpMenu = new javax.swing.JMenu();
        contentMenuItem = new javax.swing.JMenuItem();
        aboutMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("eScheduler v.2.2.r1");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        quicklunchPanel.setBackground(new java.awt.Color(255, 255, 255));
        quicklunchPanel.setPreferredSize(new java.awt.Dimension(1350, 80));

        btnSubject.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/NewSubject.png"))); // NOI18N
        btnSubject.setText("Subject");
        btnSubject.setFocusable(false);
        btnSubject.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSubject.setPreferredSize(new java.awt.Dimension(90, 90));
        btnSubject.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btnSubject.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSubject.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                btnSubjectFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                btnSubjectFocusLost(evt);
            }
        });
        btnSubject.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSubjectMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSubjectMouseExited(evt);
            }
        });
        btnSubject.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSubjectActionPerformed(evt);
            }
        });

        btnChecklist.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/NewChecklist.png"))); // NOI18N
        btnChecklist.setText("Checklist");
        btnChecklist.setFocusable(false);
        btnChecklist.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnChecklist.setPreferredSize(new java.awt.Dimension(90, 90));
        btnChecklist.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btnChecklist.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnChecklist.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnChecklistMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnChecklistMouseExited(evt);
            }
        });
        btnChecklist.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChecklistActionPerformed(evt);
            }
        });

        btnSchedule.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/NewSchedule.png"))); // NOI18N
        btnSchedule.setText("Scheduler");
        btnSchedule.setFocusable(false);
        btnSchedule.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSchedule.setPreferredSize(new java.awt.Dimension(90, 90));
        btnSchedule.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btnSchedule.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSchedule.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnScheduleMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnScheduleMouseExited(evt);
            }
        });
        btnSchedule.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnScheduleActionPerformed(evt);
            }
        });

        btnFaculty.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/NewFaculty.png"))); // NOI18N
        btnFaculty.setText("Faculty");
        btnFaculty.setFocusable(false);
        btnFaculty.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnFaculty.setPreferredSize(new java.awt.Dimension(90, 90));
        btnFaculty.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btnFaculty.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnFaculty.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnFacultyMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnFacultyMouseExited(evt);
            }
        });
        btnFaculty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFacultyActionPerformed(evt);
            }
        });

        btnRoom.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/NewRoom.png"))); // NOI18N
        btnRoom.setText("Room");
        btnRoom.setFocusable(false);
        btnRoom.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnRoom.setPreferredSize(new java.awt.Dimension(90, 90));
        btnRoom.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btnRoom.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnRoom.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnRoomMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnRoomMouseExited(evt);
            }
        });
        btnRoom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRoomActionPerformed(evt);
            }
        });

        btnCourse.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/NewCourse.png"))); // NOI18N
        btnCourse.setText("Course");
        btnCourse.setFocusable(false);
        btnCourse.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCourse.setPreferredSize(new java.awt.Dimension(90, 90));
        btnCourse.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btnCourse.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnCourse.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCourseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCourseMouseExited(evt);
            }
        });
        btnCourse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCourseActionPerformed(evt);
            }
        });

        btnDepartment.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/NewDepartment.png"))); // NOI18N
        btnDepartment.setText("Department");
        btnDepartment.setFocusable(false);
        btnDepartment.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnDepartment.setPreferredSize(new java.awt.Dimension(90, 90));
        btnDepartment.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btnDepartment.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnDepartment.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnDepartmentMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnDepartmentMouseExited(evt);
            }
        });
        btnDepartment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDepartmentActionPerformed(evt);
            }
        });

        btnCollege.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/NewCollege.png"))); // NOI18N
        btnCollege.setText("College");
        btnCollege.setFocusable(false);
        btnCollege.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCollege.setPreferredSize(new java.awt.Dimension(90, 90));
        btnCollege.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btnCollege.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnCollege.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCollegeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCollegeMouseExited(evt);
            }
        });
        btnCollege.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCollegeActionPerformed(evt);
            }
        });

        txtLogout.setForeground(new java.awt.Color(0, 2, 254));
        txtLogout.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtLogout.setText("<html><u>Logout</u></html>");
        txtLogout.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        txtLogout.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        txtLogout.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        txtLogout.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtLogoutMouseClicked(evt);
            }
        });

        picAvatar.setBackground(new java.awt.Color(102, 255, 255));
        picAvatar.setOpaque(true);

        txtWelcomeUser.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txtWelcomeUser.setText("Welcome, Sir Jojo!");
        txtWelcomeUser.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        txtWelcomeUser.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout quicklunchPanelLayout = new javax.swing.GroupLayout(quicklunchPanel);
        quicklunchPanel.setLayout(quicklunchPanelLayout);
        quicklunchPanelLayout.setHorizontalGroup(
            quicklunchPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(quicklunchPanelLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(btnSubject, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnChecklist, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnSchedule, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnFaculty, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnRoom, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnCourse, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnDepartment, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnCollege, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 203, Short.MAX_VALUE)
                .addComponent(txtWelcomeUser, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(picAvatar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtLogout, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12))
        );
        quicklunchPanelLayout.setVerticalGroup(
            quicklunchPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(quicklunchPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(btnCourse, javax.swing.GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE)
                .addComponent(btnSchedule, javax.swing.GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE)
                .addComponent(btnChecklist, javax.swing.GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE)
                .addComponent(btnSubject, javax.swing.GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE)
                .addComponent(btnFaculty, javax.swing.GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE)
                .addComponent(btnRoom, javax.swing.GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE)
                .addComponent(btnDepartment, javax.swing.GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE)
                .addComponent(btnCollege, javax.swing.GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE))
            .addGroup(quicklunchPanelLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(quicklunchPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(picAvatar, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
                    .addComponent(txtLogout)
                    .addComponent(txtWelcomeUser, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        btnSubject.getAccessibleContext().setAccessibleName("");

        statusPanel.setBackground(new java.awt.Color(255, 255, 255));
        statusPanel.setPreferredSize(new java.awt.Dimension(1366, 48));

        labelDateTime.setText("Date:");

        textFieldDate.setEditable(false);
        textFieldDate.setBackground(new java.awt.Color(255, 255, 255));
        textFieldDate.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        textFieldDate.setText("December 25, 2016");
        textFieldDate.setBorder(null);

        labelDateTime1.setText("Time:");

        textFieldTime.setEditable(false);
        textFieldTime.setBackground(new java.awt.Color(255, 255, 255));
        textFieldTime.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        textFieldTime.setText("03:00:01 PM");
        textFieldTime.setBorder(null);

        textFieldDay.setEditable(false);
        textFieldDay.setBackground(new java.awt.Color(255, 255, 255));
        textFieldDay.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        textFieldDay.setText("Sunday");
        textFieldDay.setBorder(null);

        labelDateTime2.setText("Today is:");

        jLabel1.setText("Academic Year:");

        textAY.setEditable(false);
        textAY.setBackground(new java.awt.Color(255, 255, 255));
        textAY.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        textAY.setText("2015-2016");
        textAY.setBorder(null);

        textSem.setEditable(false);
        textSem.setBackground(new java.awt.Color(255, 255, 255));
        textSem.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        textSem.setText("1st");
        textSem.setBorder(null);

        jLabel2.setText("Semester:");

        jLabel3.setText("<html>&copy 2016 All rights reserved. | <a href=\"https://www.facebook.com/jojo.choyanz\">Jojo E. Tutor</a></html> ");

        jLabel4.setText("Status:");

        txtStatus.setEditable(false);
        txtStatus.setBackground(new java.awt.Color(255, 255, 255));
        txtStatus.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtStatus.setText("Connected");
        txtStatus.setBorder(null);

        jLabel5.setText("Threads Connected:");

        txtThreads.setEditable(false);
        txtThreads.setBackground(new java.awt.Color(255, 255, 255));
        txtThreads.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtThreads.setText("0");
        txtThreads.setBorder(null);

        javax.swing.GroupLayout statusPanelLayout = new javax.swing.GroupLayout(statusPanel);
        statusPanel.setLayout(statusPanelLayout);
        statusPanelLayout.setHorizontalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, statusPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtThreads, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(textAY, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(textSem, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(labelDateTime2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(textFieldDay, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(labelDateTime)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(textFieldDate, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(labelDateTime1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(textFieldTime, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        statusPanelLayout.setVerticalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(labelDateTime1)
                        .addComponent(textFieldDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(labelDateTime)
                        .addComponent(textFieldTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(txtThreads, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(labelDateTime2)
                        .addComponent(textFieldDay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1)
                        .addComponent(textAY, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel2)
                        .addComponent(textSem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel4)
                        .addComponent(txtStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        jDesktopPane1.setMaximumSize(new java.awt.Dimension(1366, 768));

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jDesktopPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1366, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mainPanelLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jDesktopPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 613, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        fileMenu.setText("File");
        fileMenu.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        jMenuItem29.setText("Class Scheduler");
        jMenuItem29.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem29ActionPerformed(evt);
            }
        });
        fileMenu.add(jMenuItem29);

        jMenuItem24.setText("Room Scheduler");
        jMenuItem24.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem24ActionPerformed(evt);
            }
        });
        fileMenu.add(jMenuItem24);

        jMenuItem28.setText("Faculty Scheduler");
        jMenuItem28.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem28ActionPerformed(evt);
            }
        });
        fileMenu.add(jMenuItem28);
        fileMenu.add(jSeparator3);

        jMenuItem18.setText("Generate Schedule");
        jMenuItem18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem18ActionPerformed(evt);
            }
        });
        fileMenu.add(jMenuItem18);
        fileMenu.add(jSeparator8);

        jMenuItem9.setText("Log Out");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        fileMenu.add(jMenuItem9);

        jMenuItem5.setText("Exit");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        fileMenu.add(jMenuItem5);

        menuBar.add(fileMenu);

        viewMenu.setText("View");

        quicklunchPanelCheckbox.setSelected(true);
        quicklunchPanelCheckbox.setText("Quicklunch Panel");
        quicklunchPanelCheckbox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quicklunchPanelCheckboxActionPerformed(evt);
            }
        });
        viewMenu.add(quicklunchPanelCheckbox);

        statusPanelCheckbox.setSelected(true);
        statusPanelCheckbox.setText("Status Panel");
        statusPanelCheckbox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                statusPanelCheckboxActionPerformed(evt);
            }
        });
        viewMenu.add(statusPanelCheckbox);
        viewMenu.add(jSeparator7);

        userlogMenuItem.setText("User Log");
        userlogMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                userlogMenuItemActionPerformed(evt);
            }
        });
        viewMenu.add(userlogMenuItem);

        menuBar.add(viewMenu);

        inquiryMenu.setText("Inquiry");
        inquiryMenu.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        jMenuItem13.setText("Class Schedule");
        jMenuItem13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem13ActionPerformed(evt);
            }
        });
        inquiryMenu.add(jMenuItem13);

        jMenuItem14.setText("Room Schedule");
        jMenuItem14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem14ActionPerformed(evt);
            }
        });
        inquiryMenu.add(jMenuItem14);

        jMenuItem17.setText("Faculty Load");
        jMenuItem17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem17ActionPerformed(evt);
            }
        });
        inquiryMenu.add(jMenuItem17);
        inquiryMenu.add(jSeparator4);

        jMenuItem15.setText("Unscheduled Subjects");
        jMenuItem15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem15ActionPerformed(evt);
            }
        });
        inquiryMenu.add(jMenuItem15);

        menuBar.add(inquiryMenu);

        reportsMenu.setText("Reports");
        reportsMenu.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        jMenuItem26.setText("Class Schedule");
        jMenuItem26.setToolTipText("");
        jMenuItem26.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem26ActionPerformed(evt);
            }
        });
        reportsMenu.add(jMenuItem26);

        jMenuItem21.setText("Room Schedule");
        jMenuItem21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem21ActionPerformed(evt);
            }
        });
        reportsMenu.add(jMenuItem21);

        jMenuItem25.setText("Faculty Load");
        jMenuItem25.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem25ActionPerformed(evt);
            }
        });
        reportsMenu.add(jMenuItem25);
        reportsMenu.add(jSeparator6);

        jMenu2.setText("Set Preferences");

        jMenuItem16.setText("Prepared by");
        jMenuItem16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem16ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem16);

        jMenuItem19.setText("Noted by");
        jMenuItem19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem19ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem19);

        jMenuItem23.setText("Approved by");
        jMenuItem23.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem23ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem23);

        reportsMenu.add(jMenu2);

        menuBar.add(reportsMenu);

        managementMenu.setText("Management");

        jMenuItem2.setText("Subject Management");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        managementMenu.add(jMenuItem2);

        jMenuItem3.setText("Checklist Management");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        managementMenu.add(jMenuItem3);

        jMenuItem4.setText("Room Management");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        managementMenu.add(jMenuItem4);

        jMenuItem30.setText("Class Size Management");
        jMenuItem30.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem30ActionPerformed(evt);
            }
        });
        managementMenu.add(jMenuItem30);
        managementMenu.add(jSeparator1);

        jMenuItem7.setText("Year Level Management");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        managementMenu.add(jMenuItem7);

        jMenuItem6.setText("Course Management");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        managementMenu.add(jMenuItem6);

        jMenuItem11.setText("Department Management");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        managementMenu.add(jMenuItem11);

        jMenuItem10.setText("College Management");
        jMenuItem10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem10ActionPerformed(evt);
            }
        });
        managementMenu.add(jMenuItem10);
        managementMenu.add(jSeparator2);

        jMenuItem12.setText("Faculty Management");
        jMenuItem12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem12ActionPerformed(evt);
            }
        });
        managementMenu.add(jMenuItem12);

        jMenuItem27.setText("Designation Management");
        jMenuItem27.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem27ActionPerformed(evt);
            }
        });
        managementMenu.add(jMenuItem27);

        menuBar.add(managementMenu);

        settingsMenu.setText("Settings");

        jMenuItem8.setText("AY & Semester");
        jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem8ActionPerformed(evt);
            }
        });
        settingsMenu.add(jMenuItem8);
        settingsMenu.add(jSeparator5);

        jMenuItem1.setText("User Account");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        settingsMenu.add(jMenuItem1);

        jMenuItem20.setText("Database");
        jMenuItem20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem20ActionPerformed(evt);
            }
        });
        settingsMenu.add(jMenuItem20);

        jMenuItem22.setText("Logs");
        jMenuItem22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem22ActionPerformed(evt);
            }
        });
        settingsMenu.add(jMenuItem22);

        menuBar.add(settingsMenu);

        helpMenu.setMnemonic('h');
        helpMenu.setText("Help");

        contentMenuItem.setMnemonic('c');
        contentMenuItem.setText("Help Contents");
        contentMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                contentMenuItemActionPerformed(evt);
            }
        });
        helpMenu.add(contentMenuItem);

        aboutMenuItem.setMnemonic('a');
        aboutMenuItem.setText("About");
        aboutMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutMenuItemActionPerformed(evt);
            }
        });
        helpMenu.add(aboutMenuItem);

        menuBar.add(helpMenu);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(statusPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(quicklunchPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 1366, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(quicklunchPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(statusPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        int reply = JOptionPane.showConfirmDialog(rootPane, "This action will logout your account and closes the application. \nDo you want to continue exit?", "Exit", JOptionPane.YES_NO_OPTION, 3);
        if(reply==JOptionPane.YES_OPTION){
            //---------FOR LOGS---------------------
            new dbConnection().recordLog("[Close] Software..");
            //--------------------------------------
            GetterSetterClass gsc = new GetterSetterClass();
            gsc.setUserID(-1);
            System.exit(0);
        }
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        showSubject();
    }//GEN-LAST:event_jMenuItem2ActionPerformed
    private void showSubject(){
        try{
            msg.dispose();
            //---------FOR LOGS---------------------
            new dbConnection().recordLog("[Close] Subject Management");
            //--------------------------------------
        }catch(NullPointerException e){
            //no existing form yet
        }
        msg = new ManageSubjectGUI();
        jDesktopPane1.add(msg);
        msg.setSize(jDesktopPane1.getWidth(), jDesktopPane1.getHeight());
        msg.setLocation(jDesktopPane1.getLocation().x,jDesktopPane1.getLocation().y);
        msg.setVisible(true);
    }
    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        showChecklist();
    }//GEN-LAST:event_jMenuItem3ActionPerformed
    private void showChecklist(){
        try{    
            mcg.dispose();
            //---------FOR LOGS---------------------
            new dbConnection().recordLog("[Close] Checklist Management");
            //--------------------------------------
        }catch(NullPointerException e){
            //no existing form yet
        }
        mcg = new ManageClassListGUI();
        jDesktopPane1.add(mcg);
        mcg.setSize(jDesktopPane1.getWidth(), jDesktopPane1.getHeight());
        mcg.setLocation(jDesktopPane1.getLocation().x,jDesktopPane1.getLocation().y);
        mcg.setVisible(true);
    }
    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        showRoom();
    }//GEN-LAST:event_jMenuItem4ActionPerformed
    private void showRoom(){
        try{
            mrg.dispose();
            //---------FOR LOGS---------------------
            new dbConnection().recordLog("[Close] Room Management");
            //--------------------------------------
        }catch(NullPointerException e){
            //no existing form yet
        }
        mrg = new ManageRoomGUI();
        jDesktopPane1.add(mrg);
        mrg.setSize(jDesktopPane1.getWidth(), jDesktopPane1.getHeight());
        mrg.setLocation(jDesktopPane1.getLocation().x,jDesktopPane1.getLocation().y);
        mrg.setVisible(true);
    }
    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        showCourse();
    }//GEN-LAST:event_jMenuItem6ActionPerformed
    private void showCourse(){
        try{
            mccg.dispose();
            //---------FOR LOGS---------------------
            new dbConnection().recordLog("[Close] Course Management");
            //--------------------------------------
        }catch(NullPointerException e){
            //no existing form yet
        }
        mccg = new ManageCourseGUI();
        jDesktopPane1.add(mccg);
        mccg.setSize(jDesktopPane1.getWidth(), jDesktopPane1.getHeight());
        mccg.setLocation(jDesktopPane1.getLocation().x,jDesktopPane1.getLocation().y);
        mccg.setVisible(true);
    }
    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        showYearLevel();
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void showYearLevel(){
        try{
            mnyl.dispose();
            //---------FOR LOGS---------------------
            new dbConnection().recordLog("[Close] Year level Management");
            //--------------------------------------
        }catch(NullPointerException e){
            //no existing form yet
        }
        mnyl = new ManageYearLevelGUI();
        jDesktopPane1.add(mnyl);
        mnyl.setSize(jDesktopPane1.getWidth(), jDesktopPane1.getHeight());
        mnyl.setLocation(jDesktopPane1.getLocation().x,jDesktopPane1.getLocation().y);
        mnyl.setVisible(true);
    }
    
    private void jMenuItem8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem8ActionPerformed
        AcademicYearAndSemesterDialog ayasd = new AcademicYearAndSemesterDialog(this,true);
        ayasd.setLocationRelativeTo(null);
        ayasd.setVisible(true);
    }//GEN-LAST:event_jMenuItem8ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        showUserAccount();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void showUserAccount(){
        try{
            muag.dispose();
            //---------FOR LOGS---------------------
            new dbConnection().recordLog("[Close] User Account Management");
            //--------------------------------------
        }catch(NullPointerException e){
            //no existing form yet
        }
        muag = new ManageUserAccountGUI();
        jDesktopPane1.add(muag);
        muag.setSize(jDesktopPane1.getWidth(), jDesktopPane1.getHeight());
        muag.setLocation(jDesktopPane1.getLocation().x,jDesktopPane1.getLocation().y);
        muag.setVisible(true);
    }
    
    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        showDepartment();
    }//GEN-LAST:event_jMenuItem11ActionPerformed
    private void showDepartment(){
        try{
            mdg.dispose();
            //---------FOR LOGS---------------------
            new dbConnection().recordLog("[Close] Department Management");
            //--------------------------------------
        }catch(NullPointerException e){
            //no existing form yet
        }
        mdg = new ManageDepartmentGUI();
        jDesktopPane1.add(mdg);
        mdg.setSize(jDesktopPane1.getWidth(), jDesktopPane1.getHeight());
        mdg.setLocation(jDesktopPane1.getLocation().x,jDesktopPane1.getLocation().y);
        mdg.setVisible(true);
    }
    private void jMenuItem12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem12ActionPerformed
        showFaculty();
    }//GEN-LAST:event_jMenuItem12ActionPerformed
    private void showFaculty(){
        try{
            mfg.dispose();
            //---------FOR LOGS---------------------
            new dbConnection().recordLog("[Close] Faculty Management");
            //--------------------------------------
        }catch(NullPointerException e){
            //no existing form yet
        }
        mfg = new ManageFacultyGUI();
        jDesktopPane1.add(mfg);
        mfg.setSize(jDesktopPane1.getWidth(), jDesktopPane1.getHeight());
        mfg.setLocation(jDesktopPane1.getLocation().x,jDesktopPane1.getLocation().y);
        mfg.setVisible(true);
    }
    private void jMenuItem24ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem24ActionPerformed
        showRoomScheduler();
    }//GEN-LAST:event_jMenuItem24ActionPerformed
    private void showRoomScheduler(){
        instanceOfRoomScheduler++;
        switch(instanceOfRoomScheduler){
            case 1:
                RoomScheduler1 rs1 = new RoomScheduler1();
                if(rs1.isVisible()){
                    rs1.requestFocus();
                }else{
                    rs1.setTitle("Room Scheduler Instance 1");
                    rs1.setVisible(true);
                }
                break;
            case 2:
                RoomScheduler2 rs2 = new RoomScheduler2();
                if(rs2.isVisible()){
                    rs2.requestFocus();
                }else{
                    rs2.setTitle("Room Scheduler Instance 2");
                    rs2.setVisible(true);
                }
                break;
            case 3:
                RoomScheduler3 rs3 = new RoomScheduler3();
                if(rs3.isVisible()){
                    rs3.requestFocus();
                }else{
                    rs3.setTitle("Room Scheduler Instance 3");
                    rs3.setVisible(true);
                }
                break;
            case 4:
                RoomScheduler4 rs4 = new RoomScheduler4();
                if(rs4.isVisible()){
                    rs4.requestFocus();
                }else{
                    rs4.setTitle("Room Scheduler Instance 4");
                    rs4.setVisible(true);
                }
                break;
            case 5:
                RoomScheduler5 rs5 = new RoomScheduler5();
                if(rs5.isVisible()){
                    rs5.requestFocus();
                }else{
                    rs5.setTitle("Room Scheduler Instance 5");
                    rs5.setVisible(true);
                }
                break;
            default:
                instanceOfRoomScheduler=0;
                break;
        }
    }
    
    private void showReportFL(){
        try{
            rfl.dispose();
            //---------FOR LOGS---------------------
            new dbConnection().recordLog("[Close] Faculty Load Report");
            //--------------------------------------
        }catch(NullPointerException e){
            //no existing form yet
        }
        rfl = new ReportFacultyLoad();
        jDesktopPane1.add(rfl);
        rfl.setSize(jDesktopPane1.getWidth(), jDesktopPane1.getHeight());
        rfl.setLocation(jDesktopPane1.getLocation().x,jDesktopPane1.getLocation().y);
        rfl.setVisible(true);
    }
    
    private void jMenuItem25ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem25ActionPerformed
        showReportFL();
    }//GEN-LAST:event_jMenuItem25ActionPerformed

    private void btnSubjectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSubjectActionPerformed
        showSubject();
    }//GEN-LAST:event_btnSubjectActionPerformed

    private void btnChecklistActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChecklistActionPerformed
        showChecklist();
    }//GEN-LAST:event_btnChecklistActionPerformed

    private void btnScheduleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnScheduleActionPerformed
        showClassScheduler();
    }//GEN-LAST:event_btnScheduleActionPerformed

    private void btnFacultyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFacultyActionPerformed
        showFaculty();
    }//GEN-LAST:event_btnFacultyActionPerformed

    private void btnRoomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRoomActionPerformed
        showRoom();
    }//GEN-LAST:event_btnRoomActionPerformed

    private void btnCourseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCourseActionPerformed
        showCourse();
    }//GEN-LAST:event_btnCourseActionPerformed

    private void btnDepartmentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDepartmentActionPerformed
        showDepartment();
    }//GEN-LAST:event_btnDepartmentActionPerformed

    private void btnSubjectFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_btnSubjectFocusGained
        
    }//GEN-LAST:event_btnSubjectFocusGained

    private void btnSubjectFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_btnSubjectFocusLost
        
    }//GEN-LAST:event_btnSubjectFocusLost
    
    private void btnSubjectMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSubjectMouseEntered
        btnSubject.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/NewSubject_Hover.png")));
        btnSubject.setFont(new java.awt.Font("Tahoma", 1, 11));
    }//GEN-LAST:event_btnSubjectMouseEntered

    private void btnSubjectMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSubjectMouseExited
        btnSubject.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/NewSubject.png")));
        btnSubject.setFont(new java.awt.Font("Tahoma", 0, 11));
    }//GEN-LAST:event_btnSubjectMouseExited

    private void btnChecklistMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnChecklistMouseEntered
        btnChecklist.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/NewChecklist_Hover.png")));
        btnChecklist.setFont(new java.awt.Font("Tahoma", 1, 11));
    }//GEN-LAST:event_btnChecklistMouseEntered

    private void btnChecklistMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnChecklistMouseExited
        btnChecklist.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/NewChecklist.png")));
        btnChecklist.setFont(new java.awt.Font("Tahoma", 0, 11));
    }//GEN-LAST:event_btnChecklistMouseExited

    private void btnScheduleMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnScheduleMouseEntered
        btnSchedule.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/NewSchedule_Hover.png")));
        btnSchedule.setFont(new java.awt.Font("Tahoma", 1, 11));
    }//GEN-LAST:event_btnScheduleMouseEntered

    private void btnScheduleMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnScheduleMouseExited
        btnSchedule.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/NewSchedule.png")));
        btnSchedule.setFont(new java.awt.Font("Tahoma", 0, 11));
    }//GEN-LAST:event_btnScheduleMouseExited

    private void btnFacultyMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnFacultyMouseEntered
        btnFaculty.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/NewFaculty_Hover.png")));
        btnFaculty.setFont(new java.awt.Font("Tahoma", 1, 11));
    }//GEN-LAST:event_btnFacultyMouseEntered

    private void btnFacultyMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnFacultyMouseExited
        btnFaculty.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/NewFaculty.png")));
        btnFaculty.setFont(new java.awt.Font("Tahoma", 0, 11));
    }//GEN-LAST:event_btnFacultyMouseExited

    private void btnRoomMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnRoomMouseEntered
        btnRoom.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/NewRoom_Hover.png")));
        btnRoom.setFont(new java.awt.Font("Tahoma", 1, 11));
    }//GEN-LAST:event_btnRoomMouseEntered

    private void btnRoomMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnRoomMouseExited
        btnRoom.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/NewRoom.png")));
        btnRoom.setFont(new java.awt.Font("Tahoma", 0, 11));
    }//GEN-LAST:event_btnRoomMouseExited

    private void btnCourseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCourseMouseEntered
        btnCourse.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/NewCourse_Hover.png")));
        btnCourse.setFont(new java.awt.Font("Tahoma", 1, 11));
    }//GEN-LAST:event_btnCourseMouseEntered

    private void btnCourseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCourseMouseExited
        btnCourse.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/NewCourse.png")));
        btnCourse.setFont(new java.awt.Font("Tahoma", 0, 11));
    }//GEN-LAST:event_btnCourseMouseExited

    private void btnDepartmentMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDepartmentMouseEntered
        btnDepartment.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/NewDepartment_Hover.png")));
        btnDepartment.setFont(new java.awt.Font("Tahoma", 1, 11));
    }//GEN-LAST:event_btnDepartmentMouseEntered

    private void btnDepartmentMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDepartmentMouseExited
        btnDepartment.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/NewDepartment.png")));
        btnDepartment.setFont(new java.awt.Font("Tahoma", 0, 11));
    }//GEN-LAST:event_btnDepartmentMouseExited

    private void btnCollegeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCollegeMouseEntered
        btnCollege.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/NewCollege_Hover.png")));
        btnCollege.setFont(new java.awt.Font("Tahoma", 1, 11));
    }//GEN-LAST:event_btnCollegeMouseEntered

    private void btnCollegeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCollegeMouseExited
        btnCollege.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/NewCollege.png")));
        btnCollege.setFont(new java.awt.Font("Tahoma", 0, 11));
    }//GEN-LAST:event_btnCollegeMouseExited
    private void showCollege(){
        try{
            mcog.dispose();
            //---------FOR LOGS---------------------
            new dbConnection().recordLog("[Close] College Management");
            //--------------------------------------
        }catch(NullPointerException e){
            //no existing form yet
        }
        mcog = new ManageCollegeGUI();
        jDesktopPane1.add(mcog);
        mcog.setSize(jDesktopPane1.getWidth(), jDesktopPane1.getHeight());
        mcog.setLocation(jDesktopPane1.getLocation().x,jDesktopPane1.getLocation().y);
        mcog.setVisible(true);
    }
    private void jMenuItem10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem10ActionPerformed
        showCollege();
    }//GEN-LAST:event_jMenuItem10ActionPerformed

    private void btnCollegeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCollegeActionPerformed
        showCollege();
    }//GEN-LAST:event_btnCollegeActionPerformed

    private void contentMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_contentMenuItemActionPerformed
        JOptionPane.showMessageDialog(rootPane, "Fore more info, please read the documentation.");
    }//GEN-LAST:event_contentMenuItemActionPerformed

    private void aboutMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aboutMenuItemActionPerformed
        About ab = new About(this,true);
        ab.setVisible(true);
    }//GEN-LAST:event_aboutMenuItemActionPerformed

    private void txtLogoutMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtLogoutMouseClicked
        logout();
    }//GEN-LAST:event_txtLogoutMouseClicked
    
    private void showDesignation(){
        try{
            mdeg.dispose();
            //---------FOR LOGS---------------------
            new dbConnection().recordLog("[Close] Designation Management");
            //--------------------------------------
        }catch(NullPointerException e){
            //no existing form yet
        }
        mdeg = new ManageDesignationGUI();
        jDesktopPane1.add(mdeg);
        mdeg.setSize(jDesktopPane1.getWidth(), jDesktopPane1.getHeight());
        mdeg.setLocation(jDesktopPane1.getLocation().x,jDesktopPane1.getLocation().y);
        mdeg.setVisible(true);
    }
    private void jMenuItem27ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem27ActionPerformed
        showDesignation();
    }//GEN-LAST:event_jMenuItem27ActionPerformed
    
    private void showFacultyScheduler(){
        instanceOfFacultyScheduler++;
        switch(instanceOfFacultyScheduler){
            case 1:
                FacultyScheduler1 fs1 = new FacultyScheduler1();
                if(fs1.isVisible()){
                    fs1.requestFocus();
                }else{
                    fs1.setTitle("Faculty Scheduler Instance 1");
                    fs1.setVisible(true);
                }
                break;
            case 2:
                FacultyScheduler2 fs2 = new FacultyScheduler2();
                if(fs2.isVisible()){
                    fs2.requestFocus();
                }else{
                    fs2.setTitle("Faculty Scheduler Instance 2");
                    fs2.setVisible(true);
                }
                break;
            case 3:
                FacultyScheduler3 fs3 = new FacultyScheduler3();
                if(fs3.isVisible()){
                    fs3.requestFocus();
                }else{
                    fs3.setTitle("Faculty Scheduler Instance 3");
                    fs3.setVisible(true);
                }
                break;
            case 4:
                FacultyScheduler4 fs4 = new FacultyScheduler4();
                if(fs4.isVisible()){
                    fs4.requestFocus();
                }else{
                    fs4.setTitle("Faculty Scheduler Instance 4");
                    fs4.setVisible(true);
                }
                break;
            case 5:
                FacultyScheduler5 fs5 = new FacultyScheduler5();
                if(fs5.isVisible()){
                    fs5.requestFocus();
                }else{
                    fs5.setTitle("Faculty Scheduler Instance 5");
                    fs5.setVisible(true);
                }
                break;
            default:
                instanceOfFacultyScheduler=0;
                break;
        }
    }
    
    private void jMenuItem28ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem28ActionPerformed
        showFacultyScheduler();
    }//GEN-LAST:event_jMenuItem28ActionPerformed

    private void jMenuItem29ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem29ActionPerformed
        showClassScheduler();
    }//GEN-LAST:event_jMenuItem29ActionPerformed

    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem9ActionPerformed
        logout();
    }//GEN-LAST:event_jMenuItem9ActionPerformed

    private void jMenuItem13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem13ActionPerformed
        showInquiryCS();
    }//GEN-LAST:event_jMenuItem13ActionPerformed
    
    private void showInquiryCS(){
        try{
            ics.dispose();
            //---------FOR LOGS---------------------
            new dbConnection().recordLog("[Close] Class Schedule Inquiry");
            //--------------------------------------
        }catch(NullPointerException e){
            //no existing form yet
        }
        ics = new InquiryClassSchedule();
        jDesktopPane1.add(ics);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        ics.setSize(jDesktopPane1.getWidth(), jDesktopPane1.getHeight());
        ics.setLocation(jDesktopPane1.getLocation().x,jDesktopPane1.getLocation().y);
        ics.setVisible(true);
    }
    
    private void jMenuItem15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem15ActionPerformed
        showInquiryUS();
    }//GEN-LAST:event_jMenuItem15ActionPerformed

    private void showInquiryUS(){
        try{
            ius.dispose();
            //---------FOR LOGS---------------------
            new dbConnection().recordLog("[Close] Un-scheduled Subject Inquiry");
            //--------------------------------------
        }catch(NullPointerException e){
            //no existing form yet    
        }
        ius = new InquiryUnscheduleSubjects();
        jDesktopPane1.add(ius);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        ius.setSize(jDesktopPane1.getWidth(), jDesktopPane1.getHeight());
        ius.setLocation(jDesktopPane1.getLocation().x,jDesktopPane1.getLocation().y);
        ius.setVisible(true);
    }
    
    private void jMenuItem14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem14ActionPerformed
        showInquiryRS();
    }//GEN-LAST:event_jMenuItem14ActionPerformed

    private void jMenuItem17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem17ActionPerformed
        showInquiryFS();
    }//GEN-LAST:event_jMenuItem17ActionPerformed

    private void showReportCS(){
        try{
            rcs.dispose();
            //---------FOR LOGS---------------------
            new dbConnection().recordLog("[Close] Class Schedule Report");
            //--------------------------------------
        }catch(NullPointerException e){
            //no existing form yet
        }
        rcs = new ReportClassSchedule();
        jDesktopPane1.add(rcs);
        rcs.setSize(jDesktopPane1.getWidth(), jDesktopPane1.getHeight());
        rcs.setLocation(jDesktopPane1.getLocation().x,jDesktopPane1.getLocation().y);
        rcs.setVisible(true);
    }
    
    private void jMenuItem26ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem26ActionPerformed
        showReportCS();
    }//GEN-LAST:event_jMenuItem26ActionPerformed

    private void jMenuItem16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem16ActionPerformed
        PreparedByPopUpDialog pbp = new PreparedByPopUpDialog(this,true);
        pbp.setLocation((this.getWidth()-442)/2,(this.getHeight()-328)/2);
        pbp.setVisible(true);
    }//GEN-LAST:event_jMenuItem16ActionPerformed

    private void jMenuItem19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem19ActionPerformed
        NotedByPopUpDialog nbp = new NotedByPopUpDialog(this,true);
        nbp.setLocation((this.getWidth()-442)/2,(this.getHeight()-328)/2);
        nbp.setVisible(true);
    }//GEN-LAST:event_jMenuItem19ActionPerformed

    private void jMenuItem23ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem23ActionPerformed
        ApprovedByPopUpDialog abp = new ApprovedByPopUpDialog(this,true);
        abp.setLocation((this.getWidth()-442)/2,(this.getHeight()-328)/2);
        abp.setVisible(true);
    }//GEN-LAST:event_jMenuItem23ActionPerformed

    private void jMenuItem30ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem30ActionPerformed
        ClassSizeDialog csd = new ClassSizeDialog(this,true);
        csd.setLocation((this.getWidth()-420)/2,(this.getHeight()-306)/2);
        csd.setVisible(true);
    }//GEN-LAST:event_jMenuItem30ActionPerformed

    private void jMenuItem20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem20ActionPerformed
        SettingsDatabaseDialog sd = new SettingsDatabaseDialog(this,true);
        sd.setLocation((this.getWidth()-540)/2,(this.getHeight()-567)/2);
        sd.setVisible(true);
    }//GEN-LAST:event_jMenuItem20ActionPerformed

    private void quicklunchPanelCheckboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quicklunchPanelCheckboxActionPerformed
        quicklunchPanel.setVisible(quicklunchPanelCheckbox.isSelected());
    }//GEN-LAST:event_quicklunchPanelCheckboxActionPerformed

    private void statusPanelCheckboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_statusPanelCheckboxActionPerformed
        statusPanel.setVisible(statusPanelCheckbox.isSelected());
    }//GEN-LAST:event_statusPanelCheckboxActionPerformed

    private void showGenerateSchedule(){
        try{
            gs.dispose();
            //---------FOR LOGS---------------------
            new dbConnection().recordLog("[Close] Generate Schedule");
            //--------------------------------------
        }catch(NullPointerException e){
            //no existing form yet
        }
        gs = new GenerateSchedule();
        jDesktopPane1.add(gs);
        gs.setSize(jDesktopPane1.getWidth(), jDesktopPane1.getHeight());
        gs.setLocation(jDesktopPane1.getLocation().x,jDesktopPane1.getLocation().y);
        gs.setVisible(true);
    }
    
    private void jMenuItem18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem18ActionPerformed
        showGenerateSchedule();
    }//GEN-LAST:event_jMenuItem18ActionPerformed

    private void showReportRS(){
        try{
            rrs.dispose();
            //---------FOR LOGS---------------------
            new dbConnection().recordLog("[Close] Room Schedule Report");
            //--------------------------------------
        }catch(NullPointerException e){
            //no existing form yet
        }
        rrs = new ReportRoomSchedule();
        jDesktopPane1.add(rrs);
        rrs.setSize(jDesktopPane1.getWidth(), jDesktopPane1.getHeight());
        rrs.setLocation(jDesktopPane1.getLocation().x,jDesktopPane1.getLocation().y);
        rrs.setVisible(true);
    }
    
    private void jMenuItem21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem21ActionPerformed
        showReportRS();
    }//GEN-LAST:event_jMenuItem21ActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        //---------FOR LOGS---------------------
        new dbConnection().recordLog("[Close] Software..");
        //--------------------------------------
        System.exit(0);
    }//GEN-LAST:event_formWindowClosing

    private void jMenuItem22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem22ActionPerformed
        UserLogsDialog uld = new UserLogsDialog(null,true,2);
        uld.setVisible(true);
    }//GEN-LAST:event_jMenuItem22ActionPerformed

    private void userlogMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_userlogMenuItemActionPerformed
        UserLogsDialog uld = new UserLogsDialog(null,true,1);
        uld.setVisible(true);
    }//GEN-LAST:event_userlogMenuItemActionPerformed
    
    private void showInquiryFS(){
        try{
            ifs.dispose();
            //---------FOR LOGS---------------------
            new dbConnection().recordLog("[Close] Faculty Schedule Inquiry");
            //--------------------------------------
        }catch(NullPointerException e){
            //no existing form yet
        }
        ifs = new InquiryFacultySchedule();
        jDesktopPane1.add(ifs);
        ifs.setSize(jDesktopPane1.getWidth(), jDesktopPane1.getHeight());
        ifs.setLocation(jDesktopPane1.getLocation().x,jDesktopPane1.getLocation().y);
        ifs.setVisible(true);
    }
    
    private void showInquiryRS(){
        try{
            irs.dispose();
            //---------FOR LOGS---------------------
            new dbConnection().recordLog("[Close] Room Schedule Inquiry");
            //--------------------------------------
        }catch(NullPointerException e){
            //no existing form yet
        }
        irs = new InquiryRoomSchedule();
        jDesktopPane1.add(irs);
        irs.setSize(jDesktopPane1.getWidth(), jDesktopPane1.getHeight());
        irs.setLocation(jDesktopPane1.getLocation().x,jDesktopPane1.getLocation().y);
        irs.setVisible(true);
    }
    
    private void logout(){
        int reply = JOptionPane.showConfirmDialog(rootPane, "Are you sure you want to logout?", "Logout", JOptionPane.YES_NO_OPTION, 3);
        if(reply==JOptionPane.YES_OPTION){
            //---------FOR LOGS---------------------
            new dbConnection().recordLog("[Logout] User..");
            //--------------------------------------
            GetterSetterClass gsc = new GetterSetterClass();
            gsc.setUserID(-1);

            this.setVisible(false);
            LoginDialog ld = new LoginDialog(null,true);
            ld.setLocation((1366-ld.getWidth())/2, (720-ld.getHeight())/2);
            ld.setVisible(true);
        }
    }
    
    private void showClassScheduler(){
        instanceOfClassScheduler++;
        
        switch(instanceOfClassScheduler){
            case 1:
                ClassScheduler1 cs1 = new ClassScheduler1();
                if(cs1.isVisible()){
                    cs1.requestFocus();
                }else{
                    cs1.setTitle("Class Scheduler Instance 1");
                    cs1.setVisible(true);
                }
                break;
            case 2:
                ClassScheduler2 cs2 = new ClassScheduler2();
                if(cs2.isVisible()){
                    cs2.requestFocus();
                }else{
                    cs2.setTitle("Class Scheduler Instance 2");
                    cs2.setVisible(true);
                }
                break;
            case 3:
                ClassScheduler3 cs3 = new ClassScheduler3();
                if(cs3.isVisible()){
                    cs3.requestFocus();
                }else{
                    cs3.setTitle("Class Scheduler Instance 3");
                    cs3.setVisible(true);
                }
                break;
            case 4:
                ClassScheduler4 cs4 = new ClassScheduler4();
                if(cs4.isVisible()){
                    cs4.requestFocus();
                }else{
                    cs4.setTitle("Class Scheduler Instance 4");
                    cs4.setVisible(true);
                }
                break;
            case 5:
                ClassScheduler5 cs5 = new ClassScheduler5();
                if(cs5.isVisible()){
                    cs5.requestFocus();
                }else{
                    cs5.setTitle("Class Scheduler Instance 5");
                    cs5.setVisible(true);
                }
                break;
            default:
                instanceOfClassScheduler=0;
                break;
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainMDI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainMDI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainMDI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainMDI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainMDI().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem aboutMenuItem;
    private javax.swing.JButton btnChecklist;
    private javax.swing.JButton btnCollege;
    private javax.swing.JButton btnCourse;
    private javax.swing.JButton btnDepartment;
    private javax.swing.JButton btnFaculty;
    private javax.swing.JButton btnRoom;
    private javax.swing.JButton btnSchedule;
    private javax.swing.JButton btnSubject;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JMenuItem contentMenuItem;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JMenu helpMenu;
    private javax.swing.JMenu inquiryMenu;
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem12;
    private javax.swing.JMenuItem jMenuItem13;
    private javax.swing.JMenuItem jMenuItem14;
    private javax.swing.JMenuItem jMenuItem15;
    private javax.swing.JMenuItem jMenuItem16;
    private javax.swing.JMenuItem jMenuItem17;
    private javax.swing.JMenuItem jMenuItem18;
    private javax.swing.JMenuItem jMenuItem19;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem20;
    private javax.swing.JMenuItem jMenuItem21;
    private javax.swing.JMenuItem jMenuItem22;
    private javax.swing.JMenuItem jMenuItem23;
    private javax.swing.JMenuItem jMenuItem24;
    private javax.swing.JMenuItem jMenuItem25;
    private javax.swing.JMenuItem jMenuItem26;
    private javax.swing.JMenuItem jMenuItem27;
    private javax.swing.JMenuItem jMenuItem28;
    private javax.swing.JMenuItem jMenuItem29;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem30;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JPopupMenu.Separator jSeparator7;
    private javax.swing.JPopupMenu.Separator jSeparator8;
    private javax.swing.JLabel labelDateTime;
    private javax.swing.JLabel labelDateTime1;
    private javax.swing.JLabel labelDateTime2;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JMenu managementMenu;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JLabel picAvatar;
    private javax.swing.JPanel quicklunchPanel;
    private javax.swing.JCheckBoxMenuItem quicklunchPanelCheckbox;
    private javax.swing.JMenu reportsMenu;
    private javax.swing.JMenu settingsMenu;
    private javax.swing.JPanel statusPanel;
    private javax.swing.JCheckBoxMenuItem statusPanelCheckbox;
    protected javax.swing.JTextField textAY;
    private javax.swing.JTextField textFieldDate;
    private javax.swing.JTextField textFieldDay;
    private javax.swing.JTextField textFieldTime;
    protected javax.swing.JTextField textSem;
    private javax.swing.JLabel txtLogout;
    protected javax.swing.JTextField txtStatus;
    protected javax.swing.JTextField txtThreads;
    private javax.swing.JLabel txtWelcomeUser;
    private javax.swing.JMenuItem userlogMenuItem;
    private javax.swing.JMenu viewMenu;
    // End of variables declaration//GEN-END:variables
}
