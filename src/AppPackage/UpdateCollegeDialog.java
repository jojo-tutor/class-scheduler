/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package AppPackage;

import Modules.InputValidator;
import javax.swing.JOptionPane;
import javax.swing.JTable;

/**
 *
 * @author Jojo
 */
public class UpdateCollegeDialog extends javax.swing.JDialog {
    
    
    //unused constructor
    public UpdateCollegeDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }
    
    protected int id;
    protected JTable table;
    protected int rowIndex;
    private String officeID;
    private String officeCode;
    private String officeDesc;
    public UpdateCollegeDialog(java.awt.Frame parent, boolean modal,JTable table,int rowIndex,int id, String collegeCode, String collegeDesc, int facultyID) {
        super(parent, modal);
        initComponents();
        
        this.table = table;
        this.rowIndex = rowIndex;
        this.id = id;
        textFieldCollegeCode.setText(collegeCode);
        textFieldCollege.setText(collegeDesc);

        dbConnection dbcon = new dbConnection();
        officeID = dbcon.findOneQuery("id", "tbl_office", "office_code='" + collegeCode + "'");
        officeCode = collegeCode;
        officeDesc = collegeDesc;
        dbcon.fillListDualCombo(comboDean,"Select id, CONCAT(firstname,' ',middlename,' ',lastname) as name FROM tbl_faculty ORDER BY firstname");

        
        //select the item on combo box
        int currentIndex=-1;
        for(int x=0;x<comboDean.getItemCount();x++){
            comboDean.setSelectedIndex(x);
            ComboItem comboItem = (ComboItem) comboDean.getSelectedItem();
            if(comboItem.getID()==facultyID){
                currentIndex=x;
                break;
            }
        }
        comboDean.setSelectedIndex(currentIndex);
        
        //---------FOR LOGS---------------------
        new dbConnection().recordLog("[Open] Update College Dialog");
        //--------------------------------------
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        textFieldCollegeCode = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        comboDean = new javax.swing.JComboBox();
        btnCancel = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        textFieldCollege = new javax.swing.JTextArea();
        jLabel8 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setLocationByPlatform(true);
        setResizable(false);
        setType(java.awt.Window.Type.UTILITY);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Segoe UI Light", 0, 36)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 102, 255));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel7.setText(" Update College");
        jLabel7.setOpaque(true);

        textFieldCollegeCode.setFont(new java.awt.Font("Segoe UI Semilight", 0, 14)); // NOI18N
        textFieldCollegeCode.setText("CTAS");
        textFieldCollegeCode.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                textFieldCollegeCodeFocusLost(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Segoe UI Semilight", 0, 12)); // NOI18N
        jLabel2.setText("College Name:");

        jLabel1.setFont(new java.awt.Font("Segoe UI Semilight", 0, 12)); // NOI18N
        jLabel1.setText("College Code:");

        jLabel6.setFont(new java.awt.Font("Segoe UI Semilight", 0, 12)); // NOI18N
        jLabel6.setText("Dean:");

        comboDean.setFont(new java.awt.Font("Segoe UI Semilight", 0, 14)); // NOI18N
        comboDean.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Jojo E. Tutor" }));

        btnCancel.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        btnSave.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        btnSave.setText("Save Changes");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        textFieldCollege.setColumns(20);
        textFieldCollege.setFont(new java.awt.Font("Segoe UI Semilight", 0, 14)); // NOI18N
        textFieldCollege.setLineWrap(true);
        textFieldCollege.setRows(3);
        textFieldCollege.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                textFieldCollegeFocusLost(evt);
            }
        });
        jScrollPane1.setViewportView(textFieldCollege);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(comboDean, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel1)
                                .addComponent(jLabel6)
                                .addComponent(jLabel2)
                                .addComponent(textFieldCollegeCode, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(textFieldCollegeCode, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(comboDean, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancel, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
                    .addComponent(btnSave, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE))
                .addContainerGap())
        );

        jLabel8.setFont(new java.awt.Font("Segoe UI Light", 0, 5)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 102, 255));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel8.setText("_________________________________________________________________________________________________________________________________________________________________________");
        jLabel8.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel8.setOpaque(true);
        jLabel8.setVerticalTextPosition(javax.swing.SwingConstants.TOP);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel8)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jLabel8)
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(20, 20, 20))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        //---------FOR LOGS---------------------
        new dbConnection().recordLog("[Close] Update College Dialog");
        //--------------------------------------
        this.dispose();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        InputValidator iv = new InputValidator();
        boolean aFlag = iv.validateIsEmptyComponent(textFieldCollegeCode) &&
                        iv.validateIsEmptyComponent(textFieldCollege);
        if(aFlag){
            ComboItem comboItem = (ComboItem) comboDean.getSelectedItem();
            GetterSetterClass gsc = new GetterSetterClass();
            dbConnection dbcon = new dbConnection();
            if(officeCode.equalsIgnoreCase(textFieldCollegeCode.getText()) && officeDesc.equalsIgnoreCase(textFieldCollegeCode.getText())){
                //do nothing, no changes
            }else{//update
                dbcon.updateDataSilently("tbl_office", "office_code='" + textFieldCollegeCode.getText() + "',office_description='" + textFieldCollege.getText() + "'", "id='" + officeID + "'");
            }
            String desigID_dean = dbcon.findOneQuery("id", "tbl_designation", "designation='Dean'");
            boolean isUpdated = dbcon.updateDataDuplicateExceptionSilently("tbl_college", "college_code='" + textFieldCollegeCode.getText() + "',college='" + textFieldCollege.getText() + "',dean='" + comboItem.getID() + "'", "id='" + this.id + "'");
            if(isUpdated){
                boolean isInserted = dbcon.insertDataDuplicateExceptionSilently("tbl_faculty_designation","faculty_id,designation_id,office_id,semester,ay_start,ay_end", "'" + comboItem.getID() + "','" + desigID_dean + "','" + officeID + "','" + gsc.getSemester() + "','" + gsc.getAcademicYearStart() + "','" + gsc.getAcademicYearEnd() + "'");
                if(isInserted){
                    JOptionPane.showMessageDialog(null, "Data has been updated.");
                }else{//just update
                    dbcon.updateData("tbl_faculty_designation", "faculty_id='" + comboItem.getID() + "',office_id='" + officeID + "'", "designation_id='" + desigID_dean + "' and office_id='" + officeID + "' and semester='" + gsc.getSemester() + "' and ay_start='" + gsc.getAcademicYearStart() + "' and ay_end='" + gsc.getAcademicYearEnd() + "'");
                }  
                //dbcon.fillTable(table, "Select a.id, a.username, a.password, a.usertype, CONCAT(d.address,' ',b.firstname,' ',b.middlename,' ',b.lastname,' ',c.extension) as name, a.instructor_id  FROM tbl_account a, tbl_faculty b,tbl_name_extension c,tbl_name_address d WHERE (a.instructor_id=b.id) and (b.extension_id=c.id) and (b.address_id=d.id) ORDER BY a.username");
                dbcon.fillTable(table, "Select col.id, col.college_code, col.college,CONCAT(b.address,' ',a.firstname,' ',a.middlename,' ',a.lastname,' ',c.extension) AS name,col.dean FROM tbl_college col,tbl_faculty a, tbl_name_address b, tbl_name_extension c WHERE (a.address_id=b.id) and (a.extension_id=c.id) and a.id=col.dean ORDER BY col.college_code");
                //---------FOR LOGS---------------------
                new dbConnection().recordLog("[Save] Update College ID: "+id+" "+textFieldCollegeCode.getText()+"-"+textFieldCollege.getText()+" Dean: "+comboItem.getName());
                //--------------------------------------
                table.setRowSelectionInterval(rowIndex, rowIndex);
                this.dispose();
            }
        }
        
    }//GEN-LAST:event_btnSaveActionPerformed

    private void textFieldCollegeCodeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_textFieldCollegeCodeFocusLost
        new InputValidator().validateIsEmptyComponent(evt.getComponent());
    }//GEN-LAST:event_textFieldCollegeCodeFocusLost

    private void textFieldCollegeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_textFieldCollegeFocusLost
        new InputValidator().validateIsEmptyComponent(evt.getComponent());
    }//GEN-LAST:event_textFieldCollegeFocusLost

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        //---------FOR LOGS---------------------
        new dbConnection().recordLog("[Close] Update College Dialog");
        //--------------------------------------
    }//GEN-LAST:event_formWindowClosing

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(UpdateCollegeDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(UpdateCollegeDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(UpdateCollegeDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UpdateCollegeDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                UpdateCollegeDialog dialog = new UpdateCollegeDialog(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox comboDean;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea textFieldCollege;
    private javax.swing.JTextField textFieldCollegeCode;
    // End of variables declaration//GEN-END:variables
}
