/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package AppPackage;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Jojo
 */
public class TableRowMover {
    
    public void moveData(JTable table_origin, JTable table_destination){
        try
        { 
            int originRows = table_origin.getRowCount();//row 1 starts at index 0
            int originCols = table_origin.getColumnCount(); //col 1 starts at index 0
            
            for(int a=0;a<originRows;a++){
                if(table_origin.getValueAt(a,0).equals(true)){
                    //System.out.println(table_origin.getValueAt(a,1)+"|"+table_origin.getValueAt(a,2)+"|"+table_origin.getValueAt(a,3));
                    Object[] row = new Object[4];
                    row[0] = table_origin.getValueAt(a,0);
                    row[1] = table_origin.getValueAt(a,1);
                    row[2] = table_origin.getValueAt(a,2);
                    row[3] = table_origin.getValueAt(a,3);
                    ((DefaultTableModel) table_destination.getModel()).insertRow(table_destination.getRowCount(),row);
                    
                    ((DefaultTableModel) table_origin.getModel()).removeRow(a);
                    
                    a=originRows;
                    //originRows--;
                }
            }
        }
        catch(Exception ex){
            JOptionPane.showMessageDialog(null, ex);
            System.err.println(ex.toString());
        }
    }
    
    public void moveData_AllSelected(JTable table_origin, JTable table_destination){
        try
        { 
            int originRows = table_origin.getRowCount();//row 1 starts at index 0
            int originCols = table_origin.getColumnCount(); //col 1 starts at index 0
            
            for(int a=0;a<originRows;a++){
                if(table_origin.getValueAt(a,0).equals(true)){
                    //System.out.println(table_origin.getValueAt(a,1)+"|"+table_origin.getValueAt(a,2)+"|"+table_origin.getValueAt(a,3));
                    Object[] row = new Object[4];
                    row[0] = table_origin.getValueAt(a,0);
                    row[1] = table_origin.getValueAt(a,1);
                    row[2] = table_origin.getValueAt(a,2);
                    row[3] = table_origin.getValueAt(a,3);
                    ((DefaultTableModel) table_destination.getModel()).insertRow(table_destination.getRowCount(),row);
                    
                    ((DefaultTableModel) table_origin.getModel()).removeRow(a);
                    
                    a=0;
                    originRows--;
                }
            }
        }
        catch(Exception ex){
            JOptionPane.showMessageDialog(null, ex);
            System.err.println(ex.toString());
        }
    }
    
}
