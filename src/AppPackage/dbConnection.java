/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package AppPackage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Jojo
 */
public class dbConnection {
    private static String IP = "localhost:3306";
    private static String PORT = "3306";
    private static String SHORTIP = "localhost";
    private static String DB = "class_scheduler";
    private static String USERNAME = "root";
    private static String PASSWORD = "root";
    //private static boolean isAllSet = false;
    
    private static String CONN_STRING = "jdbc:mysql://" + IP + "/" + DB;
    
    private static Connection con = null;
    
    public void setIP(String IpAdd){
        IP = IpAdd;
    }
    
    public void setSHORTIP(String ShortIpAdd){
        SHORTIP = ShortIpAdd;
    }
    
    public void setPORT(String PortAdd){
        PORT = PortAdd;
    }
    
    public void setDB(String Database){
        DB = Database;
    }
    
    public void setUID(String UID){
        USERNAME = UID;
    }
    
    public void setPWD(String PWD){
        PASSWORD = PWD;
    }
    
    public String getIP(){
        return this.IP;
    }
    
    public String getSHORTIP(){
        return this.SHORTIP;
    }
    
    public String getPORT(){
        return this.PORT;
    }
    
    public String getDB(){
        return this.DB;
    }
    
    public String getUID(){
        return this.USERNAME;
    }
    
    public String getPWD(){
        return this.PASSWORD;
    }
    
    public String getConString(){
        return this.CONN_STRING;
    }
    
    //public void setAll(){
    //    isAllSet = true;
    //}
    
    public void setConString(){
        CONN_STRING = "jdbc:mysql://" + this.IP + "/" + this.DB;
    }
    
    public dbConnection(){
    }
    
    public boolean checkConnection(){       
        try{
        con = DriverManager.getConnection(CONN_STRING, USERNAME,PASSWORD);
            con.close();
            return true;
        }catch(SQLException ex){
            return false;
        }
    }
    
    public Connection getCon(){
        connect();
        return this.con;
    }
   
    public void connect(){

        try {
            this.con.close();
        } catch (SQLException ex) {
            Logger.getLogger(dbConnection.class.getName()).log(Level.SEVERE, null, ex);
        }

        try{
        con = DriverManager.getConnection(CONN_STRING, USERNAME,PASSWORD);
        }catch(SQLException ex){
            //JOptionPane.showMessageDialog(null,ex);
            System.err.println(ex.toString());
        }

    }
    
    public void close_con(){
        //
    }
    
    public void insert_data(){
        connect();
        Statement st;
        try {
            st = (Statement) con.createStatement();
            String insert = "INSERT INTO 'class_scheduler.tbl_account'('instructor_id','username','password') VALUES('11','user1','pass1')";
            st.executeUpdate(insert);
        } catch (SQLException ex) {
            if(ex.getErrorCode()==1062){
                JOptionPane.showMessageDialog(null, "The system has encountered duplicate values, please re-check your data.");
            }else{
                //JOptionPane.showMessageDialog(null, ex);
                System.err.println(ex.toString());
            }
        }
        close_con();
    }

    public void insertData(String table, String fields, String values){
        connect();
        try {
            String sqlCommand;
            Statement st;
            st = (Statement) con.createStatement();
            sqlCommand = "INSERT INTO " + table + "(" + fields + ") VALUES(" + values + ")";
            st.executeUpdate(sqlCommand);
            st.close();
            //con.close();
            JOptionPane.showMessageDialog(null, "Data has been saved.");
        } catch (SQLException ex) {
            if(ex.getErrorCode()==1062){
                JOptionPane.showMessageDialog(null, "The system has encountered duplicate values, please re-check your data.");
            }else{
                //JOptionPane.showMessageDialog(null, ex);
                System.err.println(ex.toString());
            }
        }
        close_con();
    }
    
    public boolean insertDataDuplicateException(String table, String fields, String values){
        connect();
        try {
            String sqlCommand;
            Statement st;
            st = (Statement) con.createStatement();
            sqlCommand = "INSERT INTO " + table + "(" + fields + ") VALUES(" + values + ")";
            st.executeUpdate(sqlCommand);
            st.close();
            JOptionPane.showMessageDialog(null, "Data has been saved.");
            return true;
        } catch (SQLException ex) {
            if(ex.getErrorCode()==1062){
                //JOptionPane.showMessageDialog(null, "The system has encountered duplicate values, please re-check your data.");
            }else{
                //JOptionPane.showMessageDialog(null, ex);
                //System.err.println(ex.toString());
            }
            return false;
        }
    }
    
    public boolean insertDataDuplicateExceptionSilently(String table, String fields, String values){
        connect();
        try {
            String sqlCommand;
            Statement st;
            st = (Statement) con.createStatement();
            sqlCommand = "INSERT INTO " + table + "(" + fields + ") VALUES(" + values + ")";
            st.executeUpdate(sqlCommand);
            st.close();
            //JOptionPane.showMessageDialog(null, "Data has been saved.");
            return true;
        } catch (SQLException ex) {
            if(ex.getErrorCode()==1062){
                //JOptionPane.showMessageDialog(null, "The system has encountered duplicate values, please re-check your data.");
            }else{
                //JOptionPane.showMessageDialog(null, ex);
                //System.err.println(ex.toString());
            }
            System.out.println(ex);
            return false;
        }
    }
    
    public void insertDataSilently(String table, String fields, String values){
        connect();
        try {
            String sqlCommand;
            Statement st;
            st = (Statement) con.createStatement();
            sqlCommand = "INSERT INTO " + table + "(" + fields + ") VALUES(" + values + ")";
            st.executeUpdate(sqlCommand);
            st.close();
            //con.close();
        } catch (SQLException ex) {
            if(ex.getErrorCode()==1062){
                JOptionPane.showMessageDialog(null, "The system has encountered duplicate values, please re-check your data.");
            }else{
                //JOptionPane.showMessageDialog(null, ex);
                System.err.println(ex.toString());
            }
        }
        close_con();
    }
       
    public void insertDataForAutomateClassSection(String table, String fields, String values){
        connect();
        try {
            String sqlCommand;
            Statement st;
            st = (Statement) con.createStatement();
            sqlCommand = "INSERT INTO " + table + "(" + fields + ") VALUES(" + values + ")";
            st.executeUpdate(sqlCommand);
            st.close();
            //con.close();
        } catch (SQLException ex) {
            if(ex.getErrorCode()==1062){
                //JOptionPane.showMessageDialog(null, "The system has encountered duplicate values, please re-check your data.");
            }else{
                //JOptionPane.showMessageDialog(null, ex);
                //System.err.println(ex.toString());
            }
        }
        close_con();
    }
    
    public void insertDualData(String table1,String fields1, String values1, String valuesLEC, String valuesLAB){
        connect();
        try {
            //insert to first table
            String sqlCommand;
            Statement st;
            st = (Statement) con.createStatement();
            sqlCommand = "INSERT INTO " + table1 + "(" + fields1 + ") VALUES(" + values1 + ")";
            st.executeUpdate(sqlCommand);
            st.close();
            //con.close();
            
            //get the primary key of the latest saved in the first table
            String latest_PKey = findLastPKey(table1);
            //String valuesLec = "'" + latest_PKey + "','LEC','" + valuesLEC + "'";
            String valuesLec = ("'" + latest_PKey + "',") + ("'LEC',") + valuesLEC;
            String valuesLab = ("'" + latest_PKey + "',") + ("'LAB',") + valuesLAB;
            //String valuesLab = "'" + latest_PKey + "','LAB','" + valuesLAB + "'";
            
            //insert to the second table
            String sqlCommand2;
            Statement st2;
            st2 = (Statement) con.createStatement();
            sqlCommand2 = "INSERT INTO tbl_subject_detail(subject_id,subject_type,numofhours) VALUES(" + valuesLec + ")";
            st2.executeUpdate(sqlCommand2);
            sqlCommand2 = "INSERT INTO tbl_subject_detail(subject_id,subject_type,numofhours) VALUES(" + valuesLab + ")";
            st2.executeUpdate(sqlCommand2);
            st2.close();
            //con.close();
            JOptionPane.showMessageDialog(null, "Data has been saved.");
        } catch (SQLException ex) {
            if(ex.getErrorCode()==1062){
                JOptionPane.showMessageDialog(null, "The system has encountered duplicate values, please re-check your data.");
            }else{
                //JOptionPane.showMessageDialog(null, ex);
                System.err.println(ex.toString());
            }
        }
        close_con();
    }
    
    public void updateData(String table, String SETclause, String WHEREclause){
        connect();
        try {
            String sqlCommand;
            Statement st = con.createStatement();
            sqlCommand = "UPDATE " + table + " SET " + SETclause + " WHERE " + WHEREclause + "";
            //sqlCommand = "update tbl_account set instructor_id='2',username='Maam Vilma',password='pass',usertype='Chairperson' where id='3'";
            st.execute(sqlCommand);
            st.close();
            //con.close();
            JOptionPane.showMessageDialog(null, "Data has been updated.");
        } catch (SQLException ex) {
            if(ex.getErrorCode()==1062){
                JOptionPane.showMessageDialog(null, "The system has encountered duplicate values, please re-check your data.");
            }else{
                //JOptionPane.showMessageDialog(null, ex);
                System.err.println(ex.toString());
            }
        }
        close_con();
    }
    
    public void updateDataSilently(String table, String SETclause, String WHEREclause){
        connect();
        try {
            String sqlCommand;
            Statement st = con.createStatement();
            sqlCommand = "UPDATE " + table + " SET " + SETclause + " WHERE " + WHEREclause + "";
            //sqlCommand = "update tbl_account set instructor_id='2',username='Maam Vilma',password='pass',usertype='Chairperson' where id='3'";
            st.execute(sqlCommand);
            st.close();
            //con.close();
        } catch (SQLException ex) {
            if(ex.getErrorCode()==1062){
                JOptionPane.showMessageDialog(null, "The system has encountered duplicate values, please re-check your data.");
            }else{
                //JOptionPane.showMessageDialog(null, ex);
                System.err.println(ex.toString());
            }
        }
        close_con();
    }
    
    public boolean updateDataDuplicateException(String table, String SETclause, String WHEREclause){
        connect();
        try {
            String sqlCommand;
            Statement st = con.createStatement();
            sqlCommand = "UPDATE " + table + " SET " + SETclause + " WHERE " + WHEREclause + "";
            //sqlCommand = "update tbl_account set instructor_id='2',username='Maam Vilma',password='pass',usertype='Chairperson' where id='3'";
            st.execute(sqlCommand);
            st.close();
            JOptionPane.showMessageDialog(null, "Data has been updated.");
            return true;
        } catch (SQLException ex) {
            if(ex.getErrorCode()==1062){
                //JOptionPane.showMessageDialog(null, "The system has encountered duplicate values, please re-check your data.");
            }else{
                //JOptionPane.showMessageDialog(null, ex);
                System.err.println(ex.toString());
            }
            return false;
        }
    }
    
    public boolean updateDataDuplicateExceptionSilently(String table, String SETclause, String WHEREclause){
        connect();
        try {
            String sqlCommand;
            Statement st = con.createStatement();
            sqlCommand = "UPDATE " + table + " SET " + SETclause + " WHERE " + WHEREclause + "";
            //sqlCommand = "update tbl_account set instructor_id='2',username='Maam Vilma',password='pass',usertype='Chairperson' where id='3'";
            st.execute(sqlCommand);
            st.close();
            return true;
        } catch (SQLException ex) {
            if(ex.getErrorCode()==1062){
                //JOptionPane.showMessageDialog(null, "The system has encountered duplicate values, please re-check your data.");
            }else{
                //JOptionPane.showMessageDialog(null, ex);
                //System.err.println(ex.toString());
            }
            return false;
        }
    }
    
    public void updateDualData(String table, String SETclause, String WHEREclause){
        connect();
        try {
            String sqlCommand;
            Statement st = con.createStatement();
            sqlCommand = "UPDATE " + table + " SET " + SETclause + " WHERE " + WHEREclause + "";
            //sqlCommand = "update tbl_account set instructor_id='2',username='Maam Vilma',password='pass',usertype='Chairperson' where id='3'";
            st.execute(sqlCommand);
            st.close();
            //con.close();
            //JOptionPane.showMessageDialog(null, "Data has been updated.");
        } catch (SQLException ex) {
            if(ex.getErrorCode()==1062){
                JOptionPane.showMessageDialog(null, "The system has encountered duplicate values, please re-check your data.");
            }else{
                //JOptionPane.showMessageDialog(null, ex);
                System.err.println(ex.toString());
            }
        }
        close_con();
    }
    
    public void removeData(String table, String WHEREclause){
        connect();
        try {
            String sqlCommand;
            Statement st = con.createStatement();
            sqlCommand = "DELETE FROM " + table + " WHERE " + WHEREclause + "";
            st.execute(sqlCommand);
            st.close();
            //con.close();
            JOptionPane.showMessageDialog(null, "Data has been removed.");
        } catch (SQLException ex) {
            if(ex.getErrorCode()==1451){//catch if parent row is deleted, because it is primary key
                JOptionPane.showMessageDialog(null, "Notice: You cannot remove this field, because it is used by other constraints.");
            }else{
                //JOptionPane.showMessageDialog(null, ex);
                System.err.println(ex.toString());
            }
        }
        close_con();
    }
     
    public void removeDataSilently(String table, String WHEREclause){
        connect();
        try {
            String sqlCommand;
            Statement st = con.createStatement();
            sqlCommand = "DELETE FROM " + table + " WHERE " + WHEREclause + "";
            st.execute(sqlCommand);
            st.close();
            //con.close();
        } catch (SQLException ex) {
            if(ex.getErrorCode()==1451){ //catch if parent row is deleted, because it is primary key
                JOptionPane.showMessageDialog(null, "Notice: You cannot remove this field, because it is used by other constraints.");
            }else{
                //JOptionPane.showMessageDialog(null, ex);
                System.err.println(ex.toString());
            } 
        }
        close_con();
    }
    
    public int check_login(String username,String password){
        connect();
        int value=0;
        String uname="", pwd="";
        try {
            Statement st = (Statement) con.createStatement();
            String query = "SELECT username,password FROM class_scheduler.tbl_account WHERE username='" + username + "' and password = '" + password + "' ";
            ResultSet rs = st.executeQuery(query);
            
            while(rs.next()){
                uname = rs.getString("username");
                pwd = rs.getString("password");
            }
            if(pwd.equals(password)){
                value=1;
                System.out.println(pwd);
            }else{
                value=0;System.out.println(pwd);
            }
            
        } catch (SQLException ex) {
            //JOptionPane.showMessageDialog(null, ex);
            System.err.println(ex.toString());
        }
        close_con();
        return value; 
    }
    public int check_one_value(String condition){
        connect();
        int value=0;
        try {
            Statement st = (Statement) con.createStatement();
            String query = "SELECT username,password FROM class_scheduler.tbl_account WHERE " + condition;
            ResultSet rs = st.executeQuery(query);
            while(rs.next()){
                value=1;
            }
            
        } catch (SQLException ex) {
            //JOptionPane.showMessageDialog(null, ex);
            System.err.println(ex.toString());
        }
        close_con();
        return value;
    }
    
    public String findOneQuery(String field, String table, String WHEREClause){
        connect();
        String value="-1";
        try {
            Statement st = (Statement) con.createStatement();
            String query = "SELECT " + field + " FROM " + table + " WHERE " + WHEREClause + "";
            ResultSet rs = st.executeQuery(query);
            while(rs.next()){
                value=rs.getString(1);
            }
            
        } catch (SQLException ex) {
            //JOptionPane.showMessageDialog(null,ex);
            System.err.println(ex.toString());
        }
        close_con();
        return value;
    }
    
    
    public int[] findArrayQuery(String field, String table, String WHEREClause){
        
        connect();
        int count=-1;
        try {
            Statement st = (Statement) con.createStatement();
            String query = "SELECT COUNT(" + field + ") FROM " + table + " WHERE " + WHEREClause + "";
            ResultSet rs = st.executeQuery(query);
            while(rs.next()){
                count=Integer.parseInt(rs.getString(1));
            }
            //con.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            System.err.println(ex.toString());
        }
        close_con();
        
        int []IDs = new int[count];
        
            connect();
            int index=-1;
            try {
                Statement st = (Statement) con.createStatement();
                String query = "SELECT " + field + " FROM " + table + " WHERE " + WHEREClause + "";
                ResultSet rs = st.executeQuery(query);
                while(rs.next()){
                    index+=1;
                    IDs[index]=Integer.parseInt(rs.getString(1));
                }
                //con.close();
            } catch (SQLException ex) {
                //JOptionPane.showMessageDialog(null, ex);
                System.err.println(ex.toString());
            }
            close_con();
        return IDs;
    }
    
    public String justQuery(String query){
        connect();
        String value="-1";
        try {
            Statement st = (Statement) con.createStatement();
            ResultSet rs = st.executeQuery(query);
            while(rs.next()){
                value=rs.getString(1);
            }
            
        } catch (SQLException ex) {
            //JOptionPane.showMessageDialog(null, ex);
            System.err.println(ex.toString());
        }
        close_con();
        return value;
    }
    
    public String threadsConnected(){
        connect();
        String query = "show status where `variable_name` = 'Threads_connected'";
        String value="-1";
        try {
            Statement st = (Statement) con.createStatement();
            ResultSet rs = st.executeQuery(query);
            while(rs.next()){
                value=rs.getString(2);
            }
            
        } catch (SQLException | NullPointerException ex) {
            //JOptionPane.showMessageDialog(null, ex);
            value="-1";
        }
        close_con();
        return value;
    }
    
    public String findLastPKey(String table){
        connect();
        String value="-1";
        try {
            Statement st = (Statement) con.createStatement();
            String query = "SELECT id FROM " + table + " ORDER BY id";
            ResultSet rs = st.executeQuery(query);
            while(rs.next()){
                value=rs.getString(1);
            }
            
        } catch (SQLException ex) {
            //JOptionPane.showMessageDialog(null, ex);
            System.err.println(ex.toString());
        }
        return value;
    }
    
    
    public void fillTable(JTable table, String Query){
        connect();
        try
        {
            con = DriverManager.getConnection(CONN_STRING, USERNAME,PASSWORD);

            //CreateConnection();
            com.mysql.jdbc.Statement stat;
            stat = (com.mysql.jdbc.Statement) con.createStatement();
            ResultSet rs = stat.executeQuery(Query);

            //To remove previously added rows
            while(table.getRowCount() > 0) 
            {
                ((DefaultTableModel) table.getModel()).removeRow(0);
            }
            int columns = rs.getMetaData().getColumnCount();
            while(rs.next())
            {  
                Object[] row = new Object[columns];
                for (int i = 1; i <= columns; i++)
                {  
                    row[i - 1] = rs.getObject(i);
                }
                ((DefaultTableModel) table.getModel()).insertRow(rs.getRow()-1,row);
            }

            rs.close();
            stat.close();
            //con.close();
        }
        catch(SQLException ex){
            //JOptionPane.showMessageDialog(null, ex);
            System.err.println(ex.toString());
            //System.out.println(CONN_STRING);
        }
        close_con();
    }
    
    public void fillSpecialTable(JTable table, String Query){
        connect();
        try
        {
            con = DriverManager.getConnection(CONN_STRING, USERNAME,PASSWORD);

            //CreateConnection();
            com.mysql.jdbc.Statement stat;
            stat = (com.mysql.jdbc.Statement) con.createStatement();
            ResultSet rs = stat.executeQuery(Query);

            //To remove previously added rows
            while(table.getRowCount() > 0) 
            {
                ((DefaultTableModel) table.getModel()).removeRow(0);
            }
            int columns = rs.getMetaData().getColumnCount();
            while(rs.next())
            {  
                Object[] row = new Object[columns];
                for (int i = 1; i <= columns; i++)
                {  
                    row[i-1] = rs.getObject(i);

                }
                row[0] = false;
  
                ((DefaultTableModel) table.getModel()).insertRow(rs.getRow()-1,row);
            }

            rs.close();
            stat.close();
            //con.close();
        }
        catch(SQLException ex){
            //JOptionPane.showMessageDialog(null, ex);
            System.err.println(ex.toString());
        }
        close_con();
    }
    
    public void fillListSingleCombo(JComboBox combo, String Query){
        connect();
        combo.removeAllItems();
        try {
            Statement st = (Statement) con.createStatement();
            //String query = "SELECT * FROM class_scheduler.tbl_account";
            ResultSet rs = st.executeQuery(Query);
            while(rs.next()){
                combo.addItem(rs.getString(1));
            }
            
        } catch (SQLException ex) {
            //JOptionPane.showMessageDialog(null, ex);
            System.err.println(ex.toString());
        }
        close_con();
    }
    public void fillListDualCombo(JComboBox combo, String Query){
        connect();
        combo.removeAllItems();
        try {
            Statement st = (Statement) con.createStatement();
            //String query = "SELECT * FROM class_scheduler.tbl_account";
            ResultSet rs = st.executeQuery(Query);
            while(rs.next()){
                //combo.addItem(rs.getString(1));
                //ComboItem comboItem = new ComboItem(Integer.parseInt(rs.getString(1)), rs.getString(2));
                combo.addItem(new ComboItem(Integer.parseInt(rs.getString(1)), rs.getString(2)));
            }
            
        } catch (SQLException ex) {
            //JOptionPane.showMessageDialog(null, ex);
            System.err.println(ex.toString());
        }
        close_con();
    }
    
    public void automate_tbl_classlist_section(String field, String table, String WHEREClause){
        GetterSetterClass gsc = new GetterSetterClass();
        String ids = findOneQuery("id", "tbl_classlist", "semester='" + gsc.getSemester() + "'");
        int intIDs = Integer.parseInt(ids);
        if(intIDs>0){
            String arrayIDs[] = new String[intIDs];
            connect();//select id from tbl_classlist where semester = gsc.getSemester()
            try {
                Statement st = (Statement) con.createStatement();
                String query = "SELECT " + field + " FROM " + table + " WHERE " + WHEREClause + "";
                ResultSet rs = st.executeQuery(query);
                int index=0;
                while(rs.next()){
                    arrayIDs[index] = rs.getString(1) ;
                    index++;
                    //insertDataForAutomateClassSection("tbl_classlist_section","classlist_id,section,semester,ay_start,ay_end","'" + rs.getString(1) + "','A','" + gsc.getSemester() + "','" + gsc.getAcademicYearStart()+ "','" + gsc.getAcademicYearEnd()+ "'");
                }
            } catch (SQLException ex) {
                //JOptionPane.showMessageDialog(null,ex);
                System.err.println(ex.toString());
            }
            
            for(int a=0;a<intIDs;a++){
                insertDataForAutomateClassSection("tbl_classlist_section","classlist_id,section,semester,ay_start,ay_end","'" + arrayIDs[a] + "','A','" + gsc.getSemester() + "','" + gsc.getAcademicYearStart()+ "','" + gsc.getAcademicYearEnd()+ "'");
            } 
        }  
    }
    
    public void automate_tbl_designation(){
        GetterSetterClass gsc = new GetterSetterClass();
        int prevSem=1;
        int prevAyStart=2015;
        int prevAyEnd=2016;
        switch(gsc.getSemester()){
            case 1:
                prevSem = 3;
                prevAyStart = gsc.getAcademicYearStart()-1;
                prevAyEnd = gsc.getAcademicYearEnd()-1;
                break;
            case 2:
                prevSem = 1;
                prevAyStart = gsc.getAcademicYearStart();
                prevAyEnd = gsc.getAcademicYearEnd();
                break;
            case 3:
                prevSem = 2;
                prevAyStart = gsc.getAcademicYearStart();
                prevAyEnd = gsc.getAcademicYearEnd();
                break;
            default:
                break;
        }
        //System.out.println(prevSem);
        //System.out.println(prevAyStart);
        //System.out.println(prevAyEnd);
        String ids = findOneQuery("COUNT(id)", "tbl_faculty_designation", "semester='" + prevSem + "' and ay_start='" + prevAyStart + "' and ay_end='" + prevAyEnd + "'");
        int intIDs = Integer.parseInt(ids);
        //System.out.println(intIDs);
        if(intIDs>0){
            String arrayIDs[][] = new String[intIDs][3];
            connect();//select id from tbl_faculty_designation where semester and ay of the previous
            try {
                Statement st = (Statement) con.createStatement();
                String query = "SELECT faculty_id,designation_id,office_id FROM tbl_faculty_designation WHERE semester='" + prevSem + "' and ay_start='" + prevAyStart + "' and ay_end='" + prevAyEnd + "'";
                ResultSet rs = st.executeQuery(query);
                int rowIndex=0;
                while(rs.next()){
                    arrayIDs[rowIndex][0] = rs.getString(1);
                    arrayIDs[rowIndex][1] = rs.getString(2);
                    arrayIDs[rowIndex][2] = rs.getString(3);
                    rowIndex++;
                }
                //copy the prev designation and save it to new sem,ay
                for(int r=0;r<intIDs;r++){
                    insertDataDuplicateExceptionSilently("tbl_faculty_designation", "faculty_id,designation_id,office_id,semester,ay_start,ay_end", "'" + arrayIDs[r][0] + "','" + arrayIDs[r][1] + "','" + arrayIDs[r][2] + "','" + gsc.getSemester() + "','" + gsc.getAcademicYearStart() + "','" + gsc.getAcademicYearEnd() + "'");
                }
            } catch (SQLException ex) {
                //JOptionPane.showMessageDialog(null,ex);
                System.err.println(ex.toString());
            }
        }  
    }
    
    public void saveAvatar(String path, int facultyID){
        FileInputStream fis = null;
        File avatar = null;
        
        try {
            avatar = new File(path);
            fis = new FileInputStream(avatar);
            if(addAvatar(avatar,fis,facultyID)){
                System.out.println("Avatar saved.");
            }else{
                System.out.println("Error.");
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(dbConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean addAvatar(File avatar, FileInputStream fis, int facultyID){
        connect();
        PreparedStatement ps;
        String sql = "INSERT INTO tbl_avatar(faculty_id,avatar) VALUES(?,?)";
        
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, facultyID);
            ps.setBinaryStream(2, fis, (int)avatar.length());
            ps.executeUpdate();
            return true;
            
        } catch (SQLException ex) {
            Logger.getLogger(dbConnection.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public String fetchAvatar(int paramFacID){
        String facultyName = findOneQuery("CONCAT(fac.firstname,'-',fac.lastname)", "tbl_faculty fac", "id='" + paramFacID + "'");
        connect();
        PreparedStatement ps;
        FileOutputStream fos = null;
        String sql = "SELECT * FROM tbl_avatar WHERE faculty_id='" + paramFacID + "'";

        try {
            ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            rs.last();
            Blob b = rs.getBlob("avatar");
            String facultyID = rs.getString("faculty_id");
            //fos = new FileOutputStream(facultyID+".jpg");
            fos = new FileOutputStream(new FileReaderWriter().getMyAppDIR()+facultyName+".jpg");
            int len = (int)b.length();
            byte[] buf = b.getBytes(1, len);
            fos.write(buf,0,len);
            return facultyID;
            
        } catch (SQLException ex) {
            //Logger.getLogger(dbConnection.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("No existing avatar in database yet.");
        } catch (FileNotFoundException ex) {
            //Logger.getLogger(dbConnection.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("No existing avatar in database yet.");
        } catch (IOException ex) {
            //Logger.getLogger(dbConnection.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("No existing avatar in database yet.");
        }
        
        return "-1";
    }
    
    public void recordLog(String action){
        GetterSetterClass gsc = new GetterSetterClass();
        connect();
        try {
            String sqlCommand;
            Statement st;
            st = (Statement) con.createStatement();
            String table = "tbl_logs";
            String fields = "faculty_id,login_as,action";
            String valuesAdded = "'" + gsc.getUserID() + "','" + gsc.getUsertype() + "','" + action + "'";
            sqlCommand = "INSERT INTO " + table + "(" + fields + ") VALUES(" + valuesAdded + ")";
            st.executeUpdate(sqlCommand);
            st.close();
            //con.close();
        } catch (SQLException ex) {
            if(ex.getErrorCode()==1062){
                JOptionPane.showMessageDialog(null, "The system has encountered duplicate values, please re-check your data.");
            }else{
                //JOptionPane.showMessageDialog(null, ex);
                System.err.println(ex.toString());
            }
        }
        close_con();
    }
    
    public void fetchLogAndWriteToFile(String field, String table, String WHEREClause, String ORDERClause){
        connect();
        try {
            Statement st = (Statement) con.createStatement();
            String query = "SELECT " + field + " FROM " + table + " WHERE " + WHEREClause + " ORDER BY " + ORDERClause + "";
            ResultSet rs = st.executeQuery(query);
            
            String filePath = new FileReaderWriter().getMyAppDIR()+"scheduler.log";
            File logFile = new File(filePath);
            logFile.createNewFile();
            FileWriter writer;
            writer = new FileWriter(filePath);
            
            int ctr=0;
            while(rs.next()){//1=timestamp, 2=faculty name, 3=login as , 4=action
                ctr++;
                String text = "-------------------------"+ctr+"-------------------------\r\nTimestamp: "+rs.getString(1)+"\r\n"+"User: "+rs.getString(2)+ "\r\nLogin as: " + rs.getString(3)+"\r\n"+"Event: "+rs.getString(4)+"\r\n";
                writer.write(text);
            }
            
            writer.close();
            
        } catch (SQLException | IOException ex) {
            //JOptionPane.showMessageDialog(null,ex);
            System.err.println(ex.toString());
        }
        close_con();
    }
    

}
