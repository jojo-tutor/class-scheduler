/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package AppPackage;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Jojo
 */
public class GetterSetterClass {
    private static int AcademicYearStart;
    private static int AcademicYearEnd;
    private static int semester;
    private static int userID;
    private static String userType;
    private static int UID;

    //userid=faculty_id also
    public void setUserID(int uid) {
        userID = uid;
    }
    public int getUserID() {
        return userID;
    }
    
    //actual id for tbl_account
    public void setUID(int uID) {
        UID = uID;
    }
    public int getUID() {
        return UID;
    }
    
    public void setUsertype(String utype) {
        userType = utype;
    }
    public String getUsertype() {
        return userType;
    }
    
    //academic year
    public void setAcademicYearStart(int AYStart){
        AcademicYearStart = AYStart;
    }
    public int getAcademicYearStart(){
        return AcademicYearStart;
    }
    public void setAcademicYearEnd(int AYEnd){
        AcademicYearEnd = AYEnd;
    }
    public int getAcademicYearEnd(){
        return AcademicYearEnd;
    }
    public void setSemester(int sem){
        semester = sem;
    }
    public int getSemester(){
        return semester;
    }
    
    //date & time
    public int getYearNow(){
        Date dateNow = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("Y");
        int yearNow = Integer.parseInt(sdf.format(dateNow));
        
        return yearNow;
    }
    
    public String getDateTime(){
        Date dateNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("E_yyyy_MM_dd_hh_mm_ss_a_zzz"); //Wed_2016_05_25_06_48_30_PM_CST
        return ft.format(dateNow);
    }
    
    public String getDayNow(){
        Date dateNow = new Date();
        String dayNow = String.format("%tA", dateNow); //Sunday
        
        return dayNow;
    }
    
    public String getDateNow(){
        Date dateNow = new Date();
        String dNow = String.format("%tB %<td, %<tY", dateNow); //December 25, 2016
        
        return dNow;
    }
    public String getTimeNow(){
        Date dateNow = new Date();
        String timeNow = String.format("%tr",dateNow); //07:06:12 AM
        
        return timeNow;
    }
    
    //date & time
    public String[] getDateFormatTimestamp(int yy, int mm, int dd){
        String dayStartEnd[] = new String[2];
        Date dateNow = new Date();
        Date derivedDate = new Date(dateNow.getYear()-yy,dateNow.getMonth()-mm,dateNow.getDate()-dd);//yy,mm,dd
        dayStartEnd[0] = new SimpleDateFormat("yyyy-MM-dd").format(derivedDate)+" 00:00:00";   //from->end time
        dayStartEnd[1] = new SimpleDateFormat("yyyy-MM-dd").format(dateNow)+" 23:59:59";       //to->timestamp today last time
        
        return dayStartEnd;
    } 
}
