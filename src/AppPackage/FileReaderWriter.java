/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package AppPackage;

/**
 *
 * @author Jojo
 */
import java.io.*;

public class FileReaderWriter {
    private String filePath;//="DBSettings.jojo";
    private File DBSettingsFile;// = new File(filePath);
    
    public FileReaderWriter(){
        //filePath = getMyDocumentsDIR()+"\\eScheduler";
        //DBSettingsFile = new File(filePath);
    }

    public String getMyAppDIR() {
        File dir = new File(getMyDocumentsDIR()+"\\eScheduler");
        if(dir.exists() && dir.isDirectory()){
            //the dir exists, do nothing
        }else{
            dir.mkdir();
        }
        return getMyDocumentsDIR()+"\\eScheduler\\";
    }
    
    private String getMyDocumentsDIR(){
        String myDocuments="";

        try {
            Process p =  Runtime.getRuntime().exec("reg query \"HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders\" /v personal");
            p.waitFor();

            InputStream in = p.getInputStream();
            byte[] b = new byte[in.available()];
            in.read(b);
            in.close();

            myDocuments = new String(b);
            myDocuments = myDocuments.split("\\s\\s+")[4];

        } catch(IOException | InterruptedException e) {
            System.err.println(e);
        }
        
        return myDocuments;
    }
    
    public boolean checkFileIfExists(){
        boolean basis;
        filePath = getMyAppDIR()+"DBSettings.jojo";
        DBSettingsFile = new File(filePath);
        
        if(DBSettingsFile.exists() && !DBSettingsFile.isDirectory()) { 
            basis=true;
        }else{
            try {
                //the file does not exists, create first
                DBSettingsFile.createNewFile();
            } catch (IOException ex) {
                System.err.println(ex);
            }
            basis=false;
        }
        return basis;
    }
    
    public void writeFile(String text){
        FileWriter writer;
        filePath = getMyAppDIR()+"DBSettings.jojo";
        DBSettingsFile = new File(filePath);
        //write texts in the file
        try {
            writer = new FileWriter(filePath);
            writer.write(text);
            writer.close();
        } catch (IOException e) {
            System.err.println("File not found.");
        }
    }
    
    public String[] readFile(){
        FileReader reader;
        filePath = getMyAppDIR()+"DBSettings.jojo";
        DBSettingsFile = new File(filePath);

        int numberOfLines = 6;
        String text[] = new String[numberOfLines];
        
        try {
            reader = new FileReader(filePath);
            BufferedReader bufferedReader = new BufferedReader(reader);
 
            String line;
            int index=0;
 
            while ((line = bufferedReader.readLine()) != null) {
                if(index>=numberOfLines){
                    break;
                }else{
                    text[index] = line;
                    index++;
                }
            }
            reader.close();
 
        } catch (IOException e) {
            System.err.println("File not found.");
        }
       return text;
    }
}
