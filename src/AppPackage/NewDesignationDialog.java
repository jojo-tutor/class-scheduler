/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package AppPackage;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author Jojo
 */
public class NewDesignationDialog extends javax.swing.JDialog {

    /**
     * Creates new form NewSubjectDialog
     */
    protected static JTable table;
    public NewDesignationDialog(java.awt.Frame parent, boolean modal, JTable table) {
        super(parent, modal);
        initComponents();
        
        this.table = table;
        
        fillCombo();
        
        resetFields();
        
        //---------FOR LOGS---------------------
        new dbConnection().recordLog("[Open] New Designation Dialog");
        //--------------------------------------
        
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        cmbName = new javax.swing.JComboBox();
        cmbDesig = new javax.swing.JComboBox();
        cmbOffice = new javax.swing.JComboBox();
        txtOffice = new javax.swing.JLabel();
        btnOfficePlus = new javax.swing.JButton();
        btnDesigPlus = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setLocationByPlatform(true);
        setResizable(false);
        setType(java.awt.Window.Type.UTILITY);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Segoe UI Light", 0, 36)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 102, 255));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel7.setText("New Designation");
        jLabel7.setOpaque(true);

        jLabel3.setFont(new java.awt.Font("Segoe UI Semilight", 0, 12)); // NOI18N
        jLabel3.setText("Name:");

        jLabel6.setFont(new java.awt.Font("Segoe UI Semilight", 0, 12)); // NOI18N
        jLabel6.setText("Position:");

        cmbName.setFont(new java.awt.Font("Segoe UI Semilight", 0, 14)); // NOI18N
        cmbName.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Prof. Jojo E. Tutor" }));

        cmbDesig.setFont(new java.awt.Font("Segoe UI Semilight", 0, 14)); // NOI18N
        cmbDesig.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Head" }));

        cmbOffice.setFont(new java.awt.Font("Segoe UI Semilight", 0, 14)); // NOI18N
        cmbOffice.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Research Development and Extension Office (RDE)" }));

        txtOffice.setFont(new java.awt.Font("Segoe UI Semilight", 0, 12)); // NOI18N
        txtOffice.setText("Office:");

        btnOfficePlus.setText("...");
        btnOfficePlus.setToolTipText("Office not listed? Click me.");
        btnOfficePlus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOfficePlusActionPerformed(evt);
            }
        });

        btnDesigPlus.setText("...");
        btnDesigPlus.setToolTipText("Position not listed? Click me.");
        btnDesigPlus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDesigPlusActionPerformed(evt);
            }
        });

        btnCancel.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        btnSave.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel3)
                        .addComponent(jLabel6)
                        .addComponent(txtOffice)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(194, 194, 194)
                            .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(cmbDesig, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(btnDesigPlus, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(cmbOffice, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(btnOfficePlus, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(cmbName, javax.swing.GroupLayout.PREFERRED_SIZE, 374, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmbName, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(cmbDesig, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDesigPlus, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(txtOffice)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(cmbOffice, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnOfficePlus, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jLabel8.setFont(new java.awt.Font("Segoe UI Light", 0, 5)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 102, 255));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel8.setText("___________________________________________________________________________________________________________________________________________________________________________________________");
        jLabel8.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel8.setOpaque(true);
        jLabel8.setVerticalTextPosition(javax.swing.SwingConstants.TOP);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jLabel8)
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(20, 20, 20))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void fillCombo(){
        dbConnection dbcon = new dbConnection();
        dbcon.fillListDualCombo(cmbName,"Select a.id, CONCAT(a.firstname,' ',a.middlename,' ',a.lastname,' ',c.extension) AS name FROM tbl_faculty a,tbl_name_extension c WHERE a.extension_id=c.id ORDER BY name");
        
        dbConnection dbcon2 = new dbConnection();
        dbcon2.fillListDualCombo(cmbDesig,"Select id, designation  FROM tbl_designation ORDER BY id");
        
        dbConnection dbcon3 = new dbConnection();
        dbcon3.fillListDualCombo(cmbOffice,"Select id, CONCAT(office_description,' (',office_code,')') as office FROM tbl_office ORDER BY office");
    }
    
    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        //---------FOR LOGS---------------------
        new dbConnection().recordLog("[Close] New Designation Dialog");
        //--------------------------------------
        this.dispose();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        
        ComboItem Name = (ComboItem) cmbName.getSelectedItem();
        ComboItem Desig = (ComboItem) cmbDesig.getSelectedItem();
        ComboItem Office = (ComboItem) cmbOffice.getSelectedItem();
     
        GetterSetterClass gsc = new GetterSetterClass();
        
        dbConnection dbcon = new dbConnection();
        dbcon.insertData("tbl_faculty_designation","faculty_id,designation_id,office_id,semester,ay_start,ay_end","'" + Name.getID() + "','" + Desig.getID() + "','" + Office.getID() + "','" + gsc.getSemester() + "','" + gsc.getAcademicYearStart() + "','" + gsc.getAcademicYearEnd() + "'");
        dbcon.fillTable(table, "Select facDesig.id, desig.designation, office.office_description, CONCAT(b.address,' ',a.firstname,' ',a.middlename,' ',a.lastname,' ',c.extension) AS name, a.id FROM tbl_faculty a, tbl_name_address b, tbl_name_extension c, tbl_faculty_designation facDesig, tbl_designation desig, tbl_office office WHERE (a.address_id=b.id) and (a.extension_id=c.id) and a.id=facDesig.faculty_id and facDesig.designation_id=desig.id and facDesig.office_id=office.id and semester='" + gsc.getSemester() + "' and ay_start='" + gsc.getAcademicYearStart() + "' and ay_end='" + gsc.getAcademicYearEnd() + "' ORDER BY desig.id");
        
        resetFields();
        //---------FOR LOGS---------------------
        new dbConnection().recordLog("[Save] New Designation Faculty: "+Name.getName()+" Position:"+Desig.getName()+" Office: "+Office.getName()+" Semester: "+gsc.getSemester()+" AY: "+gsc.getAcademicYearStart()+"-"+gsc.getAcademicYearEnd());
        //--------------------------------------
        this.dispose();
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnDesigPlusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDesigPlusActionPerformed
        NewPositionPopupDialog ndd = new NewPositionPopupDialog(this, true, cmbDesig);
        ndd.setLocationRelativeTo(null);
        ndd.setVisible(true);
    }//GEN-LAST:event_btnDesigPlusActionPerformed

    private void btnOfficePlusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOfficePlusActionPerformed
        NewOfficePopupDialog ndd = new NewOfficePopupDialog(this, true, cmbOffice);
        ndd.setLocationRelativeTo(null);
        ndd.setVisible(true);
    }//GEN-LAST:event_btnOfficePlusActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        //---------FOR LOGS---------------------
        new dbConnection().recordLog("[Close] New Designation Dialog");
        //--------------------------------------
    }//GEN-LAST:event_formWindowClosing
    private void resetFields(){
        cmbName.setSelectedIndex(-1);
        cmbDesig.setSelectedIndex(-1);
        cmbOffice.setSelectedIndex(-1);
        cmbName.requestFocus();
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(NewDesignationDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(NewDesignationDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(NewDesignationDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NewDesignationDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                NewDesignationDialog dialog = new NewDesignationDialog(new javax.swing.JFrame(), true, new JTable());
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });

                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDesigPlus;
    private javax.swing.JButton btnOfficePlus;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox cmbDesig;
    private javax.swing.JComboBox cmbName;
    private javax.swing.JComboBox cmbOffice;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel txtOffice;
    // End of variables declaration//GEN-END:variables
}
