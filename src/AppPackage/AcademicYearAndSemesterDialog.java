/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package AppPackage;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

/**
 *
 * @author Jojo
 */
public class AcademicYearAndSemesterDialog extends javax.swing.JDialog {

    /**
     * Creates new form SetAcademicYearDialog
     */
    public AcademicYearAndSemesterDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
        txtProgress.setText("");
        /*Date dateNow = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("Y");
        int yearStart = Integer.parseInt(sdf.format(dateNow));
        int yearEnd = yearStart + 1;*/
        
        //groupButton();
        
        getCurrentAYSem();
                
        //textFieldYearStart.setText(yearStart+"");
        //textFieldYearEnd.setText(yearEnd+"");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        comboSem = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        btnPrev = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        textFieldYearStart = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        textFieldYearEnd = new javax.swing.JTextField();
        btnNext = new javax.swing.JButton();
        btnSet = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        pbar = new javax.swing.JProgressBar();
        txtProgress = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setLocationByPlatform(true);
        setResizable(false);
        setType(java.awt.Window.Type.UTILITY);

        comboSem.setFont(new java.awt.Font("Segoe UI Semilight", 0, 14)); // NOI18N
        comboSem.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1ST SEMESTER", "2ND SEMESTER", "3RD SEMESTER" }));

        jLabel1.setFont(new java.awt.Font("Segoe UI Semilight", 0, 12)); // NOI18N
        jLabel1.setText("Academic Year:");

        jLabel3.setFont(new java.awt.Font("Segoe UI Semilight", 0, 12)); // NOI18N
        jLabel3.setText("Semester:");

        btnPrev.setText("<");
        btnPrev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrevActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(204, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        jPanel1.setForeground(new java.awt.Color(255, 255, 255));

        textFieldYearStart.setEditable(false);
        textFieldYearStart.setBackground(new java.awt.Color(255, 255, 255));
        textFieldYearStart.setFont(new java.awt.Font("Segoe UI Semilight", 0, 14)); // NOI18N
        textFieldYearStart.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        textFieldYearStart.setText("2000");

        jLabel2.setFont(new java.awt.Font("Segoe UI Semilight", 0, 14)); // NOI18N
        jLabel2.setText("-");

        textFieldYearEnd.setEditable(false);
        textFieldYearEnd.setBackground(new java.awt.Color(255, 255, 255));
        textFieldYearEnd.setFont(new java.awt.Font("Segoe UI Semilight", 0, 14)); // NOI18N
        textFieldYearEnd.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        textFieldYearEnd.setText("2001");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(textFieldYearStart, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(textFieldYearEnd, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textFieldYearStart, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(textFieldYearEnd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnNext.setText(">");
        btnNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNextActionPerformed(evt);
            }
        });

        btnSet.setFont(new java.awt.Font("Segoe UI Semibold", 0, 14)); // NOI18N
        btnSet.setText("SET");
        btnSet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSetActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(comboSem, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btnPrev, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnNext, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(85, 85, 85)
                        .addComponent(btnSet, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnPrev, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnNext, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(comboSem, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(btnSet, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jLabel8.setFont(new java.awt.Font("Segoe UI Light", 0, 5)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 102, 255));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel8.setText("_________________________________________________________________________________________________________________________________________________________________________");
        jLabel8.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel8.setOpaque(true);
        jLabel8.setVerticalTextPosition(javax.swing.SwingConstants.TOP);

        jLabel7.setFont(new java.awt.Font("Segoe UI Light", 0, 36)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 102, 255));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("Set AY & Semester");
        jLabel7.setOpaque(true);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jLabel8)
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pbar.setPreferredSize(new java.awt.Dimension(146, 20));

        txtProgress.setText("...");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(30, 30, 30))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pbar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtProgress)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtProgress)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pbar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /*private void groupButton() {
        ButtonGroup radioGroup = new ButtonGroup( );
        radioGroup.add(radioFirstSem);
        radioGroup.add(radioSecondSem);
    }*/
    
    private ProgressInfinite pi;
    class ProgressInfinite extends SwingWorker<Void, Void>{
        @Override
        public Void doInBackground(){
            pbar.setValue(0);
            doSubmit();
            return null;
        }
        @Override
        public void done(){
            
            //pbar.setIndeterminate(false);
        } 
    }
    
    private void incrementPBar(JProgressBar pb, int initialValue, int lastValue){
        for(int i=initialValue;i<=lastValue;i++){
            pb.setValue(i);
            try {
            Thread.sleep(10);
            } catch (InterruptedException ex) {
                Logger.getLogger(ReportFacultyLoad.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private void getCurrentAYSem(){
        GetterSetterClass gsc = new GetterSetterClass();
        textFieldYearStart.setText(gsc.getAcademicYearStart()+"");
        textFieldYearEnd.setText(gsc.getAcademicYearEnd()+"");
        switch (gsc.getSemester()) {
            case 1:
                comboSem.setSelectedIndex(0);
                break;
            case 2:
                comboSem.setSelectedIndex(1);
                break;
            case 3:
                comboSem.setSelectedIndex(2);
                break;
            default:
                break;
        }
    }
    
    private void btnNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNextActionPerformed
        int yearStart = Integer.parseInt(textFieldYearStart.getText()) + 1;
        int yearEnd = Integer.parseInt(textFieldYearEnd.getText()) + 1;
        
        textFieldYearStart.setText(yearStart + "");
        textFieldYearEnd.setText(yearEnd + "");
    }//GEN-LAST:event_btnNextActionPerformed

    private void btnPrevActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrevActionPerformed
        int yearStart = Integer.parseInt(textFieldYearStart.getText()) - 1;
        int yearEnd = Integer.parseInt(textFieldYearEnd.getText()) - 1;
        
        textFieldYearStart.setText(yearStart + "");
        textFieldYearEnd.setText(yearEnd + "");
    }//GEN-LAST:event_btnPrevActionPerformed

    private void doSubmit(){
        //increment progress
        txtProgress.setText("Initializing new changes..");
        incrementPBar(pbar,0,20);
        //--------------
        int sem=1;
        switch (comboSem.getSelectedIndex()) {
            case 0:
                sem=1;
                break;
            case 1:
                sem=2;
                break;
            case 2:
                sem=3;
                break;
            default:
                break;
        }
        
        dbConnection dbcon = new dbConnection();
        //search if the ay and sem exists already
        String findID = dbcon.findOneQuery("id", "tbl_ay_sem", "year_start='" + textFieldYearStart.getText() + "' and "
                + "year_end='" + textFieldYearEnd.getText() + "' and semester='" + sem + "'");
        //increment progress
        txtProgress.setText("Searching current academic year and semester..");
        incrementPBar(pbar,pbar.getValue(),30);
        //--------------
        if(findID.equalsIgnoreCase("-1")){ //means that there is no existing ay and sem yet
            //insert the new ay and sem
            dbcon.insertDataDuplicateExceptionSilently("tbl_ay_sem", "year_start,year_end,semester,status", 
                "'" + textFieldYearStart.getText() + "','" + textFieldYearEnd.getText() + "','" + sem + "','1'");
        }else{//means that there is already exists ay and sem
            dbcon.updateDataSilently("tbl_ay_sem", "status='1'", "id='" + findID + "'");
        }
        //find the id of the current ay and sem
        String id = dbcon.findOneQuery("id", "tbl_ay_sem", "year_start='" + textFieldYearStart.getText() + "' and "
                + "year_end='" + textFieldYearEnd.getText() + "' and semester='" + sem + "'");
        //change status for all that is not selected
        dbcon.updateDataSilently("tbl_ay_sem", "status='0'", "id!='" + id + "'");
        //increment progress
        txtProgress.setText("Changing status..");
        incrementPBar(pbar,pbar.getValue(),35);
        //--------------
/*      
        boolean isAYSem1Exist=false;
        boolean isAYSem2Exist=false;
        boolean isAYSem3Exist=false;
        dbConnection dbcon = new dbConnection();
        if(Integer.parseInt(dbcon.findOneQuery("COUNT(year_start)", "tbl_ay_sem", "year_start='" + textFieldYearStart.getText() + "' and semester='1'"))>0){
            isAYSem1Exist=true;
        }
        if(Integer.parseInt(dbcon.findOneQuery("COUNT(year_end)", "tbl_ay_sem", "year_end='" + textFieldYearEnd.getText() + "' and semester='2'"))>0){
            isAYSem2Exist=true;
        }
        if(Integer.parseInt(dbcon.findOneQuery("COUNT(year_end)", "tbl_ay_sem", "year_end='" + textFieldYearEnd.getText() + "' and semester='3'"))>0){
            isAYSem3Exist=true;
        }

        //just modify if exist
        if(isAYSem1Exist && sem==1){
            dbcon.updateDataSilently("tbl_ay_sem", "status='0'", "status='1'");
            dbcon.updateDataSilently("tbl_ay_sem", "status='1'", "year_start='" + textFieldYearStart.getText() + "' and semester='1'");
            //String.format("%tB %<td, %<tY", dateNow);
            //dbcon.insertData("tbl_ay_sem", "year_start,year_end,semester,status,date_start,date_end", "'" + textFieldYearStart.getText() + "','" + textFieldYearStart.getText() + "','1','0','" + String.format("%tB %<td, %<tY", date_start) + "','" + String.format("%tB %<td, %<tY", date_end) + "'");
        }
        if(isAYSem2Exist && sem==2){
            dbcon.updateDataSilently("tbl_ay_sem", "status='0'", "status='1'");
            dbcon.updateDataSilently("tbl_ay_sem", "status='1'", "year_start='" + textFieldYearStart.getText() + "' and semester='2'");
            //String.format("%tB %<td, %<tY", dateNow);
            //dbcon.insertData("tbl_ay_sem", "year_start,year_end,semester,status,date_start,date_end", "'" + textFieldYearStart.getText() + "','" + textFieldYearStart.getText() + "','1','0','" + String.format("%tB %<td, %<tY", date_start) + "','" + String.format("%tB %<td, %<tY", date_end) + "'");
        }
        if(isAYSem3Exist && sem==3){
            dbcon.updateDataSilently("tbl_ay_sem", "status='0'", "status='1'");
            dbcon.updateDataSilently("tbl_ay_sem", "status='1'", "year_start='" + textFieldYearStart.getText() + "' and semester='3'");
            //String.format("%tB %<td, %<tY", dateNow);
            //dbcon.insertData("tbl_ay_sem", "year_start,year_end,semester,status,date_start,date_end", "'" + textFieldYearStart.getText() + "','" + textFieldYearStart.getText() + "','1','0','" + String.format("%tB %<td, %<tY", date_start) + "','" + String.format("%tB %<td, %<tY", date_end) + "'");
        }
        
        
        //add new ay if not exist
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        
        if(!isAYSem1Exist){
            //String.format("%tB %<td, %<tY", dateNow);
            String date_start = textFieldYearStart.getText()+"-06-01";
            String date_end = textFieldYearStart.getText()+"-10-31";
            
            dbcon.insertDataSilently("tbl_ay_sem", "year_start,year_end,semester,status,date_start,date_end", "'" + textFieldYearStart.getText() + "','" + textFieldYearStart.getText() + "','1','0','" + date_start + "','" + date_end + "'");
            dbcon.updateDataSilently("tbl_ay_sem", "status='0'", "status='1'");
            dbcon.updateDataSilently("tbl_ay_sem", "status='1'", "year_start='" + textFieldYearStart.getText() + "' and semester='1'");
        }
        if(!isAYSem2Exist){
            //String.format("%tB %<td, %<tY", dateNow);
            String date_start = textFieldYearStart.getText()+"-11-01";
            String date_end = textFieldYearEnd.getText()+"-03-31";

            dbcon.insertDataSilently("tbl_ay_sem", "year_start,year_end,semester,status,date_start,date_end", "'" + textFieldYearStart.getText() + "','" + textFieldYearEnd.getText() + "','2','0','" + date_start + "','" + date_end + "'");
            dbcon.updateDataSilently("tbl_ay_sem", "status='0'", "status='1'");
            dbcon.updateDataSilently("tbl_ay_sem", "status='1'", "year_start='" + textFieldYearStart.getText() + "' and semester='1'");
        }
        if(!isAYSem3Exist){
            //String.format("%tB %<td, %<tY", dateNow);
            String date_start = textFieldYearEnd.getText()+"-04-04";
            String date_end = textFieldYearEnd.getText()+"-05-31";

            dbcon.insertDataSilently("tbl_ay_sem", "year_start,year_end,semester,status,date_start,date_end", "'" + textFieldYearStart.getText() + "','" + textFieldYearEnd.getText() + "','3','0','" + date_start + "','" + date_end + "'");
            dbcon.updateDataSilently("tbl_ay_sem", "status='0'", "status='1'");
            dbcon.updateDataSilently("tbl_ay_sem", "status='1'", "year_start='" + textFieldYearStart.getText() + "' and semester='1'");
        }*/

        GetterSetterClass gsc = new GetterSetterClass();
        gsc.setAcademicYearStart(Integer.parseInt(textFieldYearStart.getText()));
        gsc.setAcademicYearEnd(Integer.parseInt(textFieldYearEnd.getText()));
        gsc.setSemester(sem);
        //increment progress
        txtProgress.setText("Generate default class section..");
        incrementPBar(pbar,pbar.getValue(),50);
        //--------------
        
        /*
        MainMDI mm = new MainMDI();
        mm.setAY_Sem();
        mm.textAY.setText(textFieldYearStart.getText()+"-"+textFieldYearEnd.getText());//for display in main window
        switch (comboSem.getSelectedIndex()) {
            case 0:
                mm.textSem.setText("1st");
                break;
            case 1:
                mm.textSem.setText("2nd");
                break;
            case 2:
                mm.textSem.setText("3rd");
                break;
            default:
                break;
        }*/
        
        // for tbl_classlist_section
        dbcon.automate_tbl_classlist_section("id","tbl_classlist","semester='" + gsc.getSemester() + "'");
        //increment progress
        txtProgress.setText("Generate previous designation..");
        incrementPBar(pbar,pbar.getValue(),75);
        //--------------
        //for faculty_designation
        new dbConnection().automate_tbl_designation();
        
        //increment progress
        txtProgress.setText("Done.");
        incrementPBar(pbar,pbar.getValue(),100);
        //--------------
        JOptionPane.showMessageDialog(rootPane, "Academic Year and Semester is now Set!");
        this.dispose();
    }
    
    private void btnSetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSetActionPerformed
        pi = new ProgressInfinite();
        pi.execute();
    }//GEN-LAST:event_btnSetActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AcademicYearAndSemesterDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AcademicYearAndSemesterDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AcademicYearAndSemesterDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AcademicYearAndSemesterDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                AcademicYearAndSemesterDialog dialog = new AcademicYearAndSemesterDialog(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnNext;
    private javax.swing.JButton btnPrev;
    private javax.swing.JButton btnSet;
    private javax.swing.JComboBox comboSem;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JProgressBar pbar;
    private javax.swing.JTextField textFieldYearEnd;
    private javax.swing.JTextField textFieldYearStart;
    private javax.swing.JLabel txtProgress;
    // End of variables declaration//GEN-END:variables
}
