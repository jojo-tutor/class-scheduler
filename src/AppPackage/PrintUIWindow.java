/**
 *
 * @author Jojo
 */
package AppPackage;

import java.awt.*;
import java.awt.print.*;
import javax.swing.*;


public class PrintUIWindow implements Printable {

    JFrame frameToPrint;
    JPanel panelToPrint;
    
    @Override
    public int print(Graphics g, PageFormat pf, int pageIndex) {
    int response = NO_SUCH_PAGE;
    Graphics2D g2 = (Graphics2D) g;
    Dimension d = panelToPrint.getSize(); //get size of document
    double panelWidth = d.width; //width in pixels
    double panelHeight = d.height; //height in pixels
    double pageHeight = pf.getImageableHeight(); //height of printer page
    double pageWidth = pf.getImageableWidth(); //width of printer page
    double scale = pageWidth / panelWidth;
    int totalNumPages = (int) Math.ceil(scale * panelHeight / pageHeight);
    // make sure not print empty pages
    if (pageIndex >= totalNumPages) {
        response = NO_SUCH_PAGE;
    }
    else {
        // shift Graphic to line up with beginning of print-imageable region
        g2.translate(pf.getImageableX(), pf.getImageableY());
        // shift Graphic to line up with beginning of next page to print
        g2.translate(0f, -pageIndex * pageHeight);
        // scale the page so the width fits...
        g2.scale(scale, scale);
        // for faster printing, turn off double buffering
        panelToPrint.setDoubleBuffered(false);
        panelToPrint.paint(g2); //repaint the page for printing
        panelToPrint.setDoubleBuffered(true);
        response = Printable.PAGE_EXISTS;
    }
    return response;
}

    public void goPrint() {
         PrinterJob job = PrinterJob.getPrinterJob();
         job.setPrintable(this);
         boolean ok = job.printDialog();
         if (ok) {
             try {
                  job.print();
             } catch (PrinterException ex) {
              /* The job did not successfully complete */
             }
         }
    }
    
    public PrintUIWindow(JFrame f) {
        frameToPrint = f;
    }
     public PrintUIWindow(JPanel p) {
        panelToPrint = p;
    }

}