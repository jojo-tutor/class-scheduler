/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package AppPackage;

/**
 *
 * @author Jojo
 */

public class ComboItem {
    private int id;
    private String name;
    
    public ComboItem(int id, String name) {
        this.id = id;
        this.name = name;
    }
    public int getID() {
        return id;
    }
    public String getName() {
        return name;
    }
    @Override
    public String toString() {
        return name;
    }
}
