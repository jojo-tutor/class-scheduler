<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="report name" pageWidth="936" pageHeight="612" columnWidth="864" leftMargin="36" rightMargin="36" topMargin="36" bottomMargin="36" uuid="aeeb6757-c8c9-47b2-b349-a74e26d76643">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="table">
		<box>
			<pen lineWidth="1.0" lineColor="#000000"/>
		</box>
	</style>
	<style name="table_TH" mode="Opaque" backcolor="#F0F8FF">
		<box>
			<pen lineWidth="0.5" lineColor="#000000"/>
		</box>
	</style>
	<style name="table_CH" mode="Opaque" backcolor="#BFE1FF">
		<box>
			<pen lineWidth="0.5" lineColor="#000000"/>
		</box>
	</style>
	<style name="table_TD" mode="Opaque" backcolor="#FFFFFF">
		<box>
			<pen lineWidth="0.5" lineColor="#000000"/>
		</box>
	</style>
	<subDataset name="schedule" uuid="441a06d3-caec-44ed-a982-e4aefce06d90">
		<parameter name="S_AY_START" class="java.lang.String"/>
		<parameter name="S_AY_END" class="java.lang.String"/>
		<parameter name="S_SEMESTER" class="java.lang.String"/>
		<parameter name="S_CLASSLIST_ID" class="java.lang.String"/>
		<parameter name="S_SECTION" class="java.lang.String"/>
		<queryString>
			<![CDATA[SELECT
	sj.subject_code as SUBJECT_CODE,
	CONCAT(sj.subject_description,' (',sjd.subject_type,')') as SUBJECT,
	sched.days as DAYS,
	CONCAT(sched.start_time,' - ',sched.end_time) as TIME,
	sjd.numofhours as HOURS,
	(Select numofunits From tbl_subject 
	Where id=sjd.subject_id and sjd.subject_type!='LAB') as UNITS,
	rm.room as ROOM,
	CONCAT(fac.firstname,' ',fac.middlename,' ',fac.lastname) as FACULTY
	
FROM
	tbl_schedule sched,
	tbl_subject sj,
	tbl_subject_detail sjd,
	tbl_room rm,
	tbl_faculty fac

WHERE
	sched.classlist_id=$P{S_CLASSLIST_ID} and
	sched.section = $P{S_SECTION} and 
	rm.id=sched.room_id and
	sjd.id=sched.subject_id and
	sj.id=sjd.subject_id and
	fac.id=sched.faculty_id and
	sched.ay_start=$P{S_AY_START} and
	sched.ay_end=$P{S_AY_END} and
	sched.semester=$P{S_SEMESTER}
	
GROUP BY
	sched.id
ORDER BY
	sjd.id]]>
		</queryString>
		<field name="SUBJECT_CODE" class="java.lang.String"/>
		<field name="SUBJECT" class="java.lang.String"/>
		<field name="DAYS" class="java.lang.String"/>
		<field name="TIME" class="java.lang.String"/>
		<field name="HOURS" class="java.lang.Float"/>
		<field name="UNITS" class="java.lang.Float"/>
		<field name="ROOM" class="java.lang.String"/>
		<field name="FACULTY" class="java.lang.String"/>
		<variable name="SUM_HOURS" class="java.lang.Double" calculation="Sum">
			<variableExpression><![CDATA[$F{HOURS}]]></variableExpression>
		</variable>
		<variable name="SUM_UNITS" class="java.lang.Double" calculation="Sum">
			<variableExpression><![CDATA[$F{UNITS}]]></variableExpression>
		</variable>
		<variable name="OVERLOAD" class="java.lang.Double">
			<variableExpression><![CDATA[$V{SUM_UNITS}-30.0]]></variableExpression>
		</variable>
	</subDataset>
	<parameter name="CLASSLIST_IDs" class="java.lang.String"/>
	<parameter name="SEMESTER" class="java.lang.Number"/>
	<parameter name="SEMESTER_TEXT" class="java.lang.String"/>
	<parameter name="AY_START" class="java.lang.Number"/>
	<parameter name="AY_END" class="java.lang.Number"/>
	<parameter name="PREPARED_BY" class="java.lang.String"/>
	<parameter name="PREPARED_BY_POS" class="java.lang.String"/>
	<parameter name="NOTED_BY" class="java.lang.String"/>
	<parameter name="NOTED_BY_POS" class="java.lang.String"/>
	<parameter name="APPROVED_BY" class="java.lang.String"/>
	<parameter name="APPROVED_BY_POS" class="java.lang.String"/>
	<parameter name="PRINT_NO" class="java.lang.String"/>
	<parameter name="PRINT_DATETIME" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT
	co.course_description as COURSE_DESCRIPTION,
	cl.classlist as CLASSLIST,
	cl.id as CLASSLIST_ID,
	sched.section as SECTION,

	(SELECT tbl_vmgo.text from tbl_vmgo where id='1') as VISION,
	(SELECT tbl_vmgo.text from tbl_vmgo where id='2') as MISSION

FROM 
	tbl_classlist cl,
	tbl_course co,
	tbl_schedule sched,
	tbl_vmgo,
	tbl_faculty,
	tbl_faculty_designation,
	tbl_department,
	tbl_course,
	tbl_college
WHERE 
	sched.classlist_id IN $P!{CLASSLIST_IDs} and 
	cl.id=sched.classlist_id and 
	co.id=cl.course_id

GROUP BY	sched.classlist_id, sched.section
ORDER BY	cl.classlist, sched.section]]>
	</queryString>
	<field name="COURSE_DESCRIPTION" class="java.lang.String"/>
	<field name="CLASSLIST" class="java.lang.String"/>
	<field name="SECTION" class="java.lang.String"/>
	<field name="VISION" class="java.lang.String"/>
	<field name="MISSION" class="java.lang.String"/>
	<field name="CLASSLIST_ID" class="java.lang.String"/>
	<group name="Group1" isStartNewPage="true">
		<groupExpression><![CDATA[$F{CLASSLIST}+"-"+$F{SECTION}]]></groupExpression>
		<groupHeader>
			<band height="55">
				<textField>
					<reportElement uuid="a1ae2660-b09a-4b63-9818-194ad6fbf6cd" x="0" y="25" width="864" height="15"/>
					<textElement textAlignment="Center" verticalAlignment="Middle"/>
					<textFieldExpression><![CDATA[$F{COURSE_DESCRIPTION}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement uuid="a1ae2660-b09a-4b63-9818-194ad6fbf6cd" x="0" y="40" width="864" height="15"/>
					<textElement textAlignment="Center" verticalAlignment="Middle"/>
					<textFieldExpression><![CDATA[$F{CLASSLIST}+"-"+$F{SECTION}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement uuid="67b60cd9-ebfe-461c-8179-8e61a72c2de9" x="0" y="0" width="864" height="15"/>
					<textElement textAlignment="Center" verticalAlignment="Bottom"/>
					<textFieldExpression><![CDATA[$P{SEMESTER_TEXT}+" Semester, A.Y.: "+$P{AY_START}+" - "+$P{AY_END}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="160">
				<staticText>
					<reportElement uuid="a2ae1f37-cf59-4c26-b80a-655dac52d261" x="0" y="15" width="220" height="15"/>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="false"/>
					</textElement>
					<text><![CDATA[Prepared by:]]></text>
				</staticText>
				<textField>
					<reportElement uuid="4da7c944-fa7e-4bef-a381-eae681705f82" x="633" y="150" width="231" height="10"/>
					<textElement textAlignment="Right" verticalAlignment="Bottom">
						<font fontName="Courier New" size="7"/>
					</textElement>
					<textFieldExpression><![CDATA["PRINTED ON: "+$P{PRINT_DATETIME}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement uuid="5a51463a-92f2-4d13-9ec5-96d4bc785422" x="25" y="30" width="170" height="15"/>
					<box>
						<bottomPen lineWidth="0.25"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$P{PREPARED_BY}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement uuid="a2ae1f37-cf59-4c26-b80a-655dac52d261" x="644" y="15" width="220" height="15"/>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="false"/>
					</textElement>
					<text><![CDATA[Noted:]]></text>
				</staticText>
				<textField>
					<reportElement uuid="6f8572ca-859c-45dd-bdf4-699ce879a228" x="670" y="30" width="170" height="15"/>
					<box>
						<bottomPen lineWidth="0.25"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$P{NOTED_BY}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement uuid="9c05d693-f284-4cea-b128-cb4af42d2c74" x="0" y="45" width="220" height="15"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8" isItalic="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$P{PREPARED_BY_POS}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement uuid="a2ae1f37-cf59-4c26-b80a-655dac52d261" x="301" y="90" width="250" height="15"/>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="false"/>
					</textElement>
					<text><![CDATA[Approved:]]></text>
				</staticText>
				<textField>
					<reportElement uuid="33de3213-efb0-4cd9-9433-810737590018" x="301" y="120" width="250" height="15"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8" isItalic="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$P{APPROVED_BY_POS}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement uuid="33de3213-efb0-4cd9-9433-810737590018" x="644" y="45" width="220" height="15"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8" isItalic="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$P{NOTED_BY_POS}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement uuid="16f60aa7-0cdc-432e-9b3e-1b416a9e97f4" x="326" y="105" width="200" height="15"/>
					<box>
						<bottomPen lineWidth="0.25"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$P{APPROVED_BY}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement uuid="155c1204-2dfa-40f0-939d-91e9f8d09c16" x="0" y="150" width="231" height="10"/>
					<textElement verticalAlignment="Bottom">
						<font fontName="Courier New" size="7"/>
					</textElement>
					<textFieldExpression><![CDATA["PRINT #: "+$P{PRINT_NO}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<columnHeader>
		<band height="40" splitType="Stretch">
			<staticText>
				<reportElement uuid="835ee9d4-709b-4e68-8279-347e02dc8a0e" x="372" y="20" width="120" height="20"/>
				<box>
					<pen lineStyle="Double"/>
					<topPen lineStyle="Double"/>
					<leftPen lineStyle="Double"/>
					<bottomPen lineWidth="3.0" lineStyle="Double"/>
					<rightPen lineStyle="Double"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Serif" size="12" isBold="true" isUnderline="false"/>
				</textElement>
				<text><![CDATA[CLASS SCHEDULE]]></text>
			</staticText>
			<textField>
				<reportElement uuid="ddbb37a3-c17b-4a48-8641-0a7da373e1e4" x="0" y="0" width="209" height="10"/>
				<textElement>
					<font fontName="Courier New" size="7"/>
				</textElement>
				<textFieldExpression><![CDATA["PRINT #: "+$P{PRINT_NO}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="7c144797-e436-4df1-bf4a-cd667ff2b246" x="655" y="0" width="209" height="10"/>
				<textElement textAlignment="Right">
					<font fontName="Courier New" size="7"/>
				</textElement>
				<textFieldExpression><![CDATA["PRINTED ON: "+$P{PRINT_DATETIME}]]></textFieldExpression>
			</textField>
		</band>
	</columnHeader>
	<detail>
		<band height="65" splitType="Stretch">
			<componentElement>
				<reportElement uuid="2156d50a-4cc5-44c5-b8f1-60553fb4b2c7" key="table" style="table" x="0" y="10" width="864" height="55"/>
				<jr:table xmlns:jr="http://jasperreports.sourceforge.net/jasperreports/components" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports/components http://jasperreports.sourceforge.net/xsd/components.xsd">
					<datasetRun subDataset="schedule" uuid="16a93277-eeb1-4da2-8c65-da406539b146">
						<datasetParameter name="S_AY_START">
							<datasetParameterExpression><![CDATA[$P{AY_START}]]></datasetParameterExpression>
						</datasetParameter>
						<datasetParameter name="S_AY_END">
							<datasetParameterExpression><![CDATA[$P{AY_END}]]></datasetParameterExpression>
						</datasetParameter>
						<datasetParameter name="S_SEMESTER">
							<datasetParameterExpression><![CDATA[$P{SEMESTER}]]></datasetParameterExpression>
						</datasetParameter>
						<datasetParameter name="S_CLASSLIST_ID">
							<datasetParameterExpression><![CDATA[$F{CLASSLIST_ID}]]></datasetParameterExpression>
						</datasetParameter>
						<datasetParameter name="S_SECTION">
							<datasetParameterExpression><![CDATA[$F{SECTION}]]></datasetParameterExpression>
						</datasetParameter>
						<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
					</datasetRun>
					<jr:column uuid="3def8419-4232-4703-b58e-0d0d8bfc57d1" width="70">
						<jr:columnHeader style="table_CH" height="20" rowSpan="1">
							<box>
								<leftPen lineWidth="0.0" lineStyle="Dashed"/>
								<bottomPen lineWidth="0.25" lineStyle="Dashed"/>
								<rightPen lineWidth="0.0" lineStyle="Dashed"/>
							</box>
							<staticText>
								<reportElement uuid="c16fa4a7-9121-4219-91a8-92de14d6c89a" x="0" y="0" width="70" height="20"/>
								<box leftPadding="5"/>
								<textElement verticalAlignment="Middle">
									<font size="8" isBold="true"/>
								</textElement>
								<text><![CDATA[CODE]]></text>
							</staticText>
						</jr:columnHeader>
						<jr:columnFooter style="table_CH" height="20" rowSpan="1">
							<box>
								<topPen lineWidth="0.25" lineStyle="Dashed"/>
								<leftPen lineWidth="0.0"/>
								<bottomPen lineStyle="Solid"/>
								<rightPen lineWidth="0.0"/>
							</box>
						</jr:columnFooter>
						<jr:detailCell style="table_TD" height="16" rowSpan="1">
							<box>
								<topPen lineWidth="0.25" lineStyle="Dashed"/>
								<leftPen lineWidth="0.0"/>
								<bottomPen lineWidth="0.25" lineStyle="Dashed"/>
								<rightPen lineWidth="0.0"/>
							</box>
							<textField isBlankWhenNull="true">
								<reportElement uuid="d5d5b20c-a23a-46b2-9692-2598b0d139b8" x="0" y="0" width="70" height="16"/>
								<box leftPadding="5"/>
								<textElement verticalAlignment="Middle">
									<font size="8"/>
								</textElement>
								<textFieldExpression><![CDATA[$F{SUBJECT_CODE}]]></textFieldExpression>
							</textField>
						</jr:detailCell>
					</jr:column>
					<jr:column uuid="64674808-9779-41d5-bf85-b3af782fd6b2" width="250">
						<jr:columnHeader style="table_CH" height="20" rowSpan="1">
							<box>
								<leftPen lineWidth="0.0" lineStyle="Dashed"/>
								<bottomPen lineWidth="0.25" lineStyle="Dashed"/>
								<rightPen lineWidth="0.0" lineStyle="Dashed"/>
							</box>
							<staticText>
								<reportElement uuid="37db0340-9309-4073-a05d-6e7b61bd7d1e" x="0" y="0" width="250" height="20"/>
								<box leftPadding="5"/>
								<textElement verticalAlignment="Middle">
									<font size="8" isBold="true"/>
								</textElement>
								<text><![CDATA[DESCRIPTION]]></text>
							</staticText>
						</jr:columnHeader>
						<jr:columnFooter style="table_CH" height="20" rowSpan="1">
							<box>
								<topPen lineWidth="0.25" lineStyle="Dashed"/>
								<leftPen lineWidth="0.0"/>
								<bottomPen lineStyle="Solid"/>
								<rightPen lineWidth="0.0"/>
							</box>
						</jr:columnFooter>
						<jr:detailCell style="table_TD" height="16" rowSpan="1">
							<box>
								<topPen lineWidth="0.25" lineStyle="Dashed"/>
								<leftPen lineWidth="0.0"/>
								<bottomPen lineWidth="0.25" lineStyle="Dashed"/>
								<rightPen lineWidth="0.0"/>
							</box>
							<textField isBlankWhenNull="true">
								<reportElement uuid="d1731634-2b49-4423-a2e2-f89b22bbb89d" x="0" y="0" width="250" height="16"/>
								<box leftPadding="5"/>
								<textElement verticalAlignment="Middle">
									<font size="8"/>
								</textElement>
								<textFieldExpression><![CDATA[$F{SUBJECT}]]></textFieldExpression>
							</textField>
						</jr:detailCell>
					</jr:column>
					<jr:column uuid="71b2eb93-66e1-4bfb-972f-3f1cab7ca156" width="70">
						<jr:columnHeader style="table_CH" height="20" rowSpan="1">
							<box>
								<leftPen lineWidth="0.0" lineStyle="Dashed"/>
								<bottomPen lineWidth="0.25" lineStyle="Dashed"/>
								<rightPen lineWidth="0.0" lineStyle="Dashed"/>
							</box>
							<staticText>
								<reportElement uuid="4d7c8b43-a391-4949-969c-11662ec2ab63" x="0" y="0" width="70" height="20"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="8" isBold="true"/>
								</textElement>
								<text><![CDATA[DAYS]]></text>
							</staticText>
						</jr:columnHeader>
						<jr:columnFooter style="table_CH" height="20" rowSpan="1">
							<box>
								<topPen lineWidth="0.25" lineStyle="Dashed"/>
								<leftPen lineWidth="0.0"/>
								<bottomPen lineStyle="Solid"/>
								<rightPen lineWidth="0.0"/>
							</box>
						</jr:columnFooter>
						<jr:detailCell style="table_TD" height="16" rowSpan="1">
							<box>
								<topPen lineWidth="0.25" lineStyle="Dashed"/>
								<leftPen lineWidth="0.0"/>
								<bottomPen lineWidth="0.25" lineStyle="Dashed"/>
								<rightPen lineWidth="0.0"/>
							</box>
							<textField isBlankWhenNull="true">
								<reportElement uuid="bf721472-11d8-4138-bb30-61da79928119" x="0" y="0" width="70" height="16"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="8"/>
								</textElement>
								<textFieldExpression><![CDATA[$F{DAYS}]]></textFieldExpression>
							</textField>
						</jr:detailCell>
					</jr:column>
					<jr:column uuid="6e3e36a3-7305-42ae-bcd1-365a8bcc30b0" width="100">
						<jr:columnHeader style="table_CH" height="20" rowSpan="1">
							<box>
								<leftPen lineWidth="0.0" lineStyle="Dashed"/>
								<bottomPen lineWidth="0.25" lineStyle="Dashed"/>
								<rightPen lineWidth="0.0" lineStyle="Dashed"/>
							</box>
							<staticText>
								<reportElement uuid="25661e62-4e0e-4f4c-abe8-966c213a6dc9" x="0" y="0" width="100" height="20"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="8" isBold="true"/>
								</textElement>
								<text><![CDATA[TIME]]></text>
							</staticText>
						</jr:columnHeader>
						<jr:columnFooter style="table_CH" height="20" rowSpan="1">
							<box>
								<topPen lineWidth="0.25" lineStyle="Dashed"/>
								<leftPen lineWidth="0.0"/>
								<bottomPen lineStyle="Solid"/>
								<rightPen lineWidth="0.0"/>
							</box>
							<staticText>
								<reportElement uuid="ec1e78d3-9cdf-47b0-8df3-e31c65eaf25c" x="0" y="0" width="100" height="20"/>
								<textElement textAlignment="Right" verticalAlignment="Middle">
									<font size="8" isBold="true"/>
								</textElement>
								<text><![CDATA[Total:]]></text>
							</staticText>
						</jr:columnFooter>
						<jr:detailCell style="table_TD" height="16" rowSpan="1">
							<box>
								<topPen lineWidth="0.25" lineStyle="Dashed"/>
								<leftPen lineWidth="0.0"/>
								<bottomPen lineWidth="0.25" lineStyle="Dashed"/>
								<rightPen lineWidth="0.0"/>
							</box>
							<textField isBlankWhenNull="true">
								<reportElement uuid="310814ee-1ca8-44a5-8124-597a2ccaea90" x="0" y="0" width="100" height="16"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="8"/>
								</textElement>
								<textFieldExpression><![CDATA[$F{TIME}]]></textFieldExpression>
							</textField>
						</jr:detailCell>
					</jr:column>
					<jr:column uuid="0c58df07-38a3-4cf9-a278-548bc1c26617" width="50">
						<jr:columnHeader style="table_CH" height="20" rowSpan="1">
							<box>
								<leftPen lineWidth="0.0" lineStyle="Dashed"/>
								<bottomPen lineWidth="0.25" lineStyle="Dashed"/>
								<rightPen lineWidth="0.0" lineStyle="Dashed"/>
							</box>
							<staticText>
								<reportElement uuid="b3fdcd71-45ce-48b4-b7c3-41a9a0ee3c5b" x="0" y="0" width="50" height="20"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="8" isBold="true"/>
								</textElement>
								<text><![CDATA[UNITS]]></text>
							</staticText>
						</jr:columnHeader>
						<jr:columnFooter style="table_CH" height="20" rowSpan="1">
							<box>
								<topPen lineWidth="0.25" lineStyle="Dashed"/>
								<leftPen lineWidth="0.0"/>
								<bottomPen lineStyle="Solid"/>
								<rightPen lineWidth="0.0"/>
							</box>
							<textField>
								<reportElement uuid="7f0f7fb3-f805-4d78-9ab4-7586d5e1dded" x="0" y="0" width="50" height="20"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="8"/>
								</textElement>
								<textFieldExpression><![CDATA[$V{SUM_UNITS}]]></textFieldExpression>
							</textField>
						</jr:columnFooter>
						<jr:detailCell style="table_TD" height="16" rowSpan="1">
							<box>
								<topPen lineWidth="0.25" lineStyle="Dashed"/>
								<leftPen lineWidth="0.0"/>
								<bottomPen lineWidth="0.25" lineStyle="Dashed"/>
								<rightPen lineWidth="0.0"/>
							</box>
							<textField isBlankWhenNull="true">
								<reportElement uuid="e55b77a1-f238-4d27-acef-4a421a0efcdb" x="0" y="0" width="50" height="16"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="8"/>
								</textElement>
								<textFieldExpression><![CDATA[$F{UNITS}]]></textFieldExpression>
							</textField>
						</jr:detailCell>
					</jr:column>
					<jr:column uuid="c95d55fd-eeb5-4ecd-b160-b5eb854373ec" width="50">
						<jr:columnHeader style="table_CH" height="20" rowSpan="1">
							<box>
								<leftPen lineWidth="0.0" lineStyle="Dashed"/>
								<bottomPen lineWidth="0.25" lineStyle="Dashed"/>
								<rightPen lineWidth="0.0" lineStyle="Dashed"/>
							</box>
							<staticText>
								<reportElement uuid="b3fdcd71-45ce-48b4-b7c3-41a9a0ee3c5b" x="0" y="0" width="50" height="20"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="8" isBold="true"/>
								</textElement>
								<text><![CDATA[HOURS]]></text>
							</staticText>
						</jr:columnHeader>
						<jr:columnFooter style="table_CH" height="20" rowSpan="1">
							<box>
								<topPen lineWidth="0.25" lineStyle="Dashed"/>
								<leftPen lineWidth="0.0"/>
								<bottomPen lineStyle="Solid"/>
								<rightPen lineWidth="0.0"/>
							</box>
							<textField>
								<reportElement uuid="14488ede-0b1c-4ed2-a706-f1cb83a4cc3b" x="0" y="0" width="50" height="20"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="8"/>
								</textElement>
								<textFieldExpression><![CDATA[$V{SUM_HOURS}]]></textFieldExpression>
							</textField>
						</jr:columnFooter>
						<jr:detailCell style="table_TD" height="16" rowSpan="1">
							<box>
								<topPen lineWidth="0.25" lineStyle="Dashed"/>
								<leftPen lineWidth="0.0"/>
								<bottomPen lineWidth="0.25" lineStyle="Dashed"/>
								<rightPen lineWidth="0.0"/>
							</box>
							<textField isBlankWhenNull="true">
								<reportElement uuid="f5d3f0b7-1614-44ab-8b0c-a890b78f2dfa" x="0" y="0" width="50" height="16"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="8"/>
								</textElement>
								<textFieldExpression><![CDATA[$F{HOURS}]]></textFieldExpression>
							</textField>
						</jr:detailCell>
					</jr:column>
					<jr:column uuid="e1c9277e-6787-453e-82cf-69549c40cfb1" width="80">
						<jr:columnHeader style="table_CH" height="20" rowSpan="1">
							<box>
								<leftPen lineWidth="0.0" lineStyle="Dashed"/>
								<bottomPen lineWidth="0.25" lineStyle="Dashed"/>
								<rightPen lineWidth="0.0" lineStyle="Dashed"/>
							</box>
							<staticText>
								<reportElement uuid="ec945572-02d3-43cc-af6f-220a2c950d72" x="0" y="0" width="80" height="20"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="8" isBold="true"/>
								</textElement>
								<text><![CDATA[ROOM]]></text>
							</staticText>
						</jr:columnHeader>
						<jr:columnFooter style="table_CH" height="20" rowSpan="1">
							<box>
								<topPen lineWidth="0.25" lineStyle="Dashed"/>
								<leftPen lineWidth="0.0"/>
								<bottomPen lineStyle="Solid"/>
								<rightPen lineWidth="0.0"/>
							</box>
						</jr:columnFooter>
						<jr:detailCell style="table_TD" height="16" rowSpan="1">
							<box>
								<topPen lineWidth="0.25" lineStyle="Dashed"/>
								<leftPen lineWidth="0.0"/>
								<bottomPen lineWidth="0.25" lineStyle="Dashed"/>
								<rightPen lineWidth="0.0"/>
							</box>
							<textField isBlankWhenNull="true">
								<reportElement uuid="54519ff7-a5d0-42e1-b3c9-d838c332d741" x="0" y="0" width="80" height="16"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="8"/>
								</textElement>
								<textFieldExpression><![CDATA[$F{ROOM}]]></textFieldExpression>
							</textField>
						</jr:detailCell>
					</jr:column>
					<jr:column uuid="2f88142d-8165-4ea0-bc8c-9b567d5382c4" width="190">
						<jr:columnHeader style="table_CH" height="20" rowSpan="1">
							<box>
								<leftPen lineWidth="0.0" lineStyle="Dashed"/>
								<bottomPen lineWidth="0.25" lineStyle="Dashed"/>
								<rightPen lineWidth="0.0" lineStyle="Dashed"/>
							</box>
							<staticText>
								<reportElement uuid="01146196-7308-4323-a05a-f22398905289" x="0" y="0" width="190" height="20"/>
								<box leftPadding="5"/>
								<textElement verticalAlignment="Middle">
									<font size="8" isBold="true"/>
								</textElement>
								<text><![CDATA[INSTRUCTOR]]></text>
							</staticText>
						</jr:columnHeader>
						<jr:columnFooter style="table_CH" height="20" rowSpan="1">
							<box>
								<topPen lineWidth="0.25" lineStyle="Dashed"/>
								<leftPen lineWidth="0.0"/>
								<bottomPen lineStyle="Solid"/>
								<rightPen lineWidth="0.0"/>
							</box>
						</jr:columnFooter>
						<jr:detailCell style="table_TD" height="16" rowSpan="1">
							<box>
								<topPen lineWidth="0.25" lineStyle="Dashed"/>
								<leftPen lineWidth="0.0"/>
								<bottomPen lineWidth="0.25" lineStyle="Dashed"/>
								<rightPen lineWidth="0.0"/>
							</box>
							<textField isBlankWhenNull="true">
								<reportElement uuid="250d86fb-7237-4f77-9055-3d21b88a50ea" x="0" y="0" width="190" height="16"/>
								<box leftPadding="5"/>
								<textElement verticalAlignment="Middle">
									<font size="8"/>
								</textElement>
								<textFieldExpression><![CDATA[$F{FACULTY}]]></textFieldExpression>
							</textField>
						</jr:detailCell>
					</jr:column>
				</jr:table>
			</componentElement>
		</band>
	</detail>
</jasperReport>
